<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard.layout.master');
});
Route::get('/', 'HomeController@index')->name('home');
Route::get('login',  [ 'as' => 'login', 'uses' => 'LoginController@index']);
Route::get('/registration', 'RegistrationController@registration');
Route::get('/registration/getInstruments/', 'RegistrationController@getInstruments');
Route::get('/registration/getProvinces/', 'RegistrationController@getProvinces');
Route::get('/registration/getDistricts/', 'RegistrationController@getDistricts');
Route::get('/registration/getRegencies/', 'RegistrationController@getRegencies');
Route::get('/registration/getGenres/', 'RegistrationController@getGenres');
Route::get('/musician/setting', 'MusicianController@changePassword');
Route::post('/registration', 'RegistrationController@save');
Route::post('/registration/cekEmail', 'RegistrationController@cekEmail');
Route::get('/activate/{confirmationcode}',
			['as'=>'confirmation_path',
			'uses'=>'RegistrationController@activate']);
Route::get('/musician', 'MusicianController@show');
Route::get('/resetpassword', 'ResetPasswordController@index');
Route::post('/resetpassword', 'ResetPasswordController@do');
Route::post('/loginku', 'LoginController@do');
Route::get('/logout', 'LoginController@destroy');
Route::get('/tes','TesController@test');
Route::post('/musician/addEducationDetails', 'MusicianController@addEducationDetails');
Route::post('/musician/addAwardDetails', 'MusicianController@addAwardDetails');
Route::post('/musician/addShortBio', 'MusicianController@addShortBio');
Route::post('/musician/addFullBio', 'MusicianController@addFullBio');
Route::post('/musician/editEducation', 'MusicianController@editEducation');
Route::post('/musician/editAward', 'MusicianController@editAward');
Route::post('/musician/addSkills', 'MusicianController@addSkills');
Route::post('/musician/addProfilePic',['as'=>'image.upload.post','uses'=>'MusicianController@addProfilePic']);
Route::post('/musician/addMedia', 'MusicianController@addMedia');

