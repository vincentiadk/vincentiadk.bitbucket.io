<?php

namespace App;


use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\Model as Eloquent;
class Musisi extends Eloquent  implements Authenticatable
{
    use AuthenticableTrait;

	// use AuthenticableTrait;
     protected $table = 'musisi';
     protected $guarded=[];
     protected $fillable = [
        'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token','password', 
    ];
}
