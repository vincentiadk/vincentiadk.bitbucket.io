<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grouptype extends Model
{
     protected $table = 'grouptype';
     protected $guarded=[];
}