<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Musisi;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DB;
class LoginController extends Controller
{
//	 use AuthenticatesUsers;


	  public function __construct()
    {
        $this->middleware('guest')->except('do','index','destroy');
    }
    public function index(){
    	return view ('frontend.login');
    }
    public function do(Request $request){    
        $login_type = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL ) 
                                        ? 'email' 
                                        : 'username';
 
        $request->merge([
            $login_type => $request->input('username')
        ]);
     
        if (Auth::attempt($request->only($login_type, 'password'))) {
            return redirect('/musician');
        }
        else {
        Session()->flash('message-error', "Gagal sign in, username / password salah.");
        return redirect()->back()
            ->withInput()
            ->withErrors([
                'login' => 'These credentials do not match our records.',
                ]);
        
        }
    }
    public function destroy(){
            Auth::logout();
    return redirect()->home();
    }
}
