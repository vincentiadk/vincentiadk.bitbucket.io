<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Musisi;
use Illuminate\Support\Facades\Hash;
use App\Mail\ResetPassword;
use DB;
class ResetPasswordController extends Controller
{
    public function index(){
    	return view('frontend.resetpassword');
    }

    public function do(){

    	$user = Musisi::where('username',"=",request('username'))
							    ->orWhere('email','=',request('username'))
							    ->first();
		$newpswd = str_random(6);
		$user->password= Bcrypt($newpswd);
		$user->save();
		
	    \Mail::to($user)->send(new ResetPassword($user, $newpswd));
	    Session()->flash('message','<strong>Success</strong>, kami sudah mengirimkan email untuk mereset password ke emailmu!');
	    return redirect('/resetpassword/key');
    }
}
