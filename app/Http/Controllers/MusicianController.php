<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Regency;
use Illuminate\Support\Facades\Auth;
use App\Education;
use App\Award;
use App\Grouptype;
use App\Musisi;
use App\Media; use File;
class MusicianController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function setUserid(){
        $userid=Auth::user()->id;
    }
    public function show(){
    	$location = Regency::find(Auth::user()->location_id);
    	$skills = explode(",",Auth::user()->skills);
    	$educations = Education::where('musisi_id',Auth::user()->id)->get();
    	$awards = Award::where('musisi_id',Auth::user()->id)->get();
    	$grouptype = Grouptype::find(Auth::user()->grouptype_id);
        $youtubevids  = Media::where('musisi_id',Auth::user()->id)->where('mediatype','youtubevid')->get();
        $youtubechannels = Media::where('musisi_id',Auth::user()->id)->where('mediatype','youtubechannel')->get();
         $vimeovids = Media::where('musisi_id',Auth::user()->id)->where('mediatype','vimeovid')->get();
    	return view('dashboard.profilmusisi', compact('location',
                                                        'skills',
                                                        'educations', 
                                                        'awards',
                                                        'grouptype',
                                                        'youtubevids',
                                                        'youtubechannels','vimeovids'));
    }
    public function addEducationDetails(){
    		$eduDetail = new Education;
    	if (trim(request('name'))==""){
    		return response()->json(false);
    	}
    	else {
	    	$eduDetail->musisi_id=Auth::user()->id;
	    	$eduDetail->name=request('name');
	    	$eduDetail->year_start=request('year_start');
	    	$eduDetail->year_end=request('year_end');
	    	$eduDetail->subject=request('subject');
	    	$eduDetail->save();
	    	return response()->json(["Detail pendidikan berhasil ditambahkan",$eduDetail->id]);
    	}
    }
    public function addAwardDetails(){
    		$awardDetail = new Award;
    	if (trim(request('year_get'))=="" || trim(request('description'))==""){
    		return response()->json(false);
    	}
    	else {
	    	$awardDetail->musisi_id=request('userid');
	    	$awardDetail->year_get=request('year_get');
	    	$awardDetail->description=request('description');
	    	$awardDetail->save();
	    	return response()->json(["Detail pengalaman / penghargaan kamu berhasil ditambahkan", $awardDetail->id]);
    	}
    }
    public function addShortBio(){
    	if (trim(request('short_bio'))==""){
    		return response()->json(false);
    	}
    	else {
    		$musisi = Musisi::find(request('userid'));
         	$musisi->short_bio = request('short_bio');
         	$musisi->save();
	    	return response()->json("Biografi singkatmu berhasil disimpan");
    	}
    }
    public function addFullBio(){
    	if (trim(request('full_bio'))==""){
    		return response()->json(false);
    	}
    	else {
    		$musisi = Musisi::find(request('userid'));
         	$musisi->full_bio = request('full_bio');
         	$musisi->save();
	    	return response()->json("Biografi lengkapmu berhasil disimpan");
    	}
    }
    public function addSkills(){
    	if (trim(request('skills'))==""){
    		return response()->json(false);
    	}
    	else {
    		$musisi = Musisi::find(request('userid'));
         	$musisi->skills = request('skills');
         	$musisi->save();
	    	return response()->json(["Personil / instrumen / role kamu berhasil disimpan", $musisi->skills]);
    	}
    }
    public function editEducation(){
    	$eduDetail= Education::find(request('edu_id'));
    	switch (request('type')) {
    		case 'update':
    			if (trim(request('name'))==""){
		    		return response()->json(false);
		    	}
		    	else {	    		
		    		$eduDetail->name=request('name');
		    		$eduDetail->year_start=request('year_start');
		    		$eduDetail->year_end=request('year_end');
		    		$eduDetail->subject=request('subject');
		    		$eduDetail->save();
		    		return response()->json("Detail pendidikan kamu berhasil diubah");
	    		}
    			break;
    		case 'delete':
    			$eduDetail->delete();
    			return response()->json("Detail pendidikan berhasil dihapus");
    			break;
    		default:
    			return response()->json(false);
    			break;
    	}
    }
    public function editAward(){
    	$awardDetail= Award::find(request('award_id'));
    	switch (request('type')) {
    		case 'update':
    			if (trim(request('year_get'))==""||trim(request('description'))==""){
		    		return response()->json(false);
		    	}
		    	else {
		    		
		    		$awardDetail->year_get=request('year_get');
		    		$awardDetail->description=request('description');
		    		$awardDetail->save();
		    		return response()->json("Detail pengalaman / penghargaan kamu berhasil diubah");
		    	}
    			break;
    		case 'delete':
    			$awardDetail->delete();
    			return response()->json("Detail pengalaman / penghargaan berhasil dihapus");
    			break;
    		
    		default:
    			return response()->json(false);
    			break;
    	}    	
    }
    public function deleteEducation(){
    	if (trim(request('edu_id'))==""){
    		return response()->json(false);
    	}
    	else {
    		$eduDelete= Education::where('id',request('edu_id'));
    		$eduDelete->delete();
    		return response()->json("Detail pendidikan kamu berhasil dihapus");
    	}
    }
    public function deleteAward(){
    	if (trim(request('award_id'))==""||trim(request('description'))){
    		return response()->json(false);
    	}
    	else {
    		$awardDelete= Award::where('id',request('award_id'));
    		$awardDelete->save();
    		return response()->json("Detail pengalaman / penghargaan kamu berhasil diubah");
    	}
    }
    public function addProfilePic(){
        switch (request('picType')){
            case 'ProfilePic':
               if(request()->uploadpic=="" ){
                    return back()
                    ->with('err','Profil picture tidak berhasil diunggah')
                    ->with('is_success', false);
                }
                else{
                    request()->validate([
                        'uploadpic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]);

                    $imageName = time().'.'.request()->uploadpic->getClientOriginalExtension();
                    request()->uploadpic->move(public_path('uploads/image'), $imageName);
                    $detailMusician= Musisi::find(Auth::user()->id);
                    if (!$detailMusician->profil_pic==""){
                       File::delete(public_path('uploads/image/') . $detailMusician->profil_pic);
                    }
                    $detailMusician->profil_pic=$imageName;
                    $detailMusician->update();
                    return back()
                        ->with('success','Profil picture berhasil diunggah')
                        ->with('is_success',true)
                        ->with('image',$imageName);
                }
                break;
            case 'CoverPic' :
            if(request()->uploadcover=="" ){
                    return back()
                    ->with('err','Cover picture tidak berhasil diunggah')
                    ->with('is_success', false);
                }
                else{
                    request()->validate([
                        'uploadcover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]);

                    $imageName = time().'.'.request()->uploadcover->getClientOriginalExtension();
                    request()->uploadcover->move(public_path('uploads/image'), $imageName);
                    $detailMusician= Musisi::find(Auth::user()->id);
                    if (!$detailMusician->cover_pic==""){
                       File::delete(public_path('uploads/image/') . $detailMusician->cover_pic);
                    }
                    $detailMusician->cover_pic=$imageName;
                    $detailMusician->update();
                    return back()
                        ->with('success','Cover picture berhasil diunggah')
                        ->with('is_success',true)
                        ->with('image',$imageName);
                }
            break;
            default:
            return back();
            break;
        }
    }
    public function addMedia(){
        $userid = Auth::user()->id;
        $mediaDetail = new Media;
        $mediaDetail->musisi_id=$userid;
        switch (request('type')) {
            case 'youtubevid':
                $mediaDetail->mediatype="youtubevid";
                $mediaDetail->link=request('link');
                $mediaDetail->save();
                return response()->json(['Link youtube berhasil ditambahkan!',$mediaDetail->id]);
                break;
            case 'youtubechannel':
                $mediaDetail->mediatype="youtubechannel";
                $mediaDetail->link=request('link');
                $mediaDetail->save();
                return response()->json(['Channel youtube berhasil ditambahkan!',$mediaDetail->id]);
                break;
            case 'vimeovid':
                $mediaDetail->mediatype="vimeovid";
                $mediaDetail->link=request('link');
                $mediaDetail->save();
                return response()->json(['Link vimeo berhasil ditambahkan!',$mediaDetail->id]);
                break;
            case 'mp3':
                $mediaDetail->mediatype="mp3";
                $fileName = time().'.'.request()->mp3file->getClientOriginalExtension();
                request()->mp3file->move(public_path('uploads/mp3'), $fileName);
                $mediaDetail->link=$fileName;
                $mediaDetail->save();
                return response()->json(['Mp3 berhasil ditambahkan!',$mediaDetail->id]);
                break;
            case 'image':
                $mediaDetail->mediatype="image";
                $fileName = time().'.'.request()->imagefile->getClientOriginalExtension();
                request()->imagefile->move(public_path('uploads/image'), $imagefile);
                $mediaDetail->link=$imagefile;
                $mediaDetail->save();
                return response()->json(['Gambar berhasil ditambahkan ke galeri kamu!',$mediaDetail->id]);
                break;           
            default:
                return response()->json(false);
                break;
        }
    }
    public function changePassword(){

        return view('');
    }
}
