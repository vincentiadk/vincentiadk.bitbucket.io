<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Musisi;
use App\Instrument;
use App\Province;
use App\District;
use App\Regency;
use App\Genre;
use App\Grouptype;
use App\Mail\Registration;
use App\Mail\ActivationSuccess;
use DB;
class RegistrationController extends Controller
{

	/*public function __construct()
    {
        $this->middleware('guest');
    } */
	public function registration(){  
		$grouptype  = Grouptype::all()->sortBy('name');
       	return view('frontend.registration', compact('grouptype'));
	}
    public function save(){  
    	$location_id = DB::table('regencies')->select('id')->where('name',"=",request('location'))->first();
    	$user = new Musisi;
        $user->fullname =request('fullname');
        $user->email=request('email');
        $user->password=BCrypt(request('password'));
        $user->skills=request('skills');
        $user->genres=request('genres');
        $user->location_id=$location_id->id;
        $user->username=request('username');
        $user->mobilephone=request('mobilephone');
        $user->performlocation=request('performlocation');
        $user->musisi_type=request('musisi_type');
        $user->grouptype_id=request('grouptype_id');
        $user->activation_key=str_random(25);
        $user->save();

	    $this->validate(request(),[
	    			'username'=>'required',
	    			'password'=>'required'
	    	]);
	    auth()->login($user);
	    \Mail::to($user)->send(new Registration($user));
	    Session()->flash('message','<strong>Selamat datang di Mussy.co</strong>, kami sudah mengirimkan email konfirmasi pendaftaran kepadamu!');
	       return redirect('/musician');
	}
	public function activate($confirmationcode){
		 $user = Musisi::where('activation_key',"=",$confirmationcode)->where('is_active',"=",0)->first();
         $user->is_active = 1;
         $user->save();
         \Mail::to($user)->send(new ActivationSuccess($user));
       if (!Auth::check()){
      		Session()->flash('message', 'Akun kamu sudah berhasil diaktifkan, silakan login.');
        	return redirect ('/login');
        }
        else {
 			Session()->flash('message', "Akun kamu sudah berhasil diaktifkan");
        	return redirect ('/musician');
        }
	}
	public function getInstruments(){
		return response()->json(Instrument::all('name'));
	}
	public function getProvinces(){
		return response()->json(Province::all('name'));
	}
	public function getDistricts(){
		return response()->json(District::all('name'));
	}
	public function getRegencies(){
		return response()->json(Regency::all('name'));
	}
	public function getGenres(){
		return response()->json(Genre::all('name'));
	}
	public function cekEmail(){
	$cekEmail = Musisi::where('email','=', request('email'))->count();	
		if ($cekEmail >0 ){
			 return response()->json(false);
		}
        else { return response()->json(true);}
	}
	public function cekUsername(){	
		$cekUsername = Musisi::where('username','=', request('username'))->count();	
		if ($cekUsername >0 ){
			 return response()->json(false);
		}
        else { return response()->json(true);}
	}
}
