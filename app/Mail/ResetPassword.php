<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Musisi;
class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $user; public $newpswd;
    public function __construct(Musisi $user, $newpswd)
    {
        $this->user=$user;
        $this->newpswd=$newpswd;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.reset-password');
    }
}
