@component('mail::message')
# Halo, {{ $user->fullname }}
<hr class="colorgraph"/>
<br/>
Akun kamu sudah berhasil diaktifkan.
<br/>
Kamu bisa login melalui link ini
<br/><br/>
@component('mail::button', ['url' => ( config('app.url') . "/login" )])
Login Mussyco
@endcomponent
<br/>
Jangan lupa, lengkapi profilmu semenarik mungkin untuk membuat klien tertarik memesanmu.
<br/>
<br/>
<hr class="colorgraph"/>
<h3>Thank you,</h3>
<h3><a href="{{config('app.url')}}">{{ config('app.name') }}</a></h3>
<h3>Connecting Musician To The World</h3>

@endcomponent