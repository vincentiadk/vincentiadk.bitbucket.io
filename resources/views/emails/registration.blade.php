@component('mail::message')
# Halo, selamat bergabung dengan kami, {{ $user->fullname }}
<hr class="colorgraph"/>
<br/>
Terima kasih telah mendaftar di {{ config('app.name') }} 

Silahkan aktivasi akunmu dengan klik link di bawah ini 


@component('mail::button', ['url' => ( config('app.url') . "/activation/activation_key=" . $user->activation_key ."&uid=" . $user->id )])
Aktifasi Akun
@endcomponent

Setelah ini, silahkan lengkapi profile kamu disini. Buat profilmu selengkap & semenarik mungkin untuk membuat klien tertarik untuk memesanmu.
		
Profil yg tidak lengkap tidak akan masuk dalam halaman website <a href="{{config('app.url')}}">{{ config('app.name') }}</a>. So, pastikan semua info sudah diisi yaa

Oya, kamu mungkin perlu tau, beberapa benefit bergabung dengan <a href="{{config('app.url')}}">{{ config('app.name') }}</a> yaitu
			
- kemudahan mendapat lebih banyak job manggung secara online
- Profilmu/Bandmu bisa diakses oleh seluruh masyarakat Indonesia melalui platform <a href="{{config('app.url')}}">{{ config('app.name') }}</a>
- Koneksi ke banyak musisi hebat dari seluruh Indonesia

dan yang juga tidak kalah penting, pendaftaran sebagai musisi di platform <a href="{{config('app.url')}}">{{ config('app.name') }}</a> tidak dipungut biaya sama sekali (GRATIS)
<br/>
So, lengkapi profilmu sekarang karna ribuan job manggung menantimu!

Jika ada pertanyaan, silahkan reply email ini dan kami akan respons sesegera mungkin

<br/>
<hr class="colorgraph"/>
<h3>Thank you,</h3>
<h3><a href="{{config('app.url')}}">{{ config('app.name') }}</a></h3>
<h3>Connecting Musician To The World</h3>

@endcomponent