@component('mail::message')
# Halo, {{ $user->fullname }}
<hr class="colorgraph"/>
<br/>
Kamu melakukan permintaan untuk mereset password mu.

Berikut ini adalah detail akun kamu untuk login :

@component('mail::panel', ['url' => ''])
	username 	: {{$user->username}} <br/>
	email 		: {{$user->email}} <br/>
	password 	: {{ $newpswd }}
@endcomponent

<br/>
<br/>
Segera login dan ganti password kamu yang baru di sini

@component('mail::button', ['url' => config('app.url'). "/changepassword"])
	Sign in ke Mussy.co
@endcomponent

<br/>
<hr class="colorgraph"/>
<h3>Thank you,</h3>
<h3><a href="{{config('app.url')}}">{{ config('app.name') }}</a></h3>
<h3>Connecting Musician To The World</h3>
@endcomponent
