		<style>
	.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {
   			 color: #50a846;
		}
		.navbar .nav > li > a, .navbar .nav > li.active > .dropdown-menu > li > a {
		    color: #fff;
		    text-shadow: none;
		    font-size:14px;
		}
		.navbar .nav > li > a:hover,.navbar .nav > li.active > .dropdown-menu > li > a:hover {
		  color: #EDDDC5;
		  text-shadow: none;
		  font-size:16px;
		}
		</style>
		<header>
			<div class="navbar navbar-default navbar-static-top fixed" style="background:#685642">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/"><img src="/img/Logo-Mussyco-putih.png" alt="" width="199" height="52" /></a>
					</div>
					<div class="navbar-collapse collapse " style="">
						<ul class="nav navbar-nav">
							@if(Auth::check())
							<li class="dropdown" >
								<a href="/" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" style=" color: #B7C68B">{{Auth::user()->fullname}} <i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="/musician">Profil</a></li>	
									<li><a href="/musician/setting">Pengaturan</a></li>								
									<li><a href="/logout">Logout</a></li>

								</ul>
							</li>						
							<li ><a href="/Job">My Jobs</a></li>
							<li><a href="/Notifications">Notifikasi<!--<i class="fa fa-bullhorn" style="font-size:20px; color:green"></i>--></a></li>
							<li><a href="/Inbox">Inbox<!--<i class="fa fa-inbox" style="font-size:20px; color:green"></i>--></a></li>
													
							
							@endif
						</ul>
					</div>
				</div>
			</div>
		</header>