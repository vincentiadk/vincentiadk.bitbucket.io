<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Mussy.co - Connecting Musician To The World</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Bootstrap 3 template for corporate business" />
	<!-- css -->
	<link href="/css/bootstrap.min.css" rel="stylesheet" />
	<link href="/plugins/flexslider/flexslider.css" rel="stylesheet" media="screen" />
	<link href="/css/cubeportfolio.min.css" rel="stylesheet" />
	<link href="/css/style.css" rel="stylesheet" />
	
	<!-- Theme skin -->
	<link id="t-colors" href="/skins/sand.css" rel="stylesheet" />

	<!-- boxed bg -->
	<link id="bodybg" href="/bodybg/bg1.css" rel="stylesheet" type="text/css" />

	<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
	======================================================= -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="/js/jquery.min.js"></script>
	<script src="/js/modernizr.custom.js"></script>
	<script src="/js/jquery.easing.1.3.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/plugins/flexslider/jquery.flexslider-min.js"></script>
	<script src="/plugins/flexslider/flexslider.config.js"></script>
	<script src="/js/jquery.appear.js"></script>
	<script src="/js/stellar.js"></script>
	<script src="/js/classie.js"></script>
	<script src="/js/uisearch.js"></script>
	<script src="/js/jquery.cubeportfolio.min.js"></script>
	<script src="/js/google-code-prettify/prettify.js"></script>
	<script src="/js/animate.js"></script>
<!--	<script src="/js/custom.js"></script>-->
 <script src="/js/jquery-ui.min.js" type="text/javascript" ></script>
	 <link href="/css/jquery-ui.min.css" rel="stylesheet" />
</head>

<body>
<script type="text/javascript">

</script>


	<div id="wrapper">
		<!-- start header -->
	@include('dashboard.layouts.header')
		<!-- end header -->

		<section id="content">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						 @yield('content')
						
					</div>
				</div>
			</div>
		
		
		</section>

		@include('dashboard.layouts.footer')
	</div>
	<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>



</body>

</html>
