@extends('dashboard.layouts.master')
@section('content')
<script src="/js/bootstrap-tagsinput.js" type="text/javascript" ></script>
<script src="/js/typeahead.js" type="text/javascript" ></script>
<link href="/css/bootstrap-tagsinput.css" rel="stylesheet" />
<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >
<script src="//rawgit.com/victorjonsson/jquery-editable/master/jquery.editable.min.js"></script>

<style>
.tt-query {
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
  -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}

.tt-hint {
  color: #999
}

.tt-dropdown-menu {    /* used to be tt-dropdown-menu in older versions */
  width: 422px;
  margin-top: 4px;
  padding: 4px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
  -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
  box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.tt-suggestion {
  padding: 3px 20px;
  line-height: 24px;
}

.tt-suggestion.tt-cursor,.tt-suggestion:hover {
  color: #fff;
  background-color: #0097cf;

}

.tt-suggestion p {
  margin: 0;
}
        #container {
            
            padding: 0 20px;
            margin: 20px auto;
            background: #FFF;
        }
       
        em {
            font-size: 80%;
            color: #888;
        }
        h2 {
            font-family: Tahoma, sans-serif;
            font-size: 20px;
        }
        button {
            padding: 5px;
            margin-right: 5px;
            line-height: normal;
        }
        hr {
            margin: 10px 0;
        }
        #log {
            width: 98%;
            padding: 1%;
            color: #777;
            font-size: 80%;
            height: 80px;
        }
        p {
        	font-size:16px;
        }
.twitter-typeahead{
	width:100%;
}
.iconsetting {
	font-size:16px;
	color:#50A846;
	cursor:pointer;
	margin-right:20px;
}
.detailhover:hover{
	background:#F7FDCA;
	cursor:default;
}
.editable-popup{top:-61px !important;left:245px !important;}
.shortbio-editable:hover .shortbio-edit {
    display: inline;
}
.photo-container {
    margin-top: 5px;
    overflow: hidden;
    height: 240px;
}
.edit-profile-pic {
    background: #000;
    opacity: .4;
    transition: opacity .25s ease-out;
    -moz-transition: opacity .25s ease-out;
    -webkit-transition: opacity .25s ease-out;
    -o-transition: opacity .25s ease-out;
}
.container {
    width: 100%;
}
h1, h2, h3, h4, h5, h6 {
    margin-bottom: 0px;
}
aside .widget {
    margin-bottom: 0px;
}
.edit-insts {
    background: rgba(255,255,255,.7);
    cursor: pointer;
}
.btn-primary {
    margin-bottom: 5px;
}
.cover-picture {
    position: relative;
    height: 241px;
    background:#e6e6e6;
}
.row {
    margin: 0px; 
    margin-bottom:0px !important;
}
.left-sidebar {
    padding: 0px;
}
.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    padding: 0px; 
}
.edit-cover-photo {
    position: static;
    text-align: center;
    width: 100%;
    background: rgba(0,0,0,.75);
    height: 40px;
    line-height: 38px;
    font-size: 15px;
    opacity: .5;
    z-index: 10;
    transition: opacity .25s ease-out;
    -moz-transition: opacity .25s ease-out;
    -webkit-transition: opacity .25s ease-out;
    -o-transition: opacity .25s ease-out;
}
</style>
<script type="text/javascript">
$(document).ready(function() 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').val(),
        },
    });
    $('#success-save').hide();
    $('#error-save').hide();
    var userid =     {{ Auth::user()->id   }};
    var $editables = $('.editable'),
        $editable_bio = $('.editable_bio'),
        $info = $('#info em'),
        $info_bio = $('#info_bio em');
    function makeThingsEditable() {
        $editables.editable({
            emptyMessage: '<em>Please write something...</em>',
            callback: function(data) {
                $info.hide();
                $editables.css("border", "none");
                $editables.css("padding", "0px");
                $info.eq(0).show();
                $content = data.content;
                var datas = saveShortBio($content);
                if (datas == false) {
                    $('#error-save').show().delay(5000).fadeOut();
                    $('#success-save').hide();
                } else {
                    $('#error-save').hide();
                    $('#success-save').html('');
                    $('#success-save').append(datas);
                    $('#success-save').show().delay(5000).fadeOut();
                }
            },
        });
        $editable_bio.editable({
            emptyMessage: '<em>Please write something...</em>',
            callback: function(data) {
                $info_bio.hide();
                $editable_bio.css("border", "none");
                $editable_bio.css("padding", "0px");
                $info_bio.eq(0).show();
                $content = data.content;
                var datas = saveFullBio($content);
                if (datas == false) {
                    $('#error-save').show().delay(5000).fadeOut();
                    $('#success-save').hide();
                } else {
                    $('#error-save').hide();
                    $('#success-save').html('');
                    $('#success-save').append(datas);
                    $('#success-save').show().delay(5000).fadeOut();
                }
            }
        });
    };
    //async:false
    function saveShortBio(dataku) {
        var returnVal = '';
        $.ajax({
            async: false,
            type: "POST",
            url: '/musician/addShortBio',
            dataType: 'json',
            data: {
                short_bio: $content,
                userid: userid
            },
            success: function(data) {
                returnVal = data;
            }
        });
        return returnVal;
    };
    //async:false
    function saveFullBio(dataku) {
        var returnVal = '';
        $.ajax({
            async: false,
            type: "POST",
            url: '/musician/addFullBio',
            dataType: 'json',
            data: {
                full_bio: $content,
                userid: userid
            },
            success: function(data) {
                returnVal = data;
            }
        });
        return returnVal;
    };
    var arr = [];
    $.ajax({
    type: "GET",
	    url:'/registration/getInstruments/',
	    dataType: 'json',
	    success: function(data) {
	     for(var x in data){
	      arr.push(data[x]["name"]);
	    }
	    var instrumentsname = new Bloodhound({
	      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
	      queryTokenizer: Bloodhound.tokenizers.whitespace,
	      local: $.map(arr, function (instrument) {
	        return {
	          name: instrument
	        };
	      })
    });
	    instrumentsname.initialize();
	    $('.colinput >> input').tagsinput({
	      typeaheadjs: [{
	        minLength: 1,
	        highlight: true
	      },{
	        minlength: 3,
	        name: 'instrumentsname',
	        displayKey: 'name',
	        valueKey: 'name',
	        source: instrumentsname.ttAdapter()
	      }],
	      freeInput: true
	    });
	}	});
    editInstruments=function(){
    	$skills = "{{Auth::user()->skills}}";
    	$('.colinput >> input').tagsinput('add',$skills);
    //	 $("input[name=skills]").val($('.colinput >> input').val());
    	$('#edit-instrument').modal('show');

    }
    saveInstruments=function(){
    //	$("input[name=skills]").val($('.colinput >> input').val());
    	var skills = $('.colinput >> input').val();
    	 $.ajax({
            type: "POST",
            url: '/musician/addSkills',
            dataType: 'json',
            data: {
                skills: skills,
                userid: userid,
            },
            success: function(data) {
                if (data == false) {
                    $('#error-save').show().delay(5000).fadeOut();
                    $('#success-save').hide();
                } else {
                    $('#error-save').hide();
                    $('#success-save').html('');
                    $('#success-save').append(data[0]);
                    $('#success-save').show().delay(5000).fadeOut();
                    $('#instrument-detail').html('');
                    var instruments= data[1].split(',');
                    var RowInstrument ="";
                    $(instruments).each(function(index,value){
                    //	 console.log('div' + index + ':' + $(this).attr('id'));

                    RowInstrument += "<a id='instrument_"+index+"' href='/search?tags="+value+"' data-instrument-name='"+value+"' class='instrument-link ui-sortable-handle'><btn style='font-size:20px; width:100%;' class='btn btn-primary btn-sm instrument-btn'>"+value+"</btn></a>";
                    });
                    $('#instrument-detail').append(RowInstrument);
                }
                 $('#edit-instrument').modal('hide');

            }

        });
    }
    doEditEducation = function(id,name,subject,year_start,year_end) {
    	$('#edu_name').val(name);
        $('#edu_year_start').val(year_start);
        $('#edu_subject').val(subject);
        $('#edu_year_end').val(year_end);
        $('#edu_id').val(id);
        $('#btnSaveEdu').hide();
        $('#btnUpdateEdu').show();
    	$('#add-education').modal('show');    
    }

    isNumber = function(id) {
        var n = $('#' + id).val();
        if (n < 1950)
            $('#' + id).val(1950);
        if (n > 2200)
            $('#' + id).val(2017);
    }
    submitAwardDetails = function(type) {
        var award_year_get = $('#award_year_get').val();
        var award_description = $('#award_description').val();
        var award_year_get_edit = $('#award_year_get_edit').val();
        var award_description_edit = $('#award_description_edit').val();
        var award_id =$('#award_id').val();

        if(type==1){ //menambahkan awward baru
        $.ajax({
            type: "POST",
            url: '/musician/addAwardDetails',
            dataType: 'json',
            data: {
                year_get: award_year_get,
                description: award_description,
                userid: userid
            },
            success: function(data) {
                if (data == false) {
                    $('#error-save').show().delay(5000).fadeOut();
                    $('#success-save').hide();
                } else {
                    $('#error-save').hide();
                    $('#success-save').html('');
                    $('#success-save').append(data[0]);
                    $('#success-save').show().delay(5000).fadeOut();
                    var rowAward = "<div class='row' id='awarddetail_"+data[1]+"'><div class='col-md-10'><h5 style='margin-left:10px; font-style:italic'>" + award_year_get + " — " + award_description + "</h5></div><div class='col-md-2'><i class='fa fa-pencil iconsetting' onclick='doEditAward("+award_id+","+award_year_get+","+award_description+")' ></i><i class='fa fa-trash-o iconsetting' onclick='doDeleteAward("+award_id+")' ></i></div></div>";
                    $("#awards_detail").append(rowAward);
                    $('#award_year_get').val('');
                    $('#award_description').val('');
                    $('#awards_dummy_detail').css('display', 'none');
                }
            }
        });
    	}
    	else if (type==2) { //update dan save ke database
    		 $.ajax({
            type: "POST",
            url: '/musician/editAward',
            dataType: 'json',
            data: {
                year_get: award_year_get_edit,
                description: award_description_edit,
                award_id: award_id,
                type:'update',
            },
            success: function(data) {
                if (data == false) {
                    $('#error-save').show().delay(5000).fadeOut();
                    $('#success-save').hide();
                } else {
                    $('#error-save').hide();
                    $('#success-save').html('');
                    $('#success-save').append(data);
                    $('#success-save').show().delay(5000).fadeOut();
                    $('#awarddetail_'+award_id).html('');
                    var rowAward = "<div class='col-md-10'><h5 style='margin-left:10px; font-style:italic'>" + award_year_get_edit + " — " + award_description_edit + "</h5></div><div class='col-md-2'><i class='fa fa-pencil iconsetting' onclick='doEditAward("+award_id+","+award_year_get_edit+","+award_description_edit+")' ></i><i class='fa fa-trash-o iconsetting' onclick='doDeleteAward("+award_id+")' ></i></div>";
                    $("#awarddetail_"+award_id).append(rowAward);
                    $('#award_year_get').val('');
                    $('#award_description').val('');
                    $('#awards_dummy_detail').css('display', 'none');
                }
                 $('#edit-award').modal('hide');
            }

        });
    	}
    }
    refreshEducation =function(){
    	 $('#edu_name').val('');
         $('#edu_year_start').val('');
         $('#edu_subject').val('');
         $('#edu_year_end').val('');
    }
    addEducationDetails = function (){
    	refreshEducation();
    	$("#btnSaveEdu").show();
    	$("#btnUpdateEdu").hide();
    	$("#add-education").modal('show');
    }
    saveEducationDetails = function(type) {
        var name = $('#edu_name').val();
        var year_start = $('#edu_year_start').val();
        var subject = $('#edu_subject').val();
        var year_end = $('#edu_year_end').val();
        var edu_id=$('#edu_id').val();
        if (type==1) //tambah baru
        {
	        $.ajax({
	            type: "POST",
	            url: '/musician/addEducationDetails',
	            dataType: 'json',
	            data: {
	                name: name,
	                subject: subject,
	                year_start: year_start,
	                year_end: year_end,
	                userid: userid
	            },
	            success: function(data) {
	                if (data == false) {
	                    $('#error-save').show().delay(5000).fadeOut();
	                    $('#success-save').hide();
	                } else {	                   
	                    $('#error-save').hide();
	                    $('#success-save').html('');
	                    $('#success-save').append(data[0]);
	                    $('#success-save').show().delay(5000).fadeOut();
	                    var rowEducation = "<div class='row' id='education_"+data[1]+"''><div class='col-md-10'><h4>" + name +
	                        "</h4></div><div class='col-md-2'><i class='fa fa-pencil iconsetting' onclick='doEditEducation('"+data[1]
	                        +"',"+name+","+subject+","+year_start+","+year_end+")' ></i><i class='fa fa-trash-o iconsetting'  onclick='doDeleteEducation("+data[1]+")'  ></i></div><div class='col-md-12'><i><strong>" + subject +
	                        "</strong> (" + year_start + " - " + year_end + ")</i></div></div>";
	                    $("#edu_details").append(rowEducation);
	                   refreshEducation();
	                }
	                 $('#add-education').modal('hide');
	            }
	        });
    	}
	    else { //type=2 edit dan save data lama
	    	$.ajax({
	            type: "POST",
	            url: '/musician/editEducation',
	            dataType: 'json',
	            data: {
	                name: name,
	                subject: subject,
	                year_start: year_start,
	                year_end: year_end,
	                edu_id: edu_id,
	                type:"update"
	            },
	            success: function(data) {
	                if (data == false) {
	                    $('#error-save').show().delay(5000).fadeOut();
	                    $('#success-save').hide();
	                } else {
	                   
	                    $('#error-save').hide();
	                    $('#success-save').html('');
	                    $('#education_'+edu_id).html('');
	                     var detailEducation = "<div class='col-md-10'><h4>" + name +
	                        "</h4></div><div class='col-md-2'><i class='fa fa-pencil iconsetting' onclick='doEditEducation('"+edu_id
	                        +"',"+name+","+subject+","+year_start+","+year_end+")' ></i><i class='fa fa-trash-o iconsetting'  onclick='doDeleteEducation("+edu_id+")'  ></i></div><div class='col-md-12'><i><strong>" + subject +
	                        "</strong> (" + year_start + " - " + year_end + ")</i></div>";
	                    $("#education_"+edu_id).append(detailEducation);
	                    $('#success-save').append(data);
	                    $('#success-save').show().delay(5000).fadeOut();
	                   refreshEducation();	              
	                }
	                 $('#add-education').modal('hide');
	            }
	        });
	    }
	}
	doEditAward = function(id,year_get,description){
		$('#award_year_get_edit').val(year_get);
        $('#award_description_edit').val(description);
        $('#award_id').val(id);
		$('#edit-award').modal('show');

	}
	doDeleteEducation=function(id){
		var edu_id = id;
		if( confirm("Apa kamu yakin akan menghapus data tersebut?") ){

		//	alert('tes');
			$.ajax({
		            type: "POST",
		            url: '/musician/editEducation',
		            dataType: 'json',
		            data: {	                
		                edu_id: edu_id,
		                type:"delete"
		            },
		            success: function(data) {
		                if (data == false) {
		                    $('#error-save').show().delay(5000).fadeOut();
		                    $('#success-save').hide();
		                } else {	                   
		                    $('#error-save').hide();
		                    $('#success-save').html('');
		                 	 $('#success-save').append(data);
		                    $("#education_"+edu_id).remove();
		                    $('#success-save').show().delay(5000).fadeOut();
		                           
		                }
		            }
		        });
		}
	};
	doDeleteAward=function(id){
		var award_id = id;
		if( confirm("Apa kamu yakin akan menghapus data tersebut?") ){
			 $.ajax({
	            type: "POST",
	            url: '/musician/editAward',
	            dataType: 'json',
	            data: {
	                award_id: id,
	                type:'delete',
	            },
	            success: function(data) {
	                if (data == false) {
	                    $('#error-save').show().delay(5000).fadeOut();
	                    $('#success-save').hide();
	                } else {
	                    $('#error-save').hide();
	                    $('#success-save').html('');
	                    $('#success-save').append(data);
	                    $('#success-save').show().delay(5000).fadeOut();                   
	                    $("#awarddetail_"+award_id).remove();

	                }
	            }

	        });
		}
	}
    $editables.on('edit', function($textArea) {
        $info.hide();
        $info.eq(1).show();
        $editables.css("border", "1px solid");
        $editables.css("padding", "20px");
        //  log('Started editing element '+ this.nodeName);
    });
    $editable_bio.on('edit', function($textArea) {
        $info_bio.hide();
        $info_bio.eq(1).show();
        $editable_bio.css("border", "1px solid");
        $editable_bio.css("padding", "20px");
        //  log('Started editing element '+ this.nodeName);
    });
    uploadpp = function(){
    	$('#uploadpic').click();
        $('#picType').val('ProfilePic');
    	$('#uploadpic').on('change', function(){
    	       $('#btnUpload').click();
            }); 
       
        };
    $('.edit-cp').on('click',function(){
            $('#uploadcover').click();
            $('#picType').val('CoverPic');
            $('#uploadcover').on('change', function(){
                   $('#btnUpload').click();
                }); 
        });
    addMedia=function(type){
        var link;
        switch(type){
            case "youtubevid":
            link = $('#inputyoutubevid').val();
            break;
            case "youtubechannel" : //youtube channel
            link = $('#inputyoutubechannel').val();
            break;
            case "vimeovid" : //vimeo video
            link = $('#inputvimeovid').val();
            break;
            case "mp3" : //mp3
            link="";
            break;
            case "image" : //image gallery
            link="";
            break;
            default:
            alert("cannot find type");
            break;
        };
         $.ajax({
            type: "POST",
                    async:false,
                    url: '/musician/addMedia',
                    dataType: 'json',
                    data: {                 
                        type:type, link:link,
                    },
                    success: function(data) {
                        if (data == false) {
                            $('#error-save').show().delay(5000).fadeOut();
                            $('#success-save').hide();
                        } else {                       
                            $('#error-save').hide();
                            $('#success-save').html('');
                            $('#success-save').append(data[0]);
                            $('#success-save').show().delay(5000).fadeOut();                 
                        }
                         location.reload();
                    },
            });
    }
    makeThingsEditable();
});

</script>

@if ($flash = session('success'))
<div class="alert alert-success" role="alert" id="success-image"  style="display:block" >
{{$flash}}
</div>
@endif
<div class="alert alert-success flash-message" role="alert" id="success-save" style="display:none" >

</div>
      	<div id="error-save" class="alert alert-danger flash-message" role="alert"  >
			Tidak bisa menyimpan, mohon lengkapi data	
		</div>
<form action="/musician/addProfilePic" method="post" enctype="multipart/form-data">
     {{ csrf_field() }}
     <input type="hidden" id="picType" name="picType" >
<input type="file" name="uploadpic" style="display:none" id="uploadpic" accept="image/*">
<input type="file" name="uploadcover" style="display:none" id="uploadcover" accept="image/*">
    <input type="submit" name="previous" value="Upload" style="display:none" id="btnUpload"/>
</form>

	
<!-- Modal Short Bio-->
  <div class="modal fade" id="edit-award" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
      		<input type="hidden" id="award_id" name="award_id">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ubah Pengalaman / Penghargaan </h4>
        </div>
        <div class="modal-body">
          	<div id="addAchievementDetails" class="row">
				<div style="white-space:nowrap; display:flex;display:-webkit-flex;display:-ms-flexbox; margin:15px; margin-bottom:0px;" class="form-group">
					<input type="text" placeholder="Year" style="width:80px; border-right:none; border-top-right-radius:0; border-bottom-right-radius:0;" id="award_year_get_edit" class="form-control form-control">
					<input type="text" placeholder="Description" style="width:80%; flex-grow:1; -webkit-flex-grow:1; -ms-flex:1; border-top-left-radius:0; border-bottom-left-radius:0;" id="award_description_edit" class="form-control input-rightbtn form-control">
					<button onclick="submitAwardDetails(2)" style="border-top-right-radius:3px !important; border-bottom-right-radius:3px !important;" class="btn btn-primary input-rightbtn">Update</button>
					</div>
			</div>
        </div>

      </div>     
    </div>
  </div>
  <div class="modal fade" id="edit-instrument" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
      		
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ubah Personil / Instrument / Role  </h4>
        </div>
        <div class="modal-body">
        	  <div class="colinput">
            <input data-role="tagsinput" type="text" name="skills" placeholder="Instrument / Role / Personil"  /></div>
        </div>
 		<div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="saveInstruments()" >Save</button>
    </div>
      </div>     
    </div>
  </div>
  <div class="modal fade" id="add-education" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">

      		<input type="hidden" id="edu_id" name="edu_id">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambahkan Riwayat Pendidikan / Kursus </h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
          	<input class="form-control" type="text" placeholder="Nama sekolah / Universitas / Kursus" id="edu_name" required>
          </div>
          	<div class="form-group">
          		<input class="form-control" type="text" placeholder="Jurusan / bidang studi / jenis kursus" id="edu_subject">
          	</div>
         
          <div class="form-group">         	
          		<input class="form-control" type="number" placeholder="Tahun dimulai" id="edu_year_start" min="1900" max="2017" onchange="isNumber(id)" >        
          </div>
          <div class="form-group">
          	
          		<input class="form-control" type="text" placeholder="Tahun selesai" id="edu_year_end" maxlength="4" onchange="isNumber(id)" >
          </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="saveEducationDetails(1)" id="btnSaveEdu">Save</button>
        <button type="button" class="btn btn-primary" onclick="saveEducationDetails(2)" id="btnUpdateEdu">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<div class="col-lg-3">
	<aside class="left-sidebar">
		<div class="widget">
			<div style="z-index:1; position:relative;" class="edit-profile-pic text-center"><div style="cursor:pointer; padding-top:9px;padding-bottom:9px; color: #F5F5F5; font-size: 14.5px; font-weight:300;" class="edit-pp" onclick="uploadpp()">Click here to add a profile picture</div></div>
			<div class="photo-container text-center"> <a ui-sref="profile" href="#/dashboard/profile"> <img src="/uploads/image/{{Auth::user()->profil_pic}}" class="photo-container" style="max-width: 240px"> </a>  </div>
		</div>
		<div class="widget">
			<h3>{{ Auth::user()->fullname }}</h3>
			<h4>{{ $location->name }}</h4>
			@if(Auth::user()->musisi_type ==1 )	<h5>
				Solo Performance		
			</h5>@endif
			@if(Auth::user()->musisi_type ==2 )	<h5>
				Group Performance :: {{$grouptype->name}}		
			</h5>
			@endif
		</div>
		<div class="widget">
			<div style="overflow: hidden; min-height: 200px; width: 100%; position: relative; top: 0px; left: 0px;" class="instruments-btns always-visible ps-container ui-sortable">
				<div onclick="editInstruments()" style="text-align:center; color:#5E5E5E; border-bottom-right-radius:4px; width:68px; height:34px; position:absolute; top:0; left:0; z-index:100; padding:4px; padding-left:6px; white-space:nowrap;" class="edit-insts"><span class="fa fa-pencil"></span> Edit</div>
				<div id="instrument-detail">
				@php ($i=0)
				@foreach ($skills as $skill)
				<a id="instrument_{{ $i }}" href="/search?tags={{$skill}}" data-instrument-name="{{$skill}}" class="instrument-link ui-sortable-handle">
					<btn style="font-size:20px; width:100%;" class="btn btn-primary btn-sm instrument-btn">{{$skill}}
					</btn>
				</a>
				@php ($i++ )
				
				@endforeach
				</div>
				
					<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;">						
					</div>
				</div>
				<div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 0px;">
					<div class="ps-scrollbar-y" style="top: 0px; height: 0px;">
					
				</div>
		</div>
		</div>
		</div>
	</aside>
</div>
<div class="col-lg-9">
	<div class="row">
		<div class="cover-picture" style="background-image:url('/uploads/image/{{Auth::user()->cover_pic}}')" >
		<div class="edit-cover-photo" style="opacity: 0.5;"><div style="cursor: pointer; color: #F5F5F5; font-weight:300; " class="edit-cp">Click here to change or reposition your cover photo</div></div>
		</div>

	</div>
	<div class="row">
			<div class="col-lg-12">
				<ul class="nav nav-tabs">
									<li class="active" id="tabTentang"><a href="#one" data-toggle="tab"><i class="icon-briefcase"></i> Tentang</a></li>
									<li><a href="#two" data-toggle="tab" id="tabMedia">Media</a></li>
									<li><a href="#three" data-toggle="tab" id="tabPenampilan">Testimonial</a></li>
                                    <li><a href="#four" data-toggle="tab" id="tabPenampilan">Jadwal</a></li>
				</ul>
			<div class="tab-content">
									<div class="tab-pane active" id="one">
										
										<div class="row">
											<div class="col-md-10">
												<p style="font-weight:700;" class="js-enable-message"> <span>Harap lengkapi bagian profilmu berikut ini supaya kami dapat menampilkanmu di website Mussy.co</span>										
												</p>
												@if(trim(Auth::user()->short_bio)=="" || trim(Auth::user()->full_bio)=="" )
												<i class="fa fa-times">Biografi singkat / full</i>
											@endif
									
												<i class="fa fa-check">apa mu</i>
											</div>
										</div>
										<hr class="colorgraph">
										<div class="row">
											<h3 style="float:left">Tentang Kamu</h3>
										</div>
										
										<div class="row" style="cursor:pointer;">
										  <p id="info">
										    <em>Double click/tap text di bawah ini untuk mengubah</em>
										    <em style="display: none">ctr | cmd + &uarr;/&darr; untuk mengubah ukuran font</em>
										  </p>
										  										   
										    <p class="editable">
										@if(trim(Auth::user()->short_bio)=="")
										      <b>Halo,</b> kamu belum mengisi deskripsi singkatmu! Ayo tuliskan tagline, ide, atau gagasan kamu di sini.
										      
										 @else
										 {!! 	Auth::user()->short_bio !!}
										  @endif	
										    </p>										
										</div>
										<hr class="colorgraph">
										<div class="row">
											<div class="col-md-6">
												<div class="row"><h3 style="float:left">Pendidikan / Kursus</h3></div>
												<div id="edu_details">
													@foreach($educations as $education)
													<div class="row detailhover" id="education_{{$education->id}}" >
													<div class="col-md-10"><h4>{{$education->name}}</h4></div>
													<div class='col-md-2'><i class='fa fa-pencil iconsetting' onclick="doEditEducation({{$education->id}},'{{$education->name}}','{{$education->subject}}','{{ $education->year_start }}','{{$education->year_end }}')" ></i><i class='fa fa-trash-o iconsetting' onclick="doDeleteEducation({{$education->id}})" ></i></div>
													<div class='col-md-12'>
														<i><strong>{{$education->subject}}</strong> ({{ $education->year_start }} - {{ $education->year_end }})</i></div>
													</div>
													@endforeach
												</div>
											
												<div class="row">
												<center><div class="add-education"><span style="font-size: 45px; display: inline-block;cursor:pointer" onclick="addEducationDetails()" class="fa fa-plus-circle big-add big-add-expand"></span></div>
												</center>
											
												</div>
												<hr class="colorgraph">
											</div>
											<div class="col-md-6">
												<div class="row">
												<h3 style="float:left">Pengalaman / Penghargaan </h3>
											</div>
												<div class="row">
													@if($awards->count()==0)
													<div class="achievement dummy-achievement" id="awards_dummy_detail">
														
														<h5 style="font-style:italic">e.g.</h5>
														<h5 style="margin-left:10px; font-style:italic">2016 — Bergabung dengan Mussy.co!</h5>
														<h5 style="margin-left:10px; font-style:italic">2011 — Panasonic global awards : Penyanyi terpopuler</h5>
														<h5 style="margin-left:10px; font-style:italic">2010 — Triangle performance diploma (distinction)</h5>
													
														
													</div>
													@endif
													
													<div class="achievement dummy-achievement" id="awards_detail">
													@if($awards->count()>0)	
														@foreach($awards as $award)
														<div class="row detailhover" id="awarddetail_{{$award->id}}" >
														<div class="col-md-10"><h5 style="margin-left:10px; font-style:italic">{{$award->year_get}} — {{$award->description}}</h5></div>
														<div class="col-md-2">
															<i class='fa fa-pencil iconsetting' onclick="doEditAward({{$award->id}},'{{$award->year_get}}','{{$award->description}}')" ></i><i class='fa fa-trash-o iconsetting' onclick="doDeleteAward({{$award->id}})" ></i>
														</div>
														</div>
														@endforeach
													@endif
													</div>
													
												<div id="addAchievementDetails" class="row">
													<div style="white-space:nowrap; display:flex;display:-webkit-flex;display:-ms-flexbox; margin:15px; margin-bottom:0px;" class="form-group">
														<input type="text" placeholder="Year" style="width:80px; border-right:none; border-top-right-radius:0; border-bottom-right-radius:0;" id="award_year_get" class="form-control form-control">
														<input type="text" placeholder="Description" style="width:80%; flex-grow:1; -webkit-flex-grow:1; -ms-flex:1; border-top-left-radius:0; border-bottom-left-radius:0;" id="award_description" class="form-control input-rightbtn form-control">
														<button onclick="submitAwardDetails(1)" style="border-top-right-radius:3px !important; border-bottom-right-radius:3px !important;" class="btn btn-primary input-rightbtn">Add</button>
													</div>
												</div>
											</div>
											<hr class="colorgraph">
											</div>
										</div>
										<div class="row">
											<h3 style="float:left">Biografi</h3>
										</div>
										
										<div class="row" style="cursor:pointer;">
										  <p id="info_bio">
										    <em>Double click/tap text di bawah ini untuk mengubah</em>
										    <em style="display: none">ctr | cmd + &uarr;/&darr; untuk mengubah ukuran font</em>
										  </p>
										  										   
										    <p class="editable_bio">
										   @if(trim(Auth::user()->full_bio)=="")
										      <b>Kamu</b> bisa mengubah biografimu di sini. Silakan tulis apa pun yang dirasa menarik untuk ditampilkan dalam profil <i>kamu</i>.
										 @else
										 {!! 	Auth::user()->full_bio !!}
										
										  @endif	
										      									    	
										    </p>										
										</div>
										<hr class="colorgraph">
									</div>
									<div class="tab-pane" id="two">
										<div class="content youtube-content"><h4>Add a YouTube video</h4>
                                            <div id="youTubeDetails" style="margin-bottom: 30px;" class="row">
                                                <div style="display: inline-flex; display: -webkit-inline-flex; display: -ms-inline-flexbox" class="col-sm-12">
                                                    <img src="/img/icons/youtube-mini.png" width="34" height="34" style="margin-right:10px">
                                                    <input rows="1" data-video-type="youtube" placeholder="Enter a YouTube URL here  e.g.  https://www.youtube.com/watch?v=dQw4w9WgXcQ" style="width:50%; flex-grow:2; -webkit-flex-grow:2; -ms-flex:2;"  class="form-control input-rightbtn js-upload-youtube" id="inputyoutubevid"><btn style="width:100px;max-height: 34px;" data-video-type="youtube" class="btn btn-info add-youtube-btn input-rightbtn add-video-btn" onclick="addMedia('youtubevid')">Add</btn>
                                                </div>
                                            </div><h4>Add your YouTube channel</h4>
                                            <div id="youTubeChannelDetails" style="margin-bottom: 30px;" class="row">
                                                <div style="display: inline-flex; display: -webkit-inline-flex; display:-ms-inline-flexbox;" class="col-sm-12">
                                                    <img src="/img/icons/youtube-mini.png" width="34" height="34" style="margin-right:10px">
                                                    <input rows="1" placeholder="Enter YouTube channel URL or ID here  e.g.  https://www.youtube.com/channel/UCNsauJQIhdlpg" style="width:50%; flex-grow:2; -webkit-flex-grow:2; -ms-flex:2;" class="form-control input-rightbtn" id="inputyoutubechannel"><btn style="width:100px;max-height: 34px;" class="btn btn-info add-youtube-btn input-rightbtn" onclick="addMedia('youtubechannel')">Add</btn>
                                                </div>
                                            </div>
                                                <h4>Add a Vimeo video</h4>
                                                <div id="youTubeDetails" style="margin-bottom: 10px;" class="row">
                                                    <div style="display: inline-flex; display: -webkit-inline-flex; display: -ms-inline-flexbox" class="col-sm-12">
                                                        <img src="/img/icons/vimeo_icon.png" width="34" height="34" style="margin-right:10px">
                                                        <input rows="1" data-video-type="vimeo" placeholder="Enter a Vimeo URL here  e.g. https://vimeo.com/120469122" id="inputvimeovid" style="width:50%; flex-grow:2; -webkit-flex-grow:2; -ms-flex:2;" class="form-control input-rightbtn js-upload-vimeo">
                                                        <btn style="width:100px;max-height: 34px;" data-video-type="vimeo" class="btn btn-info add-video-btn input-rightbtn " onclick="addMedia('vimeovid')">Add</btn>
                                                    </div>
                                            </div>
                                        </div>
                                        @if ($youtubevids->count()>0)
                                         <div class="content" id="youtube-vid">
                                            <div class="row">
                                                @foreach($youtubevids as $youtubevid)
                                                                                                   
                                                        <div class="col-md-6">
                                                            <div class="row"><iframe src="{{$youtubevid->link}}" style="    height: 300px;
    width: 90%;"allowfullscreen></iframe></div>
                                                        </div>
                                                       
                                                    
                                                @endforeach
                                        </div>
                                        </div>
                                        @endif
                                         @if ($youtubechannels->count()>0)
                                        <div class="content" id="youtube-channel">
                                             <div class="row">
                                              @foreach($youtubechannels as $youtubechannel)
                                                     <div class="col-md-12">
                                                        <div class="row"><iframe src="{{$youtubechannel->link}}" style="height:400px;width: 90%;" allowfullscreen  ></iframe></div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                         @if ($vimeovids->count()>0)
                                        <div class="content" id="vimeo-vid">
                                            <div class="row">
                                         @foreach($vimeovids as $vimeovid)
                                        
                                              
                                                <div class="col-md-4">
                                                    <div class="row"><iframe src="{{$vimeovid->link}}" allowfullscreen></iframe></div>
                                                </div>
                                               
                                           
                                        @endforeach
                                         </div>
                                        </div>
                                        @endif
									</div>
									<div class="tab-pane" id="three">
										<p>
											Cu cum commodo regione definiebas. Cum ea eros laboramus, audire deseruisse his at, munere aeterno ut quo. Et ius doming causae philosophia, vitae bonorum intellegat usu cu.
										</p>
									</div>
			</div>
							 
			</div>




						</div>
						
</div>
@endsection