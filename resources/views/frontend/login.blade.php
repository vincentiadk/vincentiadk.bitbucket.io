@extends('frontend.layouts.masterblank')
@section('content')
@if ($flash = session('message'))
<div class="flash-message" class="alert alert-success" role="alert" >
{{$flash}}	
</div>
@endif
<section id="content">
			<div class="container">

				<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
						<form role="form" class="register-form" action="/loginku" method="post">
							 {{ csrf_field() }}
							<h2>Sign in <small>manage your account</small></h2>
							<hr class="colorgraph">

							<div class="form-group">
								<input type="text" name="username" id="username" class="form-control input-lg" placeholder="Email / Username" tabindex="4" required>
							</div>
							<div class="form-group">
								<input type="password" class="form-control input-lg" id="password" placeholder="Password" name="password" required>
							</div>

							<div class="row">
								<div class="col-xs-4 col-sm-3 col-md-3">
									<span class="button-checkbox">
						<button type="button" class="btn" data-color="info" tabindex="7">Remember me</button>
                        <input type="checkbox" name="remember" id="t_and_c" class="hidden" value="1">
					</span>
								</div>
							</div>

							<hr class="colorgraph">
							<div class="row">
								<div class="col-xs-12 col-md-6"><input type="submit" value="Sign in" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
								<div class="col-xs-12 col-md-6">Don't have an account? <a href="/registration">Register</a></div>
								@if ($flash = session('message-error'))
								<div class="col-xs-12 col-md-6">Lupa password? <a href="/resetpassword">Click di sini </a></div>
								@endif
							</div>
							
						</form>
						<br/>
						@if ($flasherror = session('message-error'))
							<div id="flash-message" class="alert alert-danger" role="alert" >
							{{$flasherror}}	
							</div>
							@endif
					</div>
				</div>

			</div>
		</section>
@endsection