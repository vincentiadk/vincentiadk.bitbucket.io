		<header>
			<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<ul class="topleft-info">
								<li><i class="fa fa-phone"></i> +62 088 999 123</li>
							</ul>
						</div>
						<div class="col-md-6">
							<div id="sb-search" class="sb-search">
								<form>
									<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">
									<input class="sb-search-submit" type="submit" value="">
									<span class="sb-icon-search" title="Click to start searching"></span>
								</form>
							</div>


						</div>
					</div>
				</div>
			</div>

			<div class="navbar navbar-default navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/"><img src="/img/Logo-Mussyco.png" alt="" width="199" height="52" /></a>
					</div>
					<div class="navbar-collapse collapse ">
						<ul class="nav navbar-nav">
							<li><a href="/about">Tentang Mussy.co</a></li>
							<li><a href="/howto">Cara Kerja</a></li>
							<li><a href="/job">Pasang Job</a></li>
						
							@if(!Auth::check())<li><div class="cta-btn" style="padding: 0px;  margin-top: 0px; ">
								<a href="/login" class="btn btn-theme btn-lg">Login </a>
							</div></li>
							@endif
							@if(Auth::check())<li><div class="cta-btn" style="padding: 0px;  margin-top: 0px; ">
								<a href="/musician" class="btn btn-theme btn-lg">Dashboard </a>
							</div></li>
							@endif
						</ul>
					</div>
				</div>
			</div>
		</header>