@extends('frontend.layouts.masterblank')
@section('content')
@if ($flash = session('message'))
<div id="flash-message" class="alert alert-success" role="alert" >
{{$flash}}	
</div>
@endif
<section id="content">
			<div class="container">

				<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
						<form role="form" class="register-form" action="/resetpassword" method="post">
							 {{ csrf_field() }}
							<h2>Reset Password</h2>
							<hr class="colorgraph">

							<div class="form-group">
								<input type="text" name="username" id="username" class="form-control input-lg" placeholder="Masukkan email / username kamu di sini" tabindex="4" required>
							</div>
							

							<hr class="colorgraph">
							<div class="row">
								<div class="col-xs-12 col-md-6"><input type="submit" value="Kirim Link Reset" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
								
							</div>
							
						</form>
					</div>
				</div>

			</div>
		</section>
@endsection