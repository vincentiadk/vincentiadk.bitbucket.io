@extends('frontend.layouts.masterblank')
@section('content')
<script src="/js/jquery-easing.js" type="text/javascript" ></script>
<script src="/js/bootstrap-tagsinput.js" type="text/javascript" ></script>
<script src="/js/typeahead.js" type="text/javascript" ></script>
 <script src="/js/jquery-ui.min.js" type="text/javascript" ></script>
<script src="/js/jquery.validate.js" type="text/javascript" ></script>

<link href="css/jquery.typeahead.css" rel="stylesheet" />
<link href="css/bootstrap-tagsinput.css" rel="stylesheet" />
  <link href="css/cubeportfolio.min.css" rel="stylesheet" />
  <link href="css/jquery-ui.min.css" rel="stylesheet" />
  
<style>
@import url(https://fonts.googleapis.com/css?family=Montserrat);

/* css class for the registration form generated errors */
.label-info {
    background-color: #B8B2A1;
}
.error{
  color:red;
  font-size:9pt;
}
.twitter-typeahead{
  width:100%;
}
.profilepress-reg-status {
  border-radius: 6px;
  font-size: 17px;
  line-height: 1.471;
  padding: 10px 19px;
  background-color: #e74c3c;
  color: #ffffff;
  font-weight: normal;
  display: block;
  text-align: center;
  vertical-align: middle;
  margin: 5px 0;
}
/*form styles*/

#msform {
 /* width: 400px; */
 margin: 50px auto 550px;
 text-align: center;
 position: relative;
}

#msform fieldset {
  background: white;
  border: 0 none;
  border-radius: 3px;
  box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
  padding: 20px 30px;
  box-sizing: border-box;
  width: 80%;
  margin: 0 10%;
  /*stacking fieldsets above each other*/
  
  position: absolute;
}
/*Hide all except first fieldset*/

#msform fieldset:not(:first-of-type) {
  display: none;
}
/*inputs*/

#msform input,
#msform textarea {
  padding: 15px;
  border: 1px solid #ccc;
  border-radius: 3px;
 /* margin-bottom: 10px; */
  width: 100%;
  box-sizing: border-box;
  font-family: montserrat;
  color: #2C3E50;
  font-size: 13px;
}
/*buttons*/

#msform .action-button {
  width: 100px;
  background: #665A4A;
  font-weight: bold;
  color: white;
  border: 0 none;
  border-radius: 1px;
  cursor: pointer;
  padding: 10px 5px;
  margin: 10px 5px;
}

#msform .action-button:hover,
#msform .action-button:focus {
  box-shadow: 0 0 0 2px white, 0 0 0 3px #27AE60;
}
/*headings*/

.fs-title {
  font-size: 15px;
  text-transform: uppercase;
  color: #2C3E50;
  margin-bottom: 10px;
}

.fs-subtitle {
  font-weight: normal;
  font-size: 13px;
  color: #666;
  margin-bottom: 20px;
}
/*progressbar*/

#progressbar {
  margin-bottom: 30px;
  overflow: hidden;
  /*CSS counters to number the steps*/
  
  counter-reset: step;
}

#progressbar li {
  list-style-type: none;
  color: #616161;
  text-transform: uppercase;
  font-size: 20px;
  width: 33.33%;
  float: left;
  position: relative;
}

#progressbar li:before {
  content: counter(step);
  counter-increment: step;
  width: 30px;
  line-height: 30px;
  display: block;
  font-size: 20px;
  color: #333;
  background: white;
  border-radius: 3px;
  margin: 0 auto 5px auto;
}
.bootstrap-tagsinput input{
  width:relative !important;
  border:none !important;
}
#progressbar li:first-child:after {
  /*connector not needed before the first step*/
  
  content: none;
}
/*marking active/completed steps green*/
/*The number of the step and the connector before it = green*/

#progressbar li.active:before,
#progressbar li.active:after {
  background: #665A4A;
  color: white;
}
.tt-query {
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
  -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}

.tt-hint {
  color: #999
}

.tt-dropdown-menu {    /* used to be tt-dropdown-menu in older versions */
  width: 422px;
  margin-top: 4px;
  padding: 4px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
  -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
  box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.tt-suggestion {
  padding: 3px 20px;
  line-height: 24px;
}

.tt-suggestion.tt-cursor,.tt-suggestion:hover {
  color: #fff;
  background-color: #0097cf;

}

.tt-suggestion p {
  margin: 0;
}
</style>
<script type="text/javascript">//jQuery time
$(document).ready(function() { 
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val(),
    },
});
  var current_fs, next_fs, previous_fs; //fieldsets
  var left, opacity, scale; //fieldset properties which we will animate
  var animating; //flag to prevent quick multi-click glitches
  var arr = [];var arrProvince = [];
  $.ajax({
    type: "GET",
    url:'/registration/getInstruments/',
    dataType: 'json',
    success: function(data) {
     for(var x in data){
      arr.push(data[x]["name"]);
    }
    var instrumentsname = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: $.map(arr, function (instrument) {
        return {
          name: instrument
        };
      })
    });
    instrumentsname.initialize();
    $('.colinput >> input').tagsinput({
      typeaheadjs: [{
        minLength: 1,
        highlight: true
      },{
        minlength: 3,
        name: 'instrumentsname',
        displayKey: 'name',
        valueKey: 'name',
        source: instrumentsname.ttAdapter()
      }],
      freeInput: true
    });

  } 
});
  var arrRegencies=[];
$.ajax({
    type: "GET",
    url:'/registration/getRegencies/',
    dataType: 'json',
    success: function(data) {
     for(var x in data){
      arrRegencies.push(data[x]["name"]);
    }
    var regenciesname = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: $.map(arrRegencies, function (regencies) {
        return {
          name: regencies
        };
      })
    });
    regenciesname.initialize();
    $('.colinput2 >> input').tagsinput({
      typeaheadjs: [{
        minLength: 1,
        highlight: true
      },{
        minlength: 3,
        name: 'regenciesname',
        displayKey: 'name',
        valueKey: 'name',
        source: regenciesname.ttAdapter()
      }],
      freeInput: true
    });
    $("#location").autocomplete({
                    source: arrRegencies,
                    minLength: 1
                });
  } 
});
var arrGenres=[];
$.ajax({
    type: "GET",
    url:'/registration/getGenres/',
    dataType: 'json',
    success: function(data) {
     for(var x in data){
      arrGenres.push(data[x]["name"]);
    }
    var genrename = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: $.map(arrGenres, function (genre) {
        return {
          name: genre
        };
      })
    });
    genrename.initialize();
    $('.colgenre>> input').tagsinput({
      typeaheadjs: [{
        minLength: 1,
        highlight: true
      },{
        minlength: 3,
        name: 'genrename',
        displayKey: 'name',
        valueKey: 'name',
        source: genrename.ttAdapter()
      }],
      freeInput: true
    });
  } 
});
 jQuery.validator.addMethod("noSpace", function(value, element) { 
      return value == '' || value.trim().length != 0;  
    }, "No space please and don't leave it empty!");

var form = $("#registrationform");
var valid = false;
  $(".next").click(function() {
    if($('#account_setup').css('display') == 'block'){   
    form.validate({
      rules: 
      {   
        username: {
          noSpace: true,
          required: true,
          minlength: 5,
        },    
        fullname: {
          required: true,
          minlength: 6,
        },
        password:{
          required: true,
          minlength: 6,
        },
        cpass:{
          required: true,
          minlength: 6,
          equalTo: "#password",
        },          
        email:{
          required:true,
          email: true,
          remote: 
            {
              url: '/registration/cekEmail',
              type: "POST",
              dataType: 'JSON',
              data:
              {
               email: function(){ return $('#email').val() ; }
              },
            },    
        },
      },
      messages: {       
        username: {
          required: "Username minimal 5 karakter",
        },
        fullname: {
          required: "Nama lengkap minimal 6 karakter",
        },
        password: {
          required: "Password harus aman, minimal 6 karakter",
        },
         cpass: {
          required: "Tidak boleh kosong",
          equalTo: "Konfirmasi password tidak cocok"
        },        
          email: {
          required: "Masukkan alamat emailmu",
          email: "Alamat email tidak valid",
          remote: "Email sudah pernah didaftarkan",
        },
      },
    });
    valid = form.valid();
  }
    if($('#mussical_skills').css('display') == 'block')
    {
      if ($('.colinput >> input').val()==""){
        valid=false;
        $('#skills-error').css('display','block');
      }
      else {
        valid=true;
        $('#skills-error').css('display','none');
      };
      form.validate({
          rules: {   
           musisi_type: { 
          required: true,
        },    
            location: {
              required: true,
            },
          },
          messages: { 
           musisi_type: { 
          required: "Pilih tipe musisi!" 
        },      
            location: {
              required: "Isi tempat tinggalmu.",
            },        
          },
      });
      if(valid)
       {
        valid=form.valid();
       };
       if(!valid && form.valid()==false){
        valid=false;
       }
    }
    if($('#personal_details').css('display') == 'block')
    {    
      form.validate({
        rules: {
          mobilephone: {
            required: true,
          },
        },
        messages: {
          mobilephone: {
            required: "Please enter your mobile phone number",
          },
        }
      });
     valid = form.valid();
  }
   if (valid == true)
     {
        if (animating) return false;
        animating = true;
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

      //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
          opacity: 0
        }, {
          step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
              'transform': 'scale(' + scale + ')'
            });
            next_fs.css({
              'left': left,
              'opacity': opacity
            });
          },
          duration: 800,
          complete: function() {
            current_fs.hide();
            animating = false;
          },
          //this comes from the custom easing plugin
          easing: 'easeInOutBack'
        });
    }
  });
  getInstruments=function(){
    $("input[name=skills]").val($('.colinput >> input').val());
     $("input[name=tempatperform]").val($('.colinput2 >> input').val());
  };
  $(".previous").click(function() {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({
      opacity: 0
    }, {
      step: function(now, mx) {
        //as the opacity of current_fs reduces to 0 - stored in "now"
        //1. scale previous_fs from 80% to 100%
        scale = 0.8 + (1 - now) * 0.2;
        //2. take current_fs to the right(50%) - from 0%
        left = ((1 - now) * 50) + "%";
        //3. increase opacity of previous_fs to 1 as it moves in
        opacity = 1 - now;
        current_fs.css({
          'left': left
        });
        previous_fs.css({
          'transform': 'scale(' + scale + ')',
          'opacity': opacity
        });
      },
      duration: 800,
      complete: function() {
        current_fs.hide();
        animating = false;
      },
      //this comes from the custom easing plugin
      easing: 'easeInOutBack'
    });
  });
});
</script>
<form method="post" action="/registration" data-toggle="validator" id="registrationform">
  {{ csrf_field() }}
  <div id="msform" class="col-lg-10">

    <!-- progressbar -->
    <ul id="progressbar">
      <li class="active">Account Setup</li>
      <li>Mussical skills</li>
      <li>Personal Details</li>
    </ul>
    <hr class="colorgraph">
    <!-- fieldsets -->
    
    <div class="row">
    <fieldset id="account_setup">
      <h2 class="fs-title">Create your account</h2>
      <h3 class="fs-subtitle">Your account detail</h3>
     
      <div class="form-group">
          <input type="text" name="username" placeholder="Username" /> </div>
      <div class="form-group">
          <input type="text" name="fullname" placeholder="Full name" />  </div>
        <div class="form-group">
            <input type="email" class="form-control" name="email" placeholder="Email" id="email"/>        
        </div>
        <div class="form-group">
          <input type="password" name="password" placeholder="Password" id="password"  />         
        </div>
        <div class="form-group">
          <input type="password" name="cpass" placeholder="Confirm Password"   />        
        </div>
          <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="mussical_skills">
          <h2 class="fs-title">Mussical skills</h2>
          <h3 class="fs-subtitle">What are your musical skills?</h3>
      <div class="form-group">
        <select name="musisi_type" class="form-control" id="musisi_type">
          <option value="">-Pilih tipe musisi-</option>
          <option value="1">Solo</option>
          <option value="2">Grup</option>
        </select>
      </div>    
       <div class="form-group" id="form-grouptype">
        <select name="grouptype_id" class="form-control" id="grouptype_id">
          <option value="">-Pilih jenis grup musik mu-</option>
          @foreach ($grouptype as $type)
            <option value="{{ $type->id }}">{{ $type->name }}</option>
          @endforeach
        </select>
      </div>      
       <div class="form-group">
            <input  type="text" name="location" placeholder="Dimana tempat tinggalmu?" id="location" /></div>
          <div class="colinput">
            <input data-role="tagsinput" type="text" name="skills" placeholder="Instrument / Role"  /></div>
            <div id="skills-error" class="error" style="display:none;max-width: 100%;">Mohon masukkan minimal satu jenis instrument / role.</div>
            <div class="colgenre">
            <input data-role="tagsinput" type="text" name="genres" placeholder="Apa genre musik kamu?"  /></div>
            <div id="genres-error" class="error" style="display:none;max-width: 100%;">Mohon masukkan minimal satu jenis genre musik.</div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
          </fieldset>
          <fieldset id="personal_details">
            <h2 class="fs-title">Personal Details</h2>
            <h3 class="fs-subtitle">We will never sell it</h3>
            <input type="text" name="mobilephone" placeholder="Mobile phone" />
            <div class="colinput2">
              <input type="text" data-role="tagsinput"  name="performlocation" placeholder="Where do you want to perform? (base on province)" />
          </div>

            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="submit" name="submit" class="submit action-button" value="Submit" onclick="getInstruments();"/>
          </fieldset>
        </div>

          
        </div>

      </form>
      @endsection

