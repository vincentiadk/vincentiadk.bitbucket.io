$(window).load(function() {
    if ($('.instruments-btns').length) {
        var instHeightNum = Math.max(0, $('.side-nav').height() - ($('.instruments-btns').offset().top - ($('.side-nav').offset().top - (tall ? 103 : 51))));
        var instHeight = instHeightNum + 'px';
        $('.instruments-btns').css('height', instHeight);
        $('.instruments-padding').css('height', (instHeightNum - ($('.instruments-padding').offset().top - $('.instruments-btns').offset().top)) + 'px');
        $('.instruments-btns').perfectScrollbar();
        $(window).resize(function() {
            var instHeightNum = Math.max(0, $('.side-nav').height() - ($('.instruments-btns').offset().top - ($('.side-nav').offset().top - (tall ? 103 : 51))));
            var instHeight = instHeightNum + 'px';
            $('.instruments-btns').css('height', instHeight);
            $('.instruments-padding').css('height', (instHeightNum - ($('.instruments-padding').offset().top - $('.instruments-btns').offset().top)) + 'px');
            $('.instruments-btns').perfectScrollbar('update');
            $('.instruments-btns').sortable('refresh');
        });
        reorderContacts();
        helperBackground = '';
        $('.instruments-btns').sortable({
            items: ".instrument-link",
            scroll: false,
            opacity: 0.9,
            zIndex: 20000,
            start: function(event, ui) {
                $('.instruments-btns btn').each(function() {
                    $(this).addClass('dragging-cursor');
                });
                $('.delete-inst-overlay').fadeIn(100);
            },
            stop: function(event, ui) {
                $('.instruments-btns btn').each(function() {
                    $(this).removeClass('dragging-cursor');
                });
                $('.delete-inst-overlay').fadeOut(100);
                recolourInstruments();
                resetInstrumentPadding();
                $('.instruments-btns').perfectScrollbar('update');
            },
            helper: function(event, el) {
                var cl = el.clone();
                cl.children().css('-webkit-box-shadow', '0px 0px 16px 2px rgba(0,0,0,0.75)');
                cl.children().css('-moz-box-shadow', '0px 0px 16px 2px rgba(0,0,0,0.75)');
                cl.children().css('box-shadow', '0px 0px 16px 2px rgba(0,0,0,0.75)');
                cl.children().addClass('dragging-cursor');
                cl.children().children().each(function() {
                    $(this).show();
                });
                return cl;
            },
            beforeStop: function(event, ui) {
                if (event.pageX >= 240) {
                    ui.item.remove();
                    alertify.error("Instrument deleted");
                }
            },
            sort: function(event, ui) {
                if (event.pageX >= 240) {
                    if (ui.helper && !helperBackground) {
                        helperBackground = ui.helper.children().css('background');
                        ui.helper.children().css('background', 'red');
                        $('.instruments-btns').sortable("option", "revert", false);
                    }
                } else {
                    if (ui.helper && helperBackground) {
                        ui.helper.children().css('background', helperBackground);
                        helperBackground = null;
                        $('.instruments-btns').sortable("option", "revert", 100);
                    }
                }
            },
            forceHelperSize: true,
            forcePlaceholderSize: true,
            placeholder: "ui-state-highlight sortable-placeholder",
            appendTo: 'body',
            tolerance: 'pointer',
            disabled: true,
            revert: 100
        });
    }
    if (user) {
        if (user.youtubeChannel && user.youtubeChannel.length) {
            if (user.youtubeChannel && user.youtubeChannel.length > 21) {
                $('#y-channel').ytv({
                    channelId: (user.youtubeChannel && user.youtubeChannel.length > 23 ? user.youtubeChannel : (user.youtubeChannel && user.youtubeChannel.length > 21 ? 'UC' + user.youtubeChannel : user.youtubeChannel)),
                    accent: '#008D54',
                    browsePlaylists: true,
                    controls: true,
                    autoplay: false,
                    responsive: true
                });
            } else {
                $('#y-channel').ytv({
                    user: (user.youtubeChannel && user.youtubeChannel.length > 23 ? user.youtubeChannel : (user.youtubeChannel && user.youtubeChannel.length > 21 ? 'UC' + user.youtubeChannel : user.youtubeChannel)),
                    accent: '#008D54',
                    browsePlaylists: true,
                    controls: true,
                    autoplay: false,
                    responsive: true
                });
            }
        }
    }
    $('.js-reviews-more-text').on('click', function() {
        var $text = $(this).prev('.reviews-text-overflow');
        console.log('ding');
        $($text).removeClass('reviews-text-overflow').addClass('reviews-text');
        $(this).remove();
    });
});
var widget;
var noSoundcloud = true;
if (user.soundcloud && user.soundcloud.length && (!publicProfile || user.privacy.media)) {
    noSoundcloud = false;
    SC.initialize({
        client_id: "42594aad73d9b7596f9ae064e5d009a4"
    });
    SC.oEmbed(user.soundcloud, {
        color: "ff0066"
    }, function(oEmbed) {
        if (oEmbed) {
            $('#s').html(oEmbed.html);
            widget = SC.Widget(document.querySelector('#s iframe'));
        }
    });
}
var player;
var x = user.soundcloud;
var instJS = [];
var originalUrl;
var jobsStringFormat = 'h.mma, dddd Do MMMM YYYY';
$(document).ready(function() {
    if ($('#bookLocation').length)
        $('#bookLocation').geocomplete();
    $('.js-open-enquiry-modal').on('click', openEnquiryModal);
    $('.js-price-save').on('click', postPrice);
    $('.js-price-delete').on('click', deletePrice);
    $('.js-enable-booking ').on('click', enableBooking);
    $('.js-save-notes').on('click', updateNotes);
    $(".qtip-hover[title]").qtip({
        style: {
            tip: true,
            classes: 'qtip-bootstrap qtip-jobadd'
        },
        position: {
            my: ($(window).width() < 768 ? 'bottom center' : 'top center'),
            at: ($(window).width() < 768 ? 'top center' : 'bottom center')
        },
        show: {
            delay: 0
        },
        hide: {
            event: 'blur mouseleave',
            delay: 0
        }
    });
    $('input.form-control[name="bookingDate"]').datetimepicker({
        format: jobsStringFormat,
        ignoreReadonly: true,
        stepping: 5,
        minDate: moment().startOf('day').toDate(),
        sideBySide: false,
        collapse: false,
        debug: false,
        widgetParent: $('.js-date-picker'),
        defaultDate: Date.now(),
    });
    $('.booking-form input').on('keyup', function() {
        var input = $(this);
        if (input.val().length === 0) {
            input.addClass('empty');
        } else {
            input.removeClass('empty');
        }
    });
    $('.add-video-btn').on('click', updateYouTube);
    $('.js-delete-track').on('click', deleteTrack);
    tracks.forEach(function(track, i) {
        if ($('#track-player-' + i).length) {
            var ap = new APlayer({
                element: document.getElementById('track-player-' + i),
                narrow: false,
                autoplay: false,
                showlrc: false,
                mutex: true,
                theme: '#e6d0b2',
                loop: true,
                music: {
                    title: track.title,
                    author: user.fullname,
                    url: track.url,
                    pic: '/img/playerbg.png'
                }
            });
            ap.init()
        }
    });
    window.addEventListener("popstate", function(e) {
        if (!e.state) return;
        tabnumber = e.state.tab;
        tabClicked(tabnumber, 'tabname', -1);
    }, false);
    var tab = parseInt(location.search.split('tab=')[1]).toString();
    if (parseInt(tab) == 5)
        window.location.href = '/dashboard/settings';
    else if ($('.tab-' + tab).length)
        $('.tab-' + tab).click();
    else
        showFirstTab();
    if ($(window).width() > 768 && !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Opera Mini|IEMobile|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)) {
        skroll = skrollr.init({
            forceHeight: false,
            smoothScrolling: false
        });
    }
    $('#ambassadorButton').click(function() {
        $.ajax({
            type: 'PUT',
            url: "/users/" + user._id + "/update",
            contentType: 'application/json',
            data: JSON.stringify({
                ambassador: true
            }),
            success: function(data) {
                alertify.alert("Update successful");
            }
        });
        return false;
    });
    $('#addReferrer').click(function() {
        var referrer = $('#referrer').val().trim();
        if (referrer && referrer.length) {
            $.ajax({
                type: 'POST',
                url: "/users/" + user._id + "/referrer",
                contentType: 'application/json',
                data: JSON.stringify({
                    email: referrer
                }),
                success: function(data) {
                    alertify.alert("Update successful");
                }
            });
            return false;
        }
    });
    $('#bannerSendLink').click(function() {
        $.ajax({
            type: 'POST',
            url: "/users/" + user._id + "/reverify",
            contentType: 'application/json',
            data: JSON.stringify({}),
            success: function(data) {
                alertify.alert("Thanks - we've sent you a new activation email. These can take up to 20 minutes to arrive, but please also check your spam folder.");
            }
        });
        return false;
    });
    $('#bannerChangeEmail').click(function() {
        alertify.confirm("Redirecting you to the settings page, where you can adjust your email", function(e) {
            if (e)
                window.location.href = "/dashboard/settings";
        });
        return false;
    });
    $('.inst-autocomplete').autocomplete({
        source: instrumentList
    });
    $('.placeUpdate').autocomplete({
        source: educationList
    });
    $('#placeAdd').autocomplete({
        source: educationList
    });
    $(".mobile-cover").css("height", ($(".side-nav").height() + 12));
    $('.instrument-link').click(function(e) {
        if ($('.edit-insts').text().toLowerCase().indexOf('save') > -1) {
            e.preventDefault();
            return false;
        }
    });
    $('.circle-container').click(function() {
        if (!publicProfile) {
            window.open("/invite");
        }
    });
    var tabsTop = $(".navTabs").offset().top;
    var hiddenEditCover = false;
    $(window).scroll(function() {
        var win = $(this);
        if (win.width() >= 768) {
            var $stick = $(".navTabs");
            var scrollTop = $(this).scrollTop();
            if (scrollTop > 0 && !hiddenEditCover && !$('.cancel-cp').length) {
                hiddenEditCover = true;
                $('.edit-cover-photo').animate({
                    opacity: 0
                }, 50);
            } else if (scrollTop < 1 && hiddenEditCover && !$('.cancel-cp').length) {
                hiddenEditCover = false;
                $('.edit-cover-photo').animate({
                    opacity: 0.5
                }, 50);
            }
            if (scrollTop >= tabsTop - $('.main-navbar').height()) {
                var holderHeight = $('.navtab-holder').height();
                if (!$("#scroll-placeholder").length)
                    $stick.before('<div id="scroll-placeholder" style="width:50px; height: ' + holderHeight + 'px;"></div>');
                $stick.css({
                    'position': 'fixed',
                    'top': $('.main-navbar').height() + 66,
                    'left': '50%',
                    'margin-left': 120 - ($stick.width() / 2),
                    'z-index': '999',
                    'box-shadow': '2px 1px 20px 1px rgba(34,34,34,0.75)',
                    '-moz-box-shadow': '2px 1px 20px 1px rgba(34,34,34,0.75)',
                    '-webkit-box-shadow': '2px 1px 20px 1px rgba(34,34,34,0.75)'
                });
                $('.nav-button').attr('style', 'border-radius:0px; border-bottom:1px solid #ccc !important; background: rgba(255,255,255,0.85);');
                $('.nav-button').addClass('btn-floating');
            } else {
                $stick.css({
                    'position': 'relative',
                    'top': '0',
                    'z-index': '0',
                    'margin-left': '',
                    'left': '0',
                    'box-shadow': '',
                    '-moz-box-shadow': '',
                    '-webkit-box-shadow': ''
                });
                $('.nav-button').attr('style', '');
                $('.nav-button').removeClass('btn-floating');
                $("#scroll-placeholder").remove();
            }
        }
    });
    $('#addmeinstrument').click(function() {
        var instrument = $('#addmeinst').val().trim();
        if (!instrument || !instrument.length)
            return;
        $('#addmeinstrument').addClass('disabled');
        $.ajax({
            type: 'POST',
            data: JSON.stringify({
                name: $('#groupName').val().trim(),
                username: user.fullname,
                instrument: instrument
            }),
            contentType: 'application/json',
            url: '/users/' + user._id + '/groupadd/' + addingGroupId,
            success: function(data) {
                $('.overlay-instrument').dialog('close');
                $('#addmeinstrument').removeClass('disabled');
                if (data == 'auto_added') {
                    alertify.success("Group added. Please wait while the page reloads...");
                    location.reload();
                } else if (data == 'OK' || data == 200)
                    alertify.alert("Thanks - you will be added to the group as soon as your request has been verified");
                else
                    alertify.alert("Sorry, something went wrong! Please leave feedback to tell us what happened.");
                addingGroupId = '';
                $('.btn-addgroup').attr('disabled', true);
                $('.new-group').show();
                $('.new-group').removeClass('no-transition');
                $("#groupName").css('width', '200px');
                $("#groupName").val('');
                $('.addGroupBtn').addClass('no-transition');
                $('.addGroupBtn').addClass('performer-thumb');
                $('.addGroupBtn').css('border', '');
                $('.addGroupForm').fadeOut(300, function() {
                    $('.addGroupBtn').animate({
                        width: 140
                    }, 300, 'swing', function() {
                        $('.big-group-plus').fadeIn(200);
                        $('.concert-add-txt').fadeIn(200, function() {
                            $('.addGroupBtn').click(function() {
                                addGroupForm();
                            });
                        });
                    });
                });
            }
        });
        if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking) {
            mixpanel.track("Group clicked add me", {
                "Current URL": document.URL,
                "Group": $('#groupName').val().trim(),
                "From": "Profile"
            });
        }
    });
    $(".qtip-title[title]").qtip({
        style: {
            classes: 'qtip-centred qtip-dark qtip-dark-adjust'
        },
        position: {
            my: 'top center',
            at: 'bottom center'
        },
        show: {
            event: 'mouseenter',
            delay: 0,
            effect: function() {
                $(this).show('fade', 100);
            }
        },
        hide: {
            event: 'mouseleave',
            delay: 0,
            effect: function() {
                $(this).hide('fade', 100);
            }
        }
    });
    $('.qtip-builder').each(function() {
        $currentLink = $(this);
        var pos;
        switch ($currentLink.data('orientation')) {
            case 'bottom':
                pos = {
                    my: 'top left',
                    at: 'bottom left'
                };
                break;
            case 'right':
                pos = {
                    my: 'left center',
                    at: 'right center'
                };
                break;
        }
        var fixed = $currentLink.data('fixed') === "true" ? 'qtip-fixed' : '';
        $currentLink.qtip({
            content: $currentLink.attr('title'),
            style: {
                classes: 'qtip-dark qtip-dark-adjust qtip-centred ' + fixed
            },
            position: pos
        });
    });
    $(".qtip-title-dark[title]").qtip({
        style: {
            classes: 'qtip-dark qtip-dark-adjust qtip-centred'
        },
        position: {
            my: 'left center',
            at: 'right center',
            adjust: {}
        },
        show: {
            event: 'mouseenter',
            delay: 0,
            effect: function() {
                $(this).show('fade', 100);
            }
        },
        hide: {
            event: 'mouseleave',
            delay: 0,
            effect: function() {
                $(this).hide('fade', 100);
            }
        }
    });
    $(".qtip-fixed-dark[title]").qtip({
        style: {
            classes: 'qtip-dark qtip-dark-adjust qtip-centred qtip-fixed'
        },
        position: {
            my: 'left center',
            at: 'right center',
            adjust: {}
        },
        show: {
            event: 'mouseenter',
            delay: 0,
            effect: function() {
                $(this).show('fade', 100);
            }
        },
        hide: {
            event: 'mouseleave',
            delay: 0,
            effect: function() {
                $(this).hide('fade', 100);
            }
        }
    });
    $(".beta-qtip").qtip({
        content: {
            text: "We know this isn't perfect yet, but we're working day and night to get it done"
        },
        style: {
            classes: 'qtip-dark qtip-dark-adjust qtip-dark-adjust2'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });
    $(".qtip-dark-left").qtip({
        style: {
            classes: 'qtip-dark qtip-dark-adjust qtip-dark-adjust2'
        },
        position: {
            my: 'right center',
            at: 'left center'
        }
    });
    $(".qtip-dark-above[title]").qtip({
        style: {
            tip: false,
            classes: 'qtip-dark qtip-dark-adjust qtip-above'
        },
        position: {
            my: 'bottom center',
            at: 'top center',
            adjust: {
                scroll: false,
                y: -9
            }
        },
        show: 'showFixedQtip',
        hide: 'hideFixedQtip'
    });
    $(".qtip-dark-over[title]").qtip({
        style: {
            classes: 'qtip-dark qtip-dark-adjust qtip-fixed'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        },
        show: {
            delay: 0
        },
        hide: {
            delay: 0
        }
    });
    $(".qtip-dark-bio[title]").qtip({
        style: {
            tip: false,
            classes: 'qtip-dark qtip-dark-adjust qtip-above'
        },
        position: {
            my: 'left center',
            at: 'right center',
            adjust: {
                x: -5
            }
        },
        show: 'showQtip',
        hide: 'hideQtip'
    });
    $(".qtip-block[title]").qtip({
        style: {
            tip: false,
            classes: 'qtip-dark qtip-dark-adjust qtip-block-above'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
            adjust: {
                y: -2
            }
        },
        show: {
            delay: 0
        },
        hide: {
            delay: 0
        }
    });
    $(".qtip-focus-dark[title]").qtip({
        style: {
            tip: true,
            classes: 'qtip-dark qtip-dark-adjust'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
            adjust: {
                x: 0
            }
        },
        show: 'focused',
        hide: 'textEntry'
    });
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        mixpanel.track_links(".nextconcert-link", "Click user's next concert", function(link) {
            return {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile,
                "Concert name": $(link).data('nextconcert-name')
            };
        });
        mixpanel.track_links(".group-link", "Click user group", function(link) {
            return {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile,
                "Group name": $(link).data('group-name'),
                "Clicked on": $(link).data('click-target')
            };
        });
        mixpanel.track_links(".instrument-link, .instrument-link-mobile", "Click user instrument", function(link) {
            return {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile,
                "Instrument": $(link).data('instrument-name')
            };
        });
        mixpanel.track_links(".follower-link", "Click a user's follower", function(link) {
            return {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile,
                "Follower clicked": $(link).data('follower-name')
            };
        });
        mixpanel.track_links(".twitter-link", "Click user twitter handle", function(link) {
            return {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile,
                "User twitter handle": user.twitterHandle
            };
        });
        mixpanel.track_links(".website-link", "Click user website", function(link) {
            return {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile,
                "User website": user.website
            };
        });
        mixpanel.track_links(".creategroup-link", "User add group", function(link) {
            return {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile
            };
        });
        mixpanel.track_links(".createconcert-link", "User add future concert", function(link) {
            return {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile
            };
        });
        mixpanel.track_links(".crest-link", "Click user's college crest", function(link) {
            return {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile,
                "College": $(link).data('college')
            };
        });
        mixpanel.track_links(".refer-friend-link", "Refer a friend to enable bookings", function(link) {
            return {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile
            };
        });
    }
    if ($('#groupName').length) {
        $("#groupName").autocomplete({
            autoFocus: false,
            source: function(request, response) {
                if (request.term.replace(/\s/g, '') === '')
                    response({});
                else
                    $.ajax({
                        url: "/groups/autocomplete/" + request.term.trim(),
                        data: {},
                        success: function(data) {
                            response(data.slice(0, 4));
                        }
                    });
            },
            focus: function(event, ui) {
                return false;
            },
            select: function(event, ui) {
                if (ui.item.label != "Can't find group") {
                    var id = ui.item.value;
                    $("#groupName").val(ui.item.label);
                    addingGroupId = id;
                    $('.btn-addgroup').attr('disabled', false);
                }
                return false;
            },
            response: function(event, ui) {
                $('.autocomplete-loading1').hide();
                if (ui.content.length === 0) {
                    ui.content.push({
                        label: "Can't find group",
                        value: ""
                    });
                }
            },
            search: function(event, ui) {
                $('.ui-autocomplete-search').hide();
                $('.autocomplete-loading1').show();
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            if (item.label == "Can't find group") {
                return $('<li class="ui-state-disabled" style="background-color: white !important; color:#222222 !important; opacity:0.35 !important; ">' + item.label + '</li>').appendTo(ul);
            } else {
                $(ul).addClass('ui-autocomplete-search');
                $(ul).addClass('ui-autocomplete-low');
                getProfilePicture(item.value, true, function(profilePic) {
                    $('#groupSearchResult' + profilePic.id).css('background-image', 'url("' + profilePic.url + '")');
                    $('#groupSearchResult' + profilePic.id).css('background-size', 'cover');
                    $('#groupSearchResult' + profilePic.id).css('background-position', profilePic.position);
                });
                var labelText = item.label;
                return $("<li style='clear:both; padding:0px !important; border:0px !important; margin:0px !important; line-height:48px; height:48px;'>").append("<a><div id='groupSearchResult" + item.value + "' style='float:left; background: url(\"/img/profilePictures/s/default.png\"); background-size:cover; background-position: 0px 0px; height:48px; width:48px; margin-right:10px; ' />" + "<div style='float:left; width:240px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis; word-wrap: normal;'>" + labelText + "</div></a>").appendTo(ul);
            }
        };
    }
    if ($('#pastGroupName').length) {
        $("#pastGroupName").autocomplete({
            autoFocus: false,
            source: function(request, response) {
                if (request.term.replace(/\s/g, '') === '')
                    response({});
                else
                    $.ajax({
                        url: "/groups/autocomplete/" + request.term.trim(),
                        data: {},
                        success: function(data) {
                            response(data.slice(0, 4));
                        }
                    });
            },
            focus: function(event, ui) {
                return false;
            },
            select: function(event, ui) {
                var id = ui.item.value;
                $("#pastGroupName").val(ui.item.label);
                addingPastGroupId = id;
                $('.btn-addpastgroup').attr('disabled', false);
                return false;
            },
            response: function(event, ui) {
                $('.autocomplete-loading2').hide();
            },
            search: function(event, ui) {
                $('.ui-autocomplete-search').hide();
                $('.autocomplete-loading2').show();
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            $(ul).addClass('ui-autocomplete-search');
            $(ul).addClass('ui-autocomplete-low');
            getProfilePicture(item.value, true, function(profilePic) {
                $('#groupSearchResult2' + profilePic.id).css('background-image', 'url("' + profilePic.url + '")');
                $('#groupSearchResult2' + profilePic.id).css('background-size', 'cover');
                $('#groupSearchResult2' + profilePic.id).css('background-position', profilePic.position);
            });
            var labelText = item.label;
            return $("<li style='clear:both; padding:0px !important; border:0px !important; margin:0px !important; line-height:48px; height:48px;'>").append("<a><div id='groupSearchResult2" + item.value + "' style='float:left; background: url(\"/img/profilePictures/s/default.png\"); background-size:cover; background-position: 0px 0px; height:48px; width:48px; margin-right:10px; ' />" + "<div style='float:left; width:240px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis; word-wrap: normal;'>" + labelText + "</div></a>").appendTo(ul);
        };
    }
    refreshComposerAutocomplete();

    function startProfileEdit() {
        var fullImage = $('.profile-pic').css('background-image').replace('/240/', '/full/');
        fullImage = fullImage.replace(/\?\d*/g, '?' + new Date().getTime());
        $('.profile-pic').css('background-image', fullImage);
        $('.profile-pic').css('background-position', user.profilePosition ? user.profilePosition : '0px 0px');
        $('.edit-profile-pic').css('opacity', '1');
        $('.profile-pic').css('cursor', 'move');
        $('.profile-pic').backgroundDraggable();
        $('.viewas').hide();
        $('.facebook-photo-btn').hide();
        showProfileOptions();
    }

    function showProfileOptions() {
        $('.edit-profile-pic').html('<label for="pp-upload" class="edit-pp-a" style="height:39px; font-family:\'Lato\'; font-weight:300; font-size:15px; color: #F5F5F5;cursor: pointer; width:50%; float:left; border-right:1px solid #2c3e50; padding-top: 8px;">Upload new...</label>\
										<div class="cancel-pp" style="height:39px; font-family:\'Lato\'; font-weight:300; font-size:15px; color: #F5F5F5;cursor: pointer; width:50%; float:left; padding-top: 8px; font-weight:300;">Save</div>');
        $('.profile-pic').append('<div class="reposition-text2" style="position:relative; pointer-events:none; top:50%; left:50%; margin-left:-16px; width:32px; height:32px; background-image: url(\'/img/reposition_s.png\'); background-repeat: no-repeat;"></div>');
        $(".cancel-pp").click(function() {
            stopProfileEdit();
        });
    }

    function stopProfileEdit() {
        profilePosition = $('.profile-pic').css('background-position');
        var data = {
            position: profilePosition
        };
        user.profilePosition = profilePosition;
        $.ajax({
            type: 'POST',
            url: "/users/" + user._id + "/picture/position",
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function(data) {
                var profileImage = $('.profile-pic').css('background-image').replace('/full/', '/240/');
                profileImage = profileImage.replace(/\?\d*/g, '?' + new Date().getTime());
                $('.profile-pic').css('background-image', profileImage);
                $('.profile-pic').css('background-position', '0px 0px');
                $('.viewas').show();
                $('.facebook-photo-btn').show();
                $('.profile-pic').css('cursor', 'default');
                $('.profile-pic').backgroundDraggable('disable');
                $('.edit-profile-pic').css('opacity', '');
                $('.edit-profile-pic').html('<div class="edit-pp" style="cursor:pointer; padding-top:9px;padding-bottom:9px; color: #F5F5F5; font-size: 15px; font-weight:300;">Click here to edit profile picture</div>');
                $('.reposition-text2').remove();
                updateEnabled('picture', true);
                $('.edit-pp').click(function() {
                    startProfileEdit();
                });
            }
        });
    }

    function startCoverEdit() {
        var b = $('.cover-picture').css('background-image');
        var bs = $('.cover-picture').css('background-size');
        var bp = $('.cover-picture').css('background-position');
        skroll.destroy();
        $('.cover-picture').css('background-image', b);
        $('.cover-picture').css('background-size', bs);
        $('.cover-picture').css('background-position', bp);
        $('.facebook-login').hide();
        $('.top-container').addClass('invisible');
        $('.edit-cover-photo').css('opacity', '1');
        showCoverOptions();
        startCoverDrag();
    }

    function startCoverDrag() {
        $('.cover-picture').css('cursor', 'move');
        $('.cover-picture').backgroundDraggable({
            axis: 'y'
        });
    }

    function showCoverOptions() {
        $('.edit-cover-photo').html('<label for="cp-upload" class="edit-cp-a" style="font-weight:300; height:40px; color: #F5F5F5;cursor: pointer; width:80%; float:left; border-right:1px solid #2c3e50; font-weight:300;">Click here to upload new cover photo</label>\
										<div class="cancel-cp" style="height:40px; color: #F5F5F5;cursor: pointer; width:20%; float:left; font-weight:300;">Save</div>');
        $('.cover-picture').append('<div class="reposition-text" style="position:relative; float:right; pointer-events:none; top:60px; right:50%; margin-right:-125px; width:250px; height:40px; background-image: url(\'/img/reposition.png\'); background-repeat: no-repeat;"></div>');
        $(".cancel-cp").click(function() {
            stopCoverEdit();
        });
    }

    function deletePrice(e) {
        e.preventDefault();
        var priceId = $(this).data('id');
        $.ajax({
            type: 'DELETE',
            url: '/users/' + user._id + '/price',
            data: {
                id: priceId
            },
            success: function() {
                alertify.success("Deleted price");
                $('.user-price[data-id=' + priceId + ']').remove();
            },
            error: function() {
                console.log('Failed deleting price: ' + e);
                alertify.error('Failed deleting price');
            }
        })
    }

    function postPrice(e) {
        e.preventDefault();
        var text = $('.js-prices-text').val();
        var value = $('.js-prices-value').val();
        $.ajax({
            type: 'POST',
            url: '/users/' + user._id + '/price',
            data: {
                text: text,
                value: value
            },
            success: function() {
                $('.js-prices-text').val('');
                $('.js-prices-value').val('');
                alertify.success("Saved price");
                $('.price-filler').hide();
                $('.user-prices').append('<div class=\"user-price\"><div class="row"><div class="col-xs-8 col-sm-8 col-md-8"><h1>' + text + '</h1></div><div class="col-xs-4 col-sm-4 col-md-4"><h2>' + value + '</h2></div></div>');
            },
            error: function(e) {
                console.log('Failed saving price: ' + e);
                alertify.error('Failed saving price');
            }
        });
    }

    function stopCoverEdit() {
        backgroundPosition = $('.cover-picture').css('background-position');
        var covPosShift = backgroundPosition;
        var hs = covPosShift.split(" ");
        var ys = parseInt(hs[1]);
        ys += scrollAbs;
        covPosShift = hs[0] + " " + ys + "px";
        $('.cover-picture').attr('data-0', 'background-position: ' + backgroundPosition);
        $('.cover-picture').attr('data-240', 'background-position: ' + covPosShift);
        var data = {
            position: backgroundPosition
        };
        $.ajax({
            type: 'POST',
            url: "/users/" + user._id + "/cover/position",
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function(data) {
                $('.cover-picture').css('cursor', 'default');
                $('.top-container').removeClass('invisible');
                $('.cover-picture').backgroundDraggable('disable');
                $('.facebook-login').show();
                $('.edit-cover-photo').css('opacity', '');
                $('.edit-cover-photo').html('<div class="edit-cp" style="cursor: pointer; color: #F5F5F5; font-weight:300;">Click here to change or reposition your cover photo</div>');
                $('.reposition-text').remove();
                $('.edit-cp').click(function() {
                    startCoverEdit();
                });
                if ($(window).width() > 768 && !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Opera Mini|IEMobile|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)) {
                    skroll = skrollr.init({
                        forceHeight: false,
                        smoothScrolling: false
                    });
                }
            }
        });
    }
    $('.progress .progress-bar').progressbar({
        display_text: 'fill'
    });
    $('.clearable1').clearSearch({
        callback: function() {
            $('.clearable1').keyup();
        }
    });
    $('.clearable2').clearSearch({
        callback: function() {
            $('.clearable2').keyup();
        }
    });
    $('.clearable3').clearSearch({
        callback: function() {
            $('.clearable3').keyup();
        }
    });
    $("#testQuote").keypress(function(e) {
        if (e.keyCode == 13)
            $("#testSource").focus();
    });
    $("#testSource").keypress(function(e) {
        if (e.keyCode == 13)
            $("#testYear").focus();
    });
    $("#testYear").keypress(function(e) {
        if (e.keyCode == 13)
            $("#addTest").click();
    });
    $(".location-input").keypress(function(e) {
        if (e.keyCode == 13)
            $("#save-location").click();
    });
    $("#typeAdd").keypress(function(e) {
        if (e.keyCode == 13) {}
    });
    $("#inputSoundcloud").keypress(function(e) {
        if (e.keyCode == 13) {
            updateSoundcloud();
        }
    });
    $("#inputYouTube").keypress(function(e) {
        if (e.keyCode == 13) {
            updateYouTube("youtube");
        }
    });
    $("#inputVimeo").keypress(function(e) {
        if (e.keyCode == 13) {
            updateYouTube("vimeo");
        }
    });
    $("#inputYouTubeChannel").keypress(function(e) {
        if (e.keyCode == 13) {
            updateYouTubeChannel();
        }
    });
    $("#instrumentAdd").keypress(function(e) {
        if (e.keyCode == 13) {
            submitConcertDetails(user._id);
        }
    });
    $('#composerAdd').keyup(function(event) {
        if (event.keyCode && event.keyCode != 9 && event.keyCode != 13 && event.keyCode != 16) {
            composerId = '';
            composerPic = '';
        }
    });
    $('.composer-input').keyup(function(event) {
        if (event.keyCode && event.keyCode != 9 && event.keyCode != 13 && event.keyCode != 16) {
            composerEditId = '';
        }
    });
    $('input.filter-input').keyup(function(e) {
        var code = e.keyCode || e.which;
        if (code == '9') return;
        var $input = $(this),
            inputContent = $input.val().trim().toLowerCase(),
            $panel = $input.parents('.filterable'),
            $table = $panel.find('.filter-content'),
            $rows = $table.find('.filterElement');
        var $filteredRows = $rows.filter(function() {
            var value = $(this).find('.filterMe').text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        $table.find('.no-result').remove();
        $rows.show();
        $filteredRows.hide();
        if ($rows.length && ($filteredRows.length === $rows.length)) {
            $table.prepend($('<div class="no-result text-center" style="margin-top: 20px; font-size:16px;"><div class="row">Sorry, we couldn\'t find anything</div></div>'));
        }
    });
    $('.edit-pp').click(function() {
        startProfileEdit();
    });
    $(".pp-upload").change(function() {
        if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
            mixpanel.track("User upload profile picture", {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile
            });
        }
        $('.profile-pic').removeClass('pulse');
        $('.profile-pic').css('background', 'url(/img/loading_logo.gif)');
        $('.profile-pic').css('background-position', 'center');
        $('.profile-pic').css('background-repeat', 'no-repeat');
        $('.edit-profile-pic').hide();
        $('.profile-pic').css('cursor', 'default');
        $('.profile-pic').backgroundDraggable('disable');
        $('.reposition-text2').remove();
        alertify.success("Uploading image, please wait...");
        $(".submitBtn").click();
    });
    $('.edit-cp').click(function() {
        startCoverEdit();
    });
    $(".cp-upload").change(function() {
        if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
            mixpanel.track("User upload cover picture", {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile
            });
        }
        alertify.success("Uploading image, please wait...");
        $('.coverSubmitBtn').click();
    });
    $('.fols').click(function() {
        if (user._id == userId)
            window.location.href = "/dashboard/followers";
        else {
            if ($(window).width() > 767) {
                $('.overlay-followers').dialog({
                    title: "People following " + (ownProfile ? "you" : (user.musician ? user.firstname : user.lastname)),
                    height: 437,
                    width: 500,
                    draggable: false,
                    resizable: false,
                    modal: true,
                    dialogClass: 'followers-dialog',
                    show: {
                        effect: 'fade',
                        duration: 300
                    },
                    hide: {
                        effect: 'fade',
                        duration: 300
                    },
                    open: function(event, ui) {
                        $('html').addClass('stop-scrolling');
                        $('.ui-widget-overlay').bind('click', function() {
                            $(this).siblings('.ui-dialog').find('.ui-dialog-content').dialog('close');
                        });
                        if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
                            mixpanel.track("Click user followers", {
                                "Current URL": document.URL,
                                "Profile": user.fullname,
                                "Own profile?": ownProfile,
                                "User followers": (user.followers ? user.followers.length : 0).toString()
                            });
                        }
                    },
                    beforeClose: function(event, ui) {
                        $('html').removeClass('stop-scrolling');
                    }
                });
            } else {}
        }
    });
    $('.profile-full-overlay').on('click', '.close-overlay-button', function() {
        $('html').removeClass('stop-scrolling');
        window.history.replaceState({}, $(document).attr('title'), window.location.origin + window.location.pathname);
        $(this).parents('.container-fluid').parent().fadeOut();
    });
    $(".location-value").click(function() {
        if (isAdmin || ownProfile) {
            $(this).hide();
            $(".location-input").show();
            $(".location-input").focus();
            $("#save-location").show();
        }
    });
    $("#save-location").click(function() {
        $(this).hide();
        $(".location-input").hide();
        updateLocation();
        if ($('.location-input').val().trim().length) {
            $(".location-value").html(loc).show();
            $('.location-value').css('font-style', 'normal');
        } else {
            if (!$('.location-value').text().trim().length || $('.location-value').text().trim() === 'No location set') {
                $('.location-value').css('font-style', 'italic');
                $('.location-value').html('No location set');
            } else {
                alertify.error("Location cannot be empty");
            }
            $(".location-value").show();
        }
    });
    $('.shortbio-input').maxlength({
        alwaysShow: 'true'
    });
    $(".shortbio-editable").click(function(event) {
        if (isAdmin || ownProfile) {
            if (event.target.id != "save-shortbio" && event.target.id != "save-shortbio-icon" && event.target.id != "read-fullbio") {
                $(this).removeClass('shortbio-editable');
                $(".shortbio").hide();
                $(".shortbio-input").show();
                $("#save-shortbio").css('display', 'block');
                $(".shortbio-input").focus();
            }
        }
    });
    $('#save-shortbio').click(function() {
        var text = $('.shortbio-input').val().trim();
        var s = new Sanitize(text, []);
        if (s.check()) {
            $('.saveshortbio .control-label').text(s.generateError()).show();
            $('.saveshortbio textarea').addClass('has-error');
            $('.send-message-holder').addClass('has-error');
            $('.saveshortbio').addClass('has-error');
        } else {
            $('.saveshortbio .control-label').hide();
            $('.send-message-holder').removeClass('has-error');
            $('.saveshortbio textarea').removeClass('has-error');
            $('.saveshortbio').removeClass('has-error');
            updateShortBio(text);
            if (user.biography && user.biography.trim().length) {
                $(".read-fullbio").html('<a id="read-fullbio" onclick="scrollToAnchor(\'bio\')">Read full biography</a>');
            } else {
                $(".read-fullbio").html("");
            }
            $(".shortbio-input").hide();
            $(".shortbio-input").html(text);
            $(".shortbio").html(text);
            $(this).hide();
            if (!text || !text.trim().length)
                $(".shortbio").html('<p class="shortbio qtip-title-dark" title="Click here to edit your short bio" style="cursor:pointer;">Looks like you haven\'t added a short bio yet. Click here to give a brief overview of yourself.</p>');
            $(".shortbio").show();
            $(".shortbio-stuff").addClass('shortbio-editable');
        }
    });
    $(".bio-editable").click(function(event) {
        if (isAdmin || ownProfile) {
            if (event.target.id != "save-fullbio" && event.target.id != "save-fullbio-icon") {
                var h = $('.full-bio').height();
                $('.full-bio').hide();
                $(".fullbio-input").show();
                $(".fullbio-input").css('height', h + 'px');
                $("#save-fullbio").show();
                $(this).removeClass('bio-editable');
                $(".fullbio-input").focus();
            }
        }
    });
    $("#save-fullbio").click(function() {
        var text = $('.fullbio-input').val().trim();
        var s = new Sanitize(text, []);
        if (s.check()) {
            $('.savefullbio .control-label').text(s.generateError()).show();
            $('.savefullbio').addClass('has-error');
        } else {
            $('.savefullbio').removeClass('has-error');
            $('.savefullbio .control-label').hide();
            updateBiography(text);
            $(this).hide();
            $(".fullbio-input").hide();
            $(".full-bio").html($('.fullbio-input').val().trim());
            if (!$('.fullbio-input').val().trim().length) {
                $(".full-bio").html('<p class="full-bio" style="cursor:pointer; clear:both; font-size:16px;">You haven\'t added a biography yet. Click here to start describing your career so far!</p>');
            }
            $(".full-bio").show();
            $(".bio").addClass('bio-editable');
        }
    });
    $(".songlist-editable").click(function(event) {
        if (isAdmin || ownProfile) {
            if (event.target.id != "save-songlist" && event.target.id != "save-songlist-icon") {
                var h = $('.songlist').height();
                $('.songlist').hide();
                $(".songlist-input").show();
                $(".songlist-input").css('height', h + 'px');
                $("#save-songlist").show();
                $(this).removeClass('songlist-editable');
                $(".songlist-input").focus();
            }
        }
    });
    $("#save-songlist").click(function() {
        var text = $('.songlist-input').val().trim();
        var s = new Sanitize(text, []);
        if (s.check()) {
            $('.savesonglist .control-label').text(s.generateError()).show();
            $('.savesonglist').addClass('has-error');
        } else {
            $('.savesonglist').removeClass('has-error');
            $('.savesonglist .control-label').hide();
            updateSonglist(text);
            $(this).hide();
            $(".songlist-input").hide();
            $(".songlist").html($('.songlist-input').val().trim());
            if (!$('.songlist-input').val().trim().length) {
                $(".songlist").html('<p class="songlist" style="cursor:pointer; clear:both; font-size:16px;">You haven\'t added a song list yet. Click here to add one. You can easily copy and paste into this text box.</p>');
            }
            $(".songlist").show();
            $(".songlist-row").addClass('songlist-editable');
        }
    });
    $('.instruments-col').on('click', '.edit-instruments', function() {
        $('html').addClass('stop-scrolling');
        $('.instruments-btns').hide();
        $('.bootstrap-tagsinput').show();
        $('.edit-instruments').replaceWith("<div class='save-instruments'><span id='save-glyph' class='fa fa-save'></span> Click anywhere to save</div>");
        $('.bootstrap-tagsinput input').focus();
        $('.editor-overlay').show();
        $('.bootstrap-tagsinput').attr('style', 'display: block; position:relative; z-index: 999');
        $('.save-instruments').attr('style', 'display: block; position:relative; z-index: 999; color:white; font-size:1.1em; cursor:default');
    });
    $('#courseAddGroup').hide();
    $('#placeAddGroup').hide();
    $('#collegeAddGroup').hide();
    $('#institutionType').change(function() {
        var x = $('#institutionType option:selected').text();
        $('.place-label').html(x);
        $('#placeAdd').attr("placeholder", x);
        $('#placeAddGroup').show();
        if ($(this).val().trim() != 'School') {
            if ($(this).val().trim() == 'University') {
                $('#collegeAddGroup').show();
            } else {
                $('#collegeAddGroup').hide();
            }
            $('#courseAddGroup').show();
        } else {
            $('#courseAddGroup').hide();
            $('#collegeAddGroup').hide();
        }
    });
    $('.twitter-row').click(function(event) {
        if ((isAdmin || ownProfile) && ($('.save-twitter').css('display') == 'none') && event.target.id != 'save-twitter') {
            t = $('.twitter-value').text().trim();
            if (t == 'Click here to add twitter' || typeof(t) == "undefined")
                t = '';
            if (t.indexOf("@") > -1) {
                t = t.substring(t.indexOf("@") + 1, t.length);
            }
            $('.twitter-value').html("@<input class='form-control block-input' id='inputTwitter' value='" + t.replace(/\s/g, '') + "'></input>");
            $('.twitter-value').css('font-size', '19px');
            $('.edit-twitter').hide();
            $('.save-twitter').show();
            $('#inputTwitter').keypress(function(event) {
                if (event.keyCode == 13)
                    $('.save-twitter').click();
            });
            $('#inputTwitter').focus();
            if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
                if (!t || !t.length) {
                    mixpanel.track("User add twitter handle", {
                        "Current URL": document.URL,
                        "Profile": user.fullname,
                        "Own profile?": ownProfile
                    });
                }
            }
        }
    });
    $('.save-twitter').click(function() {
        updateTwitter();
        $('.edit-twitter').show();
        $('.save-twitter').hide();
        $('.twitter-value').css('font-size', '');
    });
    $(".url-row:not(.disabled)").click(function(event) {
        if ((isAdmin || ownProfile) && ($('.save-url').css('display') == 'none') && (!event || event.target.id != 'save-url')) {
            if ($('.url-value').text() == 'Click here to set your Encore URL')
                u = '';
            else
                u = $('.url-value').text().trim().split('/').pop();
            originalUrl = u;
            $('.url-value').html("<input type='text' class='form-control url-input block-input' id='inputUrl' value='" + u + "'></input>");
            $('.edit-url').hide();
            $('.save-url').show();
            $('#inputUrl').keypress(function(event) {
                if (event.keyCode == 13)
                    $('.save-url').click();
            });
            $('#inputUrl').focus();
        }
    });
    $(".save-url").click(function() {
        alertify.confirm("Warning: this will change your invite link, and any invites you have sent will no longer be valid. Continue?", function(e) {
            if (e) {
                updatePublicUrl(function(saved) {
                    if (saved) {
                        $('.edit-url').show();
                        $('.save-url').hide();
                        $(".url-warn").hide();
                        if (u.length) {
                            $(".url-value").html('encoremusicians.com/' + u);
                            if ($(".sidebar-url").length) {
                                $(".sidebar-url").html('encoremusicians.com/' + u);
                            }
                        } else {
                            $(".url-value").html('Click here to set your Encore URL');
                        }
                    } else {
                        $(".url-warn").fadeIn(100);
                    }
                });
            } else {
                $('.edit-url').show();
                $('.save-url').hide();
                $(".url-warn").hide();
                if (originalUrl.length) {
                    $(".url-value").html('encoremusicians.com/' + originalUrl);
                    if ($(".sidebar-url").length) {
                        $(".sidebar-url").html('encoremusicians.com/' + originalUrl);
                    }
                } else {
                    $(".url-value").html('Click here to set your Encore URL');
                }
            }
        });
    });
    $(".sidebar-url").click(function() {
        $(".tab-1").click();
        scrollToAnchor("url-row", function() {
            $(".url-row").click();
        });
    });
    var profileEdit = location.search.split('photo=')[1];
    if (profileEdit == 'reposition' && (isAdmin || ownProfile)) {
        $('.overlay-unverified-user').hide();
        $('html').removeClass('stop-scrolling');
        startProfileEdit();
        window.history.replaceState({}, $(document).attr('title'), window.location.origin + window.location.pathname);
    }
    var coverEdit = location.search.split('cover=')[1];
    if (coverEdit == 'reposition' && (isAdmin || ownProfile)) {
        $('.overlay-unverified-user').hide();
        $('html').removeClass('stop-scrolling');
        startCoverEdit();
        window.history.replaceState({}, $(document).attr('title'), window.location.origin + window.location.pathname);
    }
    var autoFollow = location.search.split('follow=')[1];
    if (autoFollow == 'true') {
        if (publicProfile) {
            $('.login-button').click();
        } else {
            if ($('.follow-btn:visible').text() === 'Follow') {
                $('.follow-btn:visible').click();
                $('#youFollowed').dialog({
                    height: 'auto',
                    width: 500,
                    draggable: false,
                    resizable: false,
                    modal: true,
                    dialogClass: 'you-followed-dialog ui-darkened-dialog',
                    show: {
                        effect: 'fade',
                        duration: 300
                    },
                    hide: {
                        effect: 'fade',
                        duration: 300
                    },
                    open: function(event, ui) {
                        $('html').addClass('stop-scrolling');
                        $('.ui-widget-overlay').bind('click', function() {
                            $(this).siblings('.ui-dialog').find('.ui-dialog-content').dialog('close');
                        });
                    },
                    beforeClose: function(event, ui) {
                        $('html').removeClass('stop-scrolling');
                    }
                });
            }
        }
    }
    $('.gallery-a').click(function(event) {
        if ((typeof($(event.target).attr('class')) != "undefined") && ($(event.target).attr('class').indexOf("gallery-remove") > -1)) {
            event.stopPropagation();
            return false;
        }
    });
    if (publicProfile) {
        var t = user.twitterHandle;
        if (!t) t = '';
        t = t.trim();
        if (t.indexOf('@') === 0)
            t = t.slice(1);
        if (t.length) {
            $.ajax({
                url: "https://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names=" + t,
                dataType: 'jsonp',
                crossDomain: true
            }).done(function(data) {
                $(".twitter-count").html(data[0].followers_count);
                $(".fol-text").html("Followers");
            });
        }
    }
    $("#ref-submit").click(function() {
        var content = $("#ref-form textarea#content").val().trim();
        if (content && content !== "") {
            submitReference(userId, content, function() {
                $("#ref-form textarea#content").fadeOut();
                $("#ref-form textarea#content").replaceWith("<div class=\"alert alert-success\" style=\"max-width:600px;margin:auto;\"><strong>Done!</strong> Once " + user.firstname + " has approved your reference, it will appear on this page.</div>");
                $("#ref-form textarea#content").fadeIn();
                $('#ref-submit').parent().fadeOut();
                if (mixpanelTracking) {
                    mixpanel.track("Reference submitted", {
                        "Current URL": document.URL,
                        "Profile": user.fullname,
                        "Profile ID": user._id,
                        "Own profile?": ownProfile,
                        "Reference": content
                    });
                }
            });
        }
    });

    function referenceTransportToList(listParent, element, callback) {
        var thisorder = element.data('orderhelper');
        var lelems = $(listParent + ' .profile-reference-parent');
        var count = lelems.length;
        lelems.each(function() {
            if ($(this).data('orderhelper') > thisorder) {
                element.insertBefore($(this));
                element.fadeIn();
                callback();
                return false;
            }
            --count;
        });
        if (!count) {
            $(listParent).prepend(element);
            element.fadeIn();
            callback();
        }
    }
    $('.profile-content').delegate('.ref-display-toggle', 'click', function(e) {
        e.preventDefault();
        var parent = $(this).parents('.profile-reference-parent');
        var grabbed = $(parent.clone(true, true));
        grabbed.css("display", "none");
        var refid = grabbed.data('refid').replace(/\"/g, "");
        var target, notTarget, newStatus;
        if (grabbed.data('publicreference')) {
            grabbed.find('.fa-eye').removeClass('fa-eye').addClass('fa-eye-slash');
            grabbed.data('publicreference', false);
            notTarget = "#profilePublicReferences";
            target = "#profilePrivateReferences";
            newStatus = "HIDE";
            grabbed.addClass('profile-reference-parent-private');
        } else {
            grabbed.find('.fa-eye-slash').removeClass('fa-eye-slash').addClass('fa-eye');
            grabbed.data('publicreference', true);
            notTarget = "#profilePrivateReferences";
            target = "#profilePublicReferences";
            newStatus = "SHOW";
            grabbed.removeClass('profile-reference-parent-private');
        }
        grabbed.find('.ref-edit-section').removeClass('hidden');
        $(grabbed).addClass('opaque');
        referenceTransportToList(target, grabbed, function() {
            parent.fadeOut();
            setTimeout(function() {
                parent.remove();
            }, 1000);
            $(target + 'Heading.collapse').collapse('show');
            var dCount = $(target + "Heading").data('count');
            ++dCount;
            $(target + "Heading").data('count', dCount);
            var tCount = $(notTarget + "Heading").data('count');
            --tCount;
            $(notTarget + "Heading").data('count', tCount);
            if (!tCount) {
                $(notTarget + "Heading").collapse('hide');
            }
            refResponse(refid, newStatus, function() {
                var message = (newStatus == "HIDE" ? "Reference rejected (from profile, previously accepted)" : "Reference accepted (from profile, previously rejected)");
                if (mixpanelTracking) {
                    mixpanel.track(message, {
                        "Current URL": document.URL,
                        reference: refid
                    });
                }
            });
        });
    });
    $('.ref-review').click(function() {
        var status = $(this).data('status').replace(/\"/g, "");
        var parent = $(this).parents('.profile-reference-parent');
        var grabbed = $(parent.clone(true, true));
        grabbed.css("display", "none");
        var refid = grabbed.data('refid').replace(/\"/g, "");
        var taget, notTarget;
        if (status == "HIDE") {
            grabbed.find('.fa-eye').removeClass('fa-eye').addClass('fa-eye-slash');
            grabbed.data('publicreference', false);
            target = "#profilePrivateReferences";
            notTarget = "#profilePublicReferences";
        } else {
            grabbed.find('.fa-eye-slash').removeClass('fa-eye-slash').addClass('fa-eye');
            grabbed.data('publicreference', true);
            target = "#profilePublicReferences";
            notTarget = "#profilePrivateReferences";
        }
        grabbed.find('.ref-edit-section').removeClass('hidden');
        grabbed.find('.reference-body').addClass('reference-body-profile');
        grabbed.find('.ref-review-parent').remove();
        referenceTransportToList(target, grabbed, function() {
            var refRemain = $("#newReferences").data('referencecount');
            --refRemain;
            if (!refRemain) {
                $('#newReferences').collapse('hide');
            } else {
                $("#newReferences").data('referencecount', refRemain);
                parent.fadeOut();
                setTimeout(function() {
                    parent.remove();
                }, 1000);
            }
            $(target + 'Heading.collapse').collapse('show');
            var dCount = $(target + "Heading").data('count');
            ++dCount;
            $(target + "Heading").data('count', dCount);
            var tCount = $(notTarget + "Heading").data('count');
            --tCount;
            $(notTarget).data('count', tCount);
            if (!tCount) {
                $(notTarget + "Heading").collapse('hide');
            }
            refResponse(refid, status, function() {
                var message = (status == "HIDE" ? "Reference rejected (from profile, new reference)" : "Reference accepted (from profile, ne reference)");
                if (mixpanelTracking) {
                    mixpanel.track(message, {
                        "Current URL": document.URL,
                        reference: refid
                    });
                }
            });
        });
    });
    if (isAdmin || user._id == userId) {
        $('.fullname').click(function() {
            $(this).hide();
            $('.edit-name').show();
        });
    }
    if ((publicProfile && !user.privacy.profile) || user.hidden) {
        alertify.alert("Your profile is not visible publicly");
    }
    $('.book-btn').on('click', handleBookBtn);
    $('.js-instrument-tag').tagsinput({
        freeInput: true,
        typeahead: {
            source: instrumentList
        },
    });
    $('.js-instrument-tag').on('itemAdded', updateVideo);
    $('.js-instrument-tag').on('itemRemoved', updateVideo);
    $('.js-badge-add-toggle').on('click', toggleAddHolder);
    $('.js-badge-add').on('click', addBadge);
    $('.js-badge-remove').on('click', removeBadge);
    $('.review-text').readmore({
        collapsedHeight: 80,
        moreLink: '<button class="btn btn-link read-more-review">Read more</button>',
        lessLink: '<button class="btn btn-link read-more-review">Read less</button>',
        embedCSS: false,
        beforeToggle: function() {},
    });
    $('.private-unlock').on('click', function() {
        console.log('hey');
        $.ajax({
            type: "POST",
            url: "/users/" + user._id + "/tracks",
            data: {
                password: $("#privateTrackCode").val()
            },
            success: function(data) {
                console.log(data)
                $('.askForPassCode').hide();
                var tracks = data;
                var newTracks = tmpl('template-tracks', {
                    data: tracks
                });
                $('.tracks').empty().append(newTracks);
                tracks.forEach(function(track, i) {
                    if ($('#track-player-' + i).length) {
                        var ap = new APlayer({
                            element: document.getElementById('track-player-' + i),
                            narrow: false,
                            autoplay: false,
                            showlrc: false,
                            mutex: true,
                            theme: '#e6d0b2',
                            loop: true,
                            music: {
                                title: track.title,
                                author: user.fullname,
                                url: track.url,
                                pic: '/img/playerbg.png'
                            }
                        });
                        ap.init()
                    }
                });
            },
            error: function() {
                alertify.alert("Incorrect passcode");
            }
        });
    });
    $('input[id*="trackPrivacy"]').change(function() {
        privacyChanging = true;
        var id = $(this).attr('id').substring(13);
        if ($(this).prop('checked')) {
            updateTrackPrivacy(id, false);
        } else {
            updateTrackPrivacy(id, true);
        }
    });
    $('#updatePrivatePasscode').click(function() {
        var data = {
            privateMediaPasscode: $("#editPrivateTrackCode").val()
        };
        console.log("data = " + data);
        $.ajax({
            type: 'PUT',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: '/users/' + user._id + '/update',
            success: function(data) {
                if (data == 200 || data == 'OK') {
                    alertify.success("Saved");
                    privacyChanging = false;
                }
            }
        });
    });
    $('.admin-quality-setting').click(function() {
        var score = parseInt($(this).data('score'));
        if (score) {
            $('.we-did-it').addClass('visible');
            $('.admin-quality-section').hide();
            setTimeout(function() {
                $('.we-did-it').removeClass('visible');
            }, 500);
            $.ajax({
                type: 'POST',
                data: {
                    set: score
                },
                url: '/users/' + user._id + '/admin/rating',
                success: function(data) {
                    if (data == 200 || data == 'OK') {
                        console.log("Details updated");
                        alertify.success("Saved");
                    }
                }
            });
        }
    });
    if (firsttime && adwordsTrack) {
        adwordsTrack("872162745", "nOHHCPeO62wQucvwnwM");
        if (typeof ga != "undefined")
            ga('send', 'pageview', '/me/first');
    }
});

function updateTrackPrivacy(id, value) {
    var data = {
        id: id,
        value: value
    };
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + user._id + '/trackPrivacy',
        success: function(data) {
            if (data == 200 || data == 'OK') {
                alertify.success("Saved");
                privacyChanging = false;
            }
        }
    });
}

function toggleAddHolder() {
    $('.badge-holder-add').toggle();
}

function addBadge() {
    var src = $(this).data('badge-src');
    var type = $(this).data('type');
    var data = {};
    $('.badges').append('<div class="badge-holder qtip-builder" title="Click to remove" data-orientation="right" data-fixed="false"><img class="sidebar-icon union-badge" data-type="' + type + '" src="' + src + '" style="cursor:default;"/><span class="union-badge-remove js-badge-remove fa fa-times" data-src="' + src + '" data-type="' + type + '"></div>');
    $('.js-badge-remove').unbind('click', removeBadge).on('click', removeBadge);
    data[type] = true;
    $.ajax({
        url: '/users/' + user._id + '/badges',
        type: 'PUT',
        data: data
    });
    toggleAddHolder();
    $(this).remove();
    alertify.success("Success. Please wait...");
    window.location.reload();
}

function removeBadge() {
    var type = $(this).data('type');
    var src = $(this).data('src');
    var data = {};
    data[type] = false;
    $.ajax({
        url: '/users/' + user._id + '/badges',
        type: 'PUT',
        data: data,
        success: function() {
            $('.qtip:visible').qtip("hide");
            $('.union-badge-remove[data-type="' + type + '"]').parent().remove();
        }
    });
    $('.badge-holder-add').append('<div class="badge-holder badge-adder js-badge-add" data-type="' + type + '" data-badge-src="' + src + '"><img class="sidebar-icon union-badge" data-type="' + type + '" src="' + src + '"></div>');
    $('.js-badge-add').unbind('click', addBadge).on('click', addBadge);
    alertify.success("Success. Please wait...");
    window.location.reload();
}

function updateVideo(video) {
    var instruments = $(this).val();
    var videoId = $(this).data('videoId');
    console.log(1, videoId, instruments);
    $.ajax({
        method: 'PUT',
        contentType: 'application/json',
        url: '/users/' + user._id + '/youtube',
        data: JSON.stringify({
            instruments: instruments,
            videoId: videoId
        }),
        success: function() {
            alertify.success('Saved instrument');
        },
        error: function() {
            alertify.alert('Failed saving instrument');
        }
    })
}

function refResponse(id, status, callback) {
    $.ajax({
        type: 'POST',
        data: JSON.stringify({
            status: status,
            referenceID: id
        }),
        contentType: 'application/json',
        url: '/users/' + user._id + '/reference/status',
        success: callback
    });
}

function showFirstTab() {
    tabClicked(1, 'tabname', -1);
    window.history.replaceState({
        "tab": 1
    }, $(document).attr('title'), window.location.href);
}

function scrollToAnchor(aid, callback) {
    var aTag = $("." + aid + "");
    var top = 0;
    if (!aTag.length || !aTag.offset())
        top = 240;
    else
        top = aTag.offset().top - (tall ? 103 : 51) - 40;
    if (callback)
        $('html,body').animate({
            scrollTop: top
        }, {
            duration: 'slow',
            complete: callback
        });
    else
        $('html,body').animate({
            scrollTop: top
        }, 'slow');
}

function sidebarYoutubePlay() {
    $(".tab-2").click();
    if (widget)
        widget.pause();
    if ($('.youtubePlayer').length) {
        scrollToAnchor("youtubePlayer");
        if (player && player.pauseVideo)
            player.playVideo();
    } else {
        scrollToAnchor("youtubeChannelPlayer");
        $('.ytv-active .ytv-thumb').click();
    }
}

function sidebarSoundcloudPlay() {
    $(".tab-2").click();
    scrollToAnchor("soundcloudPlayer");
    if (widget)
        widget.play();
    if ($('.youtubePlayer').length) {
        if (player && player.pauseVideo)
            player.pauseVideo();
    }
}

function contact() {
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking) {
        mixpanel.track("Contact (from profile)", {
            "Current URL": document.URL,
            "Contacted user": user.fullname,
            "Contacted user ID": user._id
        });
    }
    contactPopup(user.firstname, user._id);
}

function publicContact() {
    if (publicProfile) {
        handleBookBtn();
    } else {
        if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking) {
            mixpanel.track("Public contact (from profile)", {
                "Current URL": document.URL,
                "Contacted user": user.fullname,
                "Contacted user ID": user._id
            });
        }
        contactPublicPopup(user.firstname, user._id);
    }
}

function handleBookBtn() {
    var params = getURLParameters();
    if (params.jobCode) {
        if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking) {
            mixpanel.track("Clicked book applicant", {
                "Current URL": document.URL,
                "Location": "Profile",
                "Applicant ID": user._id,
                "Job code": params.jobCode
            });
        }
        window.location.href = '/jobs/' + params.jobCode + '/status?book=' + user._id;
    } else {
        showFirstTab();
        scrollToAnchor('booking-form');
        openEnquiryModal();
    }
}

function openEnquiryModal(e) {
    if (ownProfile) {
        alertify.alert("Sorry, you can't send yourself a booking request!");
        return;
    }
    window.renderDirectEnquiryForm(user, true);
}

function refreshComposerAutocomplete() {
    if ($('#composerAdd').length) {
        $('#composerAdd').autocomplete({
            autoFocus: true,
            source: function(request, response) {
                var $this = $(this);
                var $element = $(this.element);
                var previous_request = $element.data("jqXHR");
                if (previous_request) {
                    previous_request.abort();
                }
                if (request.term.replace(/\s/g, '') === '') {
                    $element.data("jqXHR", null);
                    response({});
                } else {
                    $element.data("jqXHR", $.ajax({
                        url: "/composers/autocomplete/" + request.term.trim(),
                        data: {},
                        success: function(data) {
                            if (data && data.length > 6) {
                                data.splice(6, data.length);
                                searchOverflow = true;
                            } else {
                                searchOverflow = false;
                            }
                            response(data);
                        }
                    }));
                }
            },
            focus: function(event, ui) {
                return false;
            },
            select: function(event, ui) {
                if (ui.item.label != "Can't find composer") {
                    $(this).val(ui.item.label);
                    if (this.id == 'composerAdd') {
                        composerId = ui.item.value;
                        composerPic = ui.item.thumbnail;
                    } else {
                        composerEditId = ui.item.value;
                    }
                }
                return false;
            },
            response: function(event, ui) {
                $('.autocomplete-loading9').hide();
                if (ui.content.length === 0) {
                    ui.content.push({
                        label: "Can't find composer",
                        value: ""
                    });
                }
            },
            search: function(event, ui) {
                $('.ui-autocomplete-search').hide();
                $('.autocomplete-loading9').show();
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            if (item.label == "Can't find composer") {
                return $('<li class="ui-state-disabled" style="background-color: white !important; color:#222222 !important; opacity:0.35 !important; ">' + item.label + '</li>').appendTo(ul);
            } else {
                $(ul).addClass('ui-autocomplete-search');
                $(ul).addClass('ui-autocomplete-search-wide');
                getProfilePicture(item.value, false, function(profilePic) {
                    $('#searchResult20' + profilePic.id).css('background-image', 'url("' + profilePic.url + '")');
                    $('#searchResult20' + profilePic.id).css('background-size', 'cover');
                    $('#searchResult20' + profilePic.id).css('background-position', profilePic.position);
                });
                var labelText = item.label;
                labelText = labelText.replace(new RegExp('(' + preg_quote($("#composerAdd").val()) + ')', 'gi'), "<strong>$1</strong>");
                return $("<li style='clear:both; padding:0px !important; border:0px !important; margin:0px !important; line-height:48px; height:48px;'>").append("<a><div id='searchResult20" + item.value + "' style='float:left; background: url(\"/img/profilePictures/s/default.png\"); background-size:cover; background-position: 0px 0px; height:48px; width:48px; margin-right:10px; ' />" + "<div style='float:left; width:215px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis; word-wrap: normal;'>" + labelText + "</div></a>").appendTo(ul);
            }
        };
    }
    if ($('.composer-input').length) {
        $('.composer-input').autocomplete({
            autoFocus: true,
            source: function(request, response) {
                var $this = $(this);
                var $element = $(this.element);
                var previous_request = $element.data("jqXHR");
                if (previous_request) {
                    previous_request.abort();
                }
                if (request.term.replace(/\s/g, '') === '') {
                    $element.data("jqXHR", null);
                    response({});
                } else {
                    $element.data("jqXHR", $.ajax({
                        url: "/composers/autocomplete/" + request.term.trim(),
                        data: {},
                        success: function(data) {
                            if (data && data.length > 6) {
                                data.splice(6, data.length);
                                searchOverflow = true;
                            } else {
                                searchOverflow = false;
                            }
                            response(data);
                        }
                    }));
                }
            },
            focus: function(event, ui) {
                return false;
            },
            select: function(event, ui) {
                if (ui.item.label != "Can't find composer") {
                    $(this).val(ui.item.label);
                    if (this.id == 'composerAdd') {
                        composerId = ui.item.value;
                        composerPic = ui.item.thumbnail;
                    } else {
                        composerEditId = ui.item.value;
                    }
                }
                return false;
            },
            response: function(event, ui) {
                $('.autocomplete-loading10').hide();
                if (ui.content.length === 0) {
                    ui.content.push({
                        label: "Can't find composer",
                        value: ""
                    });
                }
            },
            search: function(event, ui) {
                $('.ui-autocomplete-search').hide();
                $('.autocomplete-loading10').show();
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            if (item.label == "Can't find composer") {
                return $('<li class="ui-state-disabled" style="background-color: white !important; color:#222222 !important; opacity:0.35 !important; ">' + item.label + '</li>').appendTo(ul);
            } else {
                $(ul).addClass('ui-autocomplete-search');
                $(ul).addClass('ui-autocomplete-search-narrow');
                getProfilePicture(item.value, false, function(profilePic) {
                    $('#searchResult20' + profilePic.id).css('background-image', 'url("' + profilePic.url + '")');
                    $('#searchResult20' + profilePic.id).css('background-size', 'cover');
                    $('#searchResult20' + profilePic.id).css('background-position', profilePic.position);
                });
                var labelText = item.label;
                labelText = labelText.replace(new RegExp('(' + preg_quote($(this.element).val()) + ')', 'gi'), "<strong>$1</strong>");
                return $("<li style='clear:both; padding:0px !important; border:0px !important; margin:0px !important; line-height:48px; height:48px;'>").append("<a><div id='searchResult20" + item.value + "' style='float:left; background: url(\"/img/profilePictures/s/default.png\"); background-size:cover; background-position: 0px 0px; height:48px; width:48px; margin-right:10px; ' />" + "<div style='float:left; width:215px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis; word-wrap: normal;'>" + labelText + "</div></a>").appendTo(ul);
            }
        };
    }
    $("#composerAdd, .composer-input").focus(function() {
        $(this).keydown();
    }).blur(function() {
        $('.autocomplete-loading9').hide();
        $('.autocomplete-loading10').hide();
        $(this).keyup();
    });
}

function editName() {
    var data = {};
    data.firstname = $('#firstnameedit').val().trim();
    data.lastname = $('#lastnameedit').val().trim();
    data.othername = $('#othernameedit').val().trim();
    if (data.othername.length)
        data.fullname = data.firstname + " " + data.othername + " " + data.lastname;
    else
        data.fullname = data.firstname + " " + data.lastname;
    if (data.fullname.length && data.firstname.length && data.lastname.length) {
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: '/users/' + user._id + '/name',
            success: function(data2) {
                if (data2 == 200 || data2 == 'OK') {
                    alertify.success("Name changed");
                    $('.edit-name').hide();
                    $('.fullname').text(data.fullname);
                    $('.fullname').show();
                }
            }
        });
    } else {
        alertify.error("Please enter at least your first and last names");
    }
}

function editComposerImage() {
    var data = {};
    data.image = $('#composerimageedit').val().trim();
    if (data.image && data.image.length) {
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: '/users/' + user._id + '/composerimage',
            success: function(data) {
                if (data == 200 || data == 'OK') {
                    alertify.success("Changed, please wait...");
                    window.location.href = '/people/' + user._id;
                }
            }
        });
    } else {
        alertify.error("Please enter a non-empty value");
    }
}

function contactPublicPopup(name, recipientID) {
    var width = ($(window).width() < 768 ? '90vw' : '70%');
    var height = ($(window).width() < 768 ? $(window).height() - 50 : 'auto');
    if ($(window).width() < 768)
        $('body').addClass('mobile');
    else
        $('body').removeClass('mobile');
    $('.public-contact-overlay').dialog({
        height: height,
        width: width,
        draggable: false,
        resizable: false,
        modal: true,
        dialogClass: 'contact-dialog',
        show: {
            effect: 'fade',
            duration: 300
        },
        hide: {
            effect: 'fade',
            duration: 300
        },
        open: function(event, ui) {
            $('html').addClass('stop-scrolling');
            $('.ui-widget-overlay').bind('click', function() {
                $(this).siblings('.ui-dialog').find('.ui-dialog-content').dialog('close');
            });
        },
        beforeClose: function(event, ui) {
            $('html').removeClass('stop-scrolling');
        }
    });
    $(".nameTextPub").text("We'll send your message as an email to " + name);
    $("#messageTextareaPub").text("Hi " + name + ",\n\n\n\nBest wishes,\n\n");
    $("#sendMsgPub").attr("onclick", "publicMessage('" + recipientID + "')");
}

function publicMessage(targetId) {
    var contactEmail = $("#messageSenderPub").val();
    var validEmailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!validEmailRegEx.test(contactEmail)) {
        alertify.error("Please enter a valid email address");
        return;
    }
    if ($("#messageSubjectPub").val().replace(/\s/g, '').length && $("#messageSenderName").val().replace(/\s/g, '').length && $("#messageSenderPub").val().replace(/\s/g, '').length && $("#messageTextareaPub").val().replace(/\s/g, '').length) {
        var data = {
            subject: $("#messageSubjectPub").val(),
            message: $("#messageTextareaPub").val(),
            email: $("#messageSenderPub").val(),
            fullname: $("#messageSenderName").val()
        };
        var subjectFilter = new Sanitize(data.subject, []);
        if (subjectFilter.check()) {
            var error = subjectFilter.generateError();
            var subjectError = error.slice(0, -1) + ' from the subject field.'
            alertify.alert(subjectError);
            return;
        }
        var params = getURLParameters();
        if (params.jobRef && params.jobRef.length) {
            data.autorelease = params.jobRef;
            data.jobId = params.jobRef;
        }
        if (params.jobCode && params.jobCode.length) {
            data.jobCode = params.jobCode;
        }
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: '/users/publicmessage/' + targetId,
            success: function(data) {
                if (data == 200 || data == 'OK') {
                    console.log("Message sent!");
                    alertify.success("Message sent");
                    $('.public-contact-overlay').dialog('close');
                    $('html').removeClass('stop-scrolling');
                }
            },
            error: function(xhr, err) {
                console.log("error!");
                console.log(err);
                alertify.alert("Sorry, something went wrong. Please try again or contact help@encoremusicians.com if this problem persists.");
            }
        });
        if (params.jobRef) {
            $.ajax({
                type: 'PUT',
                data: JSON.stringify({}),
                contentType: 'application/json',
                url: '/jobs/' + params.jobRef + '/replied',
                data: JSON.stringify({
                    applicantId: targetId
                }),
                error: function(err) {
                    console.log(err)
                },
                success: function(data) {
                    if (data == 200 || data == 'OK') {
                        console.log("applicant updated");
                    }
                }
            });
        }
        if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking) {
            mixpanel.register("email", $("#messageSenderPub").val());
            mixpanel.register("name", $("#messageSenderName").val());
        }
    } else {
        alertify.error("Please enter your details, subject and a message");
    }
}

function saveContactsOrder() {
    var str = $('.grid-2').sortable("serialize");
    $.ajax({
        type: 'POST',
        data: JSON.stringify({}),
        contentType: 'application/json',
        url: '/users/' + user._id + '/contactsort?' + str,
        success: function(data) {}
    });
}

function reorderContacts() {
    if (contacts && contacts.length) {
        var ul = $("ul.grid-2");
        var lis = ul.children("li");
        lis.detach().sort(function(a, b) {
            var id1 = $(a).attr('id');
            var id2 = $(b).attr('id');
            var n1 = parseInt(id1.slice(9, id1.length));
            var n2 = parseInt(id2.slice(9, id2.length));
            var indexA = contacts.indexOf(n1);
            var indexB = contacts.indexOf(n2);
            if (indexA == indexB)
                return 0;
            if (indexA > indexB)
                return 1;
            else
                return -1;
        });
        ul.append(lis);
    }
}

function addGroupForm() {
    $('.addGroupBtn').unbind('click');
    $('.addGroupBtn').addClass('no-transition');
    $('.addGroupBtn').removeClass('performer-thumb');
    $('.addGroupBtn').css('border', '2px solid rgb(70, 144, 192)');
    $('.big-group-plus').fadeOut(100);
    $('.concert-add-txt').fadeOut(100, function() {
        $('.addGroupBtn').animate({
            width: 400
        }, 500, 'swing', function() {
            $('.addGroupForm').fadeIn(200);
        });
    });
}
$("#groupName").focus(function() {
    $('.new-group').addClass('no-transition');
    $('.new-group').fadeOut(100);
    $(this).animate({
        width: "300px"
    }, {
        duration: 200,
        queue: false,
        complete: function() {
            $('#groupName').keydown();
        }
    });
}).blur(function() {
    $('.new-group').fadeIn(100, function() {
        $('.new-group').removeClass('no-transition');
    });
    $(this).animate({
        width: "200px"
    }, {
        duration: 200,
        queue: false
    });
    $('.autocomplete-loading1').hide();
    $(this).keyup();
});
$('#groupName').keydown(function(event) {
    if (event.keyCode && event.keyCode != 13) {
        addingGroupId = '';
        $('.btn-addgroup').attr('disabled', true);
    }
});
$('.addGroupBtn:not(.disabled)').click(function() {
    addGroupForm();
});
$('.btn-addgroup').click(function() {
    if (addingGroupId && addingGroupId.trim().length) {
        var width = ($(window).width() < 768 ? '90vw' : 400);
        $('.overlay-instrument').dialog({
            height: 'auto',
            width: width,
            draggable: false,
            resizable: false,
            modal: true,
            dialogClass: 'you-followed-dialog ui-darkened-dialog ui-mobile-dialog',
            show: {
                effect: 'fade',
                duration: 300
            },
            hide: {
                effect: 'fade',
                duration: 300
            },
            open: function(event, ui) {
                $('html').addClass('stop-scrolling');
                $('.ui-widget-overlay').bind('click', function() {
                    $(this).siblings('.ui-dialog').find('.ui-dialog-content').dialog('close');
                });
                $('head').append("<style type='text/css'> .ui-autocomplete { z-index: 999999 !important; } </style>");
            },
            beforeClose: function(event, ui) {
                $('html').removeClass('stop-scrolling');
            }
        });
    } else {
        $('.btn-addgroup').attr('disabled', true);
    }
});

function removeGroup(id) {
    alertify.confirm("Are you sure you want to remove this group?", function(e) {
        if (e) {
            alertify.set({
                labels: {
                    ok: "Set as past group",
                    cancel: "Remove"
                },
                buttonReverse: true
            });
            alertify.confirm("If you were in this group but have now left, click 'Set as past group'.<br>If you were added to this group by mistake, click 'Remove'.", function(e, str) {
                alertify.set({
                    labels: {
                        ok: "OK",
                        cancel: "Cancel"
                    },
                    buttonReverse: false
                });
                if (e) {
                    console.log('clicked past grp');
                    $.ajax({
                        type: 'POST',
                        data: JSON.stringify({}),
                        contentType: 'application/json',
                        url: '/groups/' + id + '/graduate/' + user._id,
                        success: function(data) {
                            if (data == 'OK' || data == 200) {
                                alertify.alert("Thanks - this will now appear as one of your past groups");
                                location.reload(true);
                            } else {
                                alertify.alert("Sorry, something went wrong! Please leave feedback to tell us what happened.");
                            }
                        }
                    });
                    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking) {
                        mixpanel.track("Group on profile clicked graduate", {
                            "Current URL": document.URL,
                            "After clicking remove": true
                        });
                    }
                } else {
                    console.log('clicked remove');
                    $.ajax({
                        type: 'POST',
                        data: JSON.stringify({}),
                        contentType: 'application/json',
                        url: '/users/' + user._id + '/group/' + id + '/delete',
                        success: function(data) {
                            if (data == 'OK' || data == 200) {
                                alertify.alert("Thanks - you've been removed from this group");
                                location.reload(true);
                            }
                        }
                    });
                }
            }, "");
        }
    });
}

function removePastGroup(id) {
    alertify.confirm("Are you sure?", function(e) {
        if (e) {
            $.ajax({
                type: 'POST',
                data: JSON.stringify({}),
                contentType: 'application/json',
                url: '/users/' + user._id + '/pastgroup/' + id + '/delete',
                success: function(data) {
                    if (data == 'OK' || data == 200) {
                        $('#pastGroup' + id).fadeOut(300, function() {
                            $(this).remove();
                        });
                        alertify.success("Group removed from profile");
                    }
                }
            });
        }
    });
}

function addPastGroupForm() {
    $('.addPastGroupBtn').unbind('click');
    $('.addPastGroupBtn').addClass('no-transition');
    $('.addPastGroupBtn').removeClass('performer-thumb');
    $('.addPastGroupBtn').css('border', '2px solid rgb(70, 144, 192)');
    $('.big-pgroup-plus').fadeOut(100);
    $('.pgroup-add-txt').fadeOut(100, function() {
        $('.addPastGroupBtn').animate({
            width: 400
        }, 500, 'swing', function() {
            $('.addPastGroupForm').fadeIn(200, function() {
                $('#pastGroupName').focus();
            });
        });
    });
}
$("#pastGroupName").focus(function() {
    $('.new-pgroup').addClass('no-transition');
    $('.new-pgroup').fadeOut(100);
    $('#pastGroupName').keydown();
    if (!$('#pastGroupName').val().trim().length) {
        $(this).trigger('focused');
    }
}).blur(function() {
    $(this).trigger('textEntry');
    $('.new-pgroup').fadeIn(100, function() {
        $('.new-pgroup').removeClass('no-transition');
    });
    $('.autocomplete-loading2').hide();
    $(this).keyup();
});
$('#pastGroupName').keyup(function(event) {
    if (event.keyCode && event.keyCode != 9 && event.keyCode != 13 && event.keyCode != 16) {
        addingPastGroupId = '';
        if (!$('#pastGroupName').val().trim().length) {
            $(this).trigger('focused');
            $('.btn-addpastgroup').attr('disabled', true);
        } else {
            $(this).trigger('textEntry');
            $('.btn-addpastgroup').attr('disabled', false);
        }
    }
});
$('.addPastGroupBtn:not(.disabled)').click(function() {
    addPastGroupForm();
});
$('#pastGroupEnd').keypress(function(e) {
    if (e.keyCode == 13) {
        $('.btn-addpastgroup').click();
    }
});
$('.btn-addpastgroup').click(function() {
    if (addingPastGroupId && addingPastGroupId.trim().length) {
        console.log('group matched');
        $.ajax({
            type: 'POST',
            data: JSON.stringify({
                name: $('#pastGroupName').val().trim(),
                username: user.fullname,
                insts: $('#pastGroupInsts').val().trim(),
                start: $('#pastGroupStart').val().trim(),
                end: $('#pastGroupEnd').val().trim()
            }),
            contentType: 'application/json',
            url: '/users/' + user._id + '/pastgroup/' + addingPastGroupId,
            success: function(data) {
                if (data == 'OK' || data == 200) {
                    alertify.success("Group added, please wait...");
                    window.location.href = '/people/' + user._id;
                } else
                    alertify.alert("Sorry, something went wrong! Please leave feedback to tell us what happened.");
                addingGroupId = '';
                $('.btn-addpastgroup').attr('disabled', true);
                $('.new-pgroup').show();
                $('.new-pgroup').removeClass('no-transition');
                $("#pastGroupName").css('width', '200px');
                $("#pastGroupName").val('');
                $('.addPastGroupBtn').addClass('no-transition');
                $('.addPastGroupBtn').addClass('performer-thumb');
                $('.addPastGroupBtn').css('border', '');
                $('.addPastGroupForm').fadeOut(300, function() {
                    $('.addPastGroupBtn').animate({
                        width: 140
                    }, 300, 'swing', function() {
                        $('.big-pgroup-plus').fadeIn(200);
                        $('.pgroup-add-txt').fadeIn(200, function() {
                            $('.addPastGroupBtn').click(function() {
                                addPastGroupForm();
                            });
                        });
                    });
                });
            }
        });
    } else {
        console.log('group is new');
        $.ajax({
            type: 'POST',
            data: JSON.stringify({
                name: $('#pastGroupName').val().trim(),
                username: user.fullname,
                insts: $('#pastGroupInsts').val().trim(),
                start: $('#pastGroupStart').val().trim(),
                end: $('#pastGroupEnd').val().trim()
            }),
            contentType: 'application/json',
            url: '/users/' + user._id + '/pastgroup',
            success: function(data) {
                if (data == 'OK' || data == 200) {
                    alertify.success("Group added, please wait...");
                    window.location.href = '/people/' + user._id;
                } else
                    alertify.alert("Sorry, something went wrong! Please leave feedback to tell us what happened.");
                addingGroupId = '';
                $('.btn-addpastgroup').attr('disabled', true);
                $('.new-pgroup').show();
                $('.new-pgroup').removeClass('no-transition');
                $("#pastGroupName").css('width', '200px');
                $("#pastGroupName").val('');
                $('.addPastGroupBtn').addClass('no-transition');
                $('.addPastGroupBtn').addClass('performer-thumb');
                $('.addPastGroupBtn').css('border', '');
                $('.addPastGroupForm').fadeOut(300, function() {
                    $('.addPastGroupBtn').animate({
                        width: 140
                    }, 300, 'swing', function() {
                        $('.big-pgroup-plus').fadeIn(200);
                        $('.pgroup-add-txt').fadeIn(200, function() {
                            $('.addPastGroupBtn').click(function() {
                                addPastGroupForm();
                            });
                        });
                    });
                });
            }
        });
    }
});

function getProfilePicture(id, isGroup, cb) {
    var url;
    if (isGroup)
        url = '/groups/';
    else
        url = '/users/';
    $.ajax({
        type: 'POST',
        data: JSON.stringify({
            targetSize: 48
        }),
        contentType: 'application/json',
        url: url + id + '/getProfilePicture',
        success: function(data) {
            cb(data);
        }
    });
}

function goToConcert(id, name) {
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        mixpanel.track("Click on user's concert", {
            "Current URL": document.URL,
            "Profile": user.fullname,
            "Own profile?": ownProfile,
            "Concert": name,
            "Concert ID": id
        });
    }
    window.location.href = '/concerts/' + id;
}

function tabClicked(tab, name, direction) {
    $('.profile-content').addClass('hidden');
    $('.profile-tab-' + tab).removeClass('hidden');
    direction = (typeof(direction) == 'undefined' ? 1 : direction);
    if (direction > 0) updateUrl('tab', tab.toString());
    $('.navTabs').children().each(function() {
        $(this).removeClass('navTabSelected');
    });
    $('.tab-' + tab).addClass('navTabSelected');
}
$('.reset-sent-close').click(function() {
    $('.reset-sent').slideUp('medium', function() {});
});

function setFacebookId(id) {
    var data = {
        content: id
    };
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + user._id + '/facebook',
        success: function(data) {
            console.log('Facebook ID stored');
        }
    });
}

function setProfilePicture(url) {
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        mixpanel.track("User facebook connect picture", {
            "Current URL": document.URL,
            "Profile": user.fullname,
            "Own profile?": ownProfile
        });
    }
    alertify.success("Picture found, please wait for the image to load...");
    $.ajax({
        type: 'POST',
        url: "/users/" + user._id + "/picture/facebook",
        contentType: 'application/json',
        data: JSON.stringify({
            url: url
        }),
        success: function(data) {
            var d = new Date();
            $('.profile-pic').css('background-image', 'url("' + data + '?' + d.getTime() + '")');
            $('.facebook-photo-btn').remove();
            $('.profile-picture-fader').remove();
        }
    });
}

function editInstruments() {
    if ($('.edit-insts').text().toLowerCase().indexOf('save') < 0) {
        $('.edit-insts').html('<span class="fa fa-check"></i> Save');
        $('.instruments-btns').children().each(function() {
            if ($(this).hasClass('instrument-link')) {
                $(this).children().addClass('drag-cursor');
                $(this).children().children().each(function() {
                    $(this).addClass('drag-handle');
                });
            }
        });
        $('.instruments').trigger('showFixedQtip');
        $('.instruments-btns').css('height', ($('.instruments-btns').height() - 34) + 'px');
        $('.instruments-padding').css('height', ($('.instruments-padding').height() - 34) + 'px');
        $('.instruments-btns').css('position', 'relative');
        $('.edit-insts').hide();
        $('.instruments-btns').before('<div id="instControls" style="height:34px; width:240px; background:white; text-align:center;"><div onclick="editInstruments()" class="save-instruments"><span class="fa fa-check"></span> Save</div><div onclick="addInstrumentForm()" class="add-new-instrument"><span class="fa fa-plus" style="margin-right:4px; top:1px;"></span> Add new</div></div>');
        $('.instruments-btns').sortable('enable');
        $('.instruments-btns').perfectScrollbar('update');
    } else {
        if ($('.instrument-link').length) {
            $('.edit-insts').html('<span class="fa fa-pencil"></span> Edit');
            $('.edit-insts').css('width', '68px');
        } else {
            $('.edit-insts').html('<span class="fa fa-pencil"></span> Add instruments');
            $('.edit-insts').css('width', '145px');
        }
        $('.instruments-btns').children().each(function() {
            if ($(this).hasClass('instrument-link')) {
                $(this).children().removeClass('drag-cursor');
                $(this).children().children().each(function() {
                    $(this).removeClass('drag-handle');
                });
            }
        });
        $('.instruments').trigger('hideFixedQtip');
        $('.instruments-btns').css('height', ($('.instruments-btns').height() + 34) + 'px');
        $('.instruments-padding').css('height', ($('.instruments-padding').height() + 34) + 'px');
        $('.instruments-btns').css('position', 'absolute');
        $('.edit-insts').show();
        $('#instControls').remove();
        $('.instruments-btns').sortable('disable');
        $('.instruments-btns').perfectScrollbar('update');
        saveInstruments();
    }
}

function addInstrumentForm() {
    var controls = $('#instControls').html();
    $('#instControls').html('<input type="text" placeholder="Instrument" class="inst-autocomplete" id="inst-input">' + '<span id="addInst" class="fa fa-check small-button" style="font-size:20px; margin-left:5px;"></span>' + '<span id="cancelInst" class="fa fa-times small-button" style="font-size:20px; margin-left:5px;"></span>');
    $('.inst-autocomplete').autocomplete({
        source: instrumentList,
        select: function(event, ui) {
            $('#inst-input').val(ui.item.value);
            $('#addInst').click();
        }
    });
    $('#inst-input').focus();
    $('#addInst').click(function() {
        var inst = $('#inst-input').val().trim();
        if (inst && inst.length) {
            var count = $('.instrument-link').length;
            var colours = ['#09477E', '#115FA5', '#2578C2', '#428BCA'];
            var colour = colours[Math.min(colours.length - 1, count)];
            var html = '<a class="instrument-link" id="instrument' + count + '" href="/search/' + inst + '" data-instrument-name="' + inst + '">\
							<btn style="font-size:' + Math.max(14, (20 - 2 * count)) + 'px; width:100%; background: ' + colour + ';" class="btn btn-primary btn-sm instrument-btn drag-cursor">' + inst + '\
								<span class="drag-handle" style="display:none; float:left;color: #F5F5F5;line-height: 1.5;font-weight: 300;font-family: sans-serif;">:::</span>\
								<span class="drag-handle" style="display:none; float:right;color: #F5F5F5;line-height: 1.5;font-weight: 300;font-family: sans-serif;">:::</span></btn></a>';
            if (count)
                $('.instrument-link').last().after(html);
            else
                $('.instruments-btns').prepend(html);
            resetInstrumentPadding();
            $('.instruments-btns').perfectScrollbar('update');
            $('.instruments-btns').sortable('refresh');
            $('.instrument-link').click(function(e) {
                if ($('.edit-insts').text().toLowerCase().indexOf('save') > -1) {
                    e.preventDefault();
                    return false;
                }
            });
            alertify.success("Instrument added");
        }
        $('#addInst').unbind('click');
        $('#cancelInst').unbind('click');
        $('#instControls').html(controls);
    });
    $('#inst-input').keypress(function(e) {
        if (e.keyCode == 13) {
            $('#addInst').click();
        }
    });
    $('#cancelInst').click(function() {
        $('#addInst').unbind('click');
        $('#cancelInst').unbind('click');
        $('#instControls').html(controls);
    });
}

function recolourInstruments() {
    colours = ['#09477E', '#115FA5', '#2578C2', '#428BCA'];
    $root = $('.instruments-btns');
    $root.children().each(function(index) {
        if ($(this).hasClass('instrument-link')) {
            $(this).children().css('background', colours[Math.min(index, colours.length - 1)]);
            $(this).children().css('font-size', (Math.max(14, 20 - 2 * index)) + 'px');
        }
    });
}

function resetInstrumentPadding() {
    var colours = ['#09477E', '#115FA5', '#2578C2', '#428BCA'];
    var index = $('.instrument-link').length;
    $('.instruments-padding').css('background', colours[Math.min(colours.length - 1, index)]);
    var instHeightNum = Math.max(0, $('.side-nav').height() - ($('.instruments-btns').offset().top - ($('.side-nav').offset().top - (tall ? 103 : 51))));
    $('.instruments-padding').css('height', (instHeightNum - ($('.instruments-padding').offset().top - $('.instruments-btns').offset().top)) + 'px');
}

function saveInstruments() {
    $root = $('.instruments-btns');
    var instList = [];
    $root.children().each(function(index) {
        if ($(this).hasClass('instrument-link')) {
            instList.push($(this).children().clone().children().remove().end().text().trim());
        }
    });
    var data = {
        instruments: instList
    };
    $.ajax({
        type: 'POST',
        url: "/users/" + user._id + "/instruments",
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function(data) {
            alertify.success("Changes saved");
        }
    });
}

function deleteInstrument(inst) {
    var data = {};
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + user._id + '/instruments/' + inst + '/delete',
        success: function(data) {}
    });
}

function markPageVisited() {
    console.log("marking page as visited");
    $.ajax({
        type: 'PUT',
        data: JSON.stringify({
            'visited.profile': true
        }),
        contentType: 'application/json',
        url: '/users/' + user._id + '/update',
        success: function(data) {}
    });
}

function updatePublicUrl(cb) {
    u = $('.url-input').val().replace(/\s/g, '');
    var data = {
        content: u
    };
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + user._id + '/url',
        success: function(data) {
            if (data && (data == 'OK' || data == 200)) {
                cb(true);
            } else {
                cb(false);
            }
        }
    });
}

function updateLocation() {
    loc = $('.location-input').val().trim();
    var data = {
        content: loc
    };
    console.log(data);
    if (loc && loc.length)
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: '/users/' + user._id + '/location',
            success: function(data) {
                return loc;
            }
        });
}

function updateShortBio(text) {
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        mixpanel.track("User update short bio", {
            "Current URL": document.URL,
            "Profile": user.fullname,
            "Own profile?": ownProfile
        });
    }
    $.ajax({
        type: 'POST',
        data: JSON.stringify({
            content: text
        }),
        contentType: 'application/json',
        url: '/users/' + user._id + '/shortbio',
        success: function(data) {
            var isSet = text.length > 0 ? true : false;
            updateEnabled('bio', isSet);
            return text;
        }
    });
}

function updateEnabled(key, isSet) {
    if (key == 'media')
        isSet = ((user.videos && user.videos.length) || (user.youtubeChannel && user.youtubeChannel.length) || (user.soundcloud && user.soundcloud.length) || (user.tracks && user.tracks.length));
    enableBookingStatus[key] = isSet;
    if (isSet) {
        $('.js-enable-' + key).removeClass('fa-times').addClass('fa-check');
    } else {
        $('.js-enable-' + key).removeClass('fa-check').addClass('fa-times');
    }
    if (enableBookingStatus.public && enableBookingStatus.picture && enableBookingStatus.bio && enableBookingStatus.media && enableBookingStatus.referral) {
        $('.js-enable-booking').removeAttr("disabled", "disabled");
        $('.js-enable-message').text("You have unlocked the Encore booking form!");
    } else {
        $('.js-enable-booking').attr("disabled", "disabled");
        $('.js-enable-message').text("Complete your profile to enable booking enquiries:");
    }
}

function enableBooking() {
    $('.js-enable-booking').addClass('active');
    $.ajax({
        type: 'POST',
        data: JSON.stringify({
            field: 'booking',
            value: true
        }),
        contentType: 'application/json',
        url: '/users/' + user._id + '/privacy',
        success: function(data) {
            if (data == 200 || data == 'OK') {
                location.reload();
            }
        }
    });
}

function updateFollowCount(up) {
    $tab = $('.fol-number');
    var txt = $tab.text();
    var count = parseInt(txt);
    if (up)
        count++;
    else
        count = Math.max(0, count - 1);
    $tab.html(count);
    if (count == 1) {
        $('.fol-text').html("Follower");
    } else {
        $('.fol-text').html("Followers");
    }
}

function follow(action) {
    var id = user._id;
    $('.follow-btn').prop('disabled', true);
    console.log(id);
    var data = {};
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + action + '/' + id,
        success: function(data) {
            if (data == 'done' && action == 'follow') {
                $(".follow-btn").html('<span class="fa fa-user" style="color:white; margin-right:10px; position:relative; top:-1px;"></span>Following');
                $('.follow-btn').attr("onclick", "follow('unfollow');");
                following = true;
                updateFollowCount(true);
            } else if (data == 'done' && action == 'unfollow') {
                $(".follow-btn").html('<span class="fa fa-plus" style="color:white; margin-right:10px; position:relative; top:0px;"></span>&nbsp;Follow');
                $('.follow-btn').attr("onclick", "follow('follow');");
                following = false;
                updateFollowCount(false);
            } else {
                alert(data);
            }
            $('.follow-btn').prop('disabled', true);
        }
    });
}
$('#endAdd').keypress(function(event) {
    if (event.keyCode == 13)
        submitEducationDetails();
});

function submitEducationDetails() {
    var id = user._id;
    var uni = $('#placeAdd').val().trim();
    var data;
    if (uni.indexOf('Oxford') > -1) {
        data = {
            institutionType: $('#institutionType option:selected').text(),
            place: $('#placeAdd').val().trim(),
            course: $('#courseAdd').val().trim(),
            oxCollege: $('#collegeAdd').val().trim(),
            startYear: $('#startAdd').val().trim(),
            endYear: $('#endAdd').val().trim()
        };
    } else {
        data = {
            institutionType: $('#institutionType option:selected').text(),
            place: $('#placeAdd').val().trim(),
            course: $('#courseAdd').val().trim(),
            camCollege: $('#collegeAdd').val().trim(),
            startYear: $('#startAdd').val().trim(),
            endYear: $('#endAdd').val().trim()
        };
    }
    console.log(data);
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        mixpanel.track("User add education item", {
            "Current URL": document.URL,
            "Profile": user.fullname,
            "Own profile?": ownProfile,
            "Education": data.course + ' at ' + data.place
        });
    }
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + id + '/education',
        success: function(data) {
            window.location.href = '/people/' + user._id;
            alertify.success("Education details added. Please wait...");
            $('#addEducationDetails').fadeOut();
        }
    });
}

function addEducationDetails(groupId) {
    $(".add-education .big-add").slideUp("fast", function() {
        $("#addEducationDetails").slideDown("fast", function() {});
    });
}

function cancelEducationAdd() {
    $("#addEducationDetails").slideUp("fast", function() {
        $(".add-education .big-add").slideDown("fast", function() {});
    });
}

function cancelEducationEdit(id) {
    $("#editEducationDetails" + id).slideUp("fast", function() {
        $("#educationDetails" + id).slideDown("fast", function() {});
    });
}

function editEducationDetails(id) {
    $("#educationDetails" + id).slideUp("fast", function() {
        $("#editEducationDetails" + id).slideDown("fast", function() {
            if ($('#courseUpdate').length)
                $('#courseUpdate').focus();
        });
    });
    $('#crests').slideUp("fast");
}

function saveEducationDetails(id, eduId) {
    var data = {
        course: $("#editEducationDetails" + eduId + " .courseUpdate").val(),
        camCollege: $("#camCollegeUpdate" + eduId).val(),
        oxCollege: $("#oxCollegeUpdate" + eduId).val(),
        place: $("#editEducationDetails" + eduId + " .placeUpdate").val(),
        startYear: $("#editEducationDetails" + eduId + " .startUpdate").val(),
        endYear: $("#editEducationDetails" + eduId + " .endUpdate").val()
    };
    console.log(data);
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + id + '/education/' + eduId,
        success: function(data) {
            window.location.href = '/people/' + user._id;
            $("#editEducationDetails" + eduId).slideUp("fast", function() {
                $("#educationDetails" + eduId).slideDown("fast", function() {});
                $('#crests').slideDown("fast");
            });
            alertify.success("Education details saved. Please wait...");
        }
    });
}

function saveEducationDetailsSchool(id, eduId) {
    var data = {
        place: $("#editEducationDetails" + eduId + " .placeUpdate").val(),
        startYear: $("#editEducationDetails" + eduId + " .startUpdate").val(),
        endYear: $("#editEducationDetails" + eduId + " .endUpdate").val()
    };
    console.log(data);
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + id + '/education/' + eduId,
        success: function(data) {
            window.location.href = '/people/' + user._id;
            $("#editEducationDetails" + eduId).slideUp("fast", function() {
                $("#educationDetails" + eduId).slideDown("fast", function() {});
            });
            alertify.success("Education details saved. Please wait...");
        }
    });
}

function deleteEducationDetails(id, eduId) {
    var data = {};
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + id + '/education/' + eduId + '/delete',
        success: function(data) {
            window.location.href = '/people/' + user._id;
            $("#editEducationDetails" + eduId).slideUp("fast", function() {});
            alertify.error("Education item deleted. Please wait...");
        }
    });
}
$('#achDescription').keypress(function(event) {
    if (event.keyCode == 13)
        submitAchievementDetails();
});

function submitAchievementDetails() {
    var id = user._id;
    var data = {
        description: $('#achDescription').val().trim(),
        startYear: $('#achStartAdd').val().trim(),
    };
    console.log(data);
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        mixpanel.track("User add achievement", {
            "Current URL": document.URL,
            "Profile": user.fullname,
            "Own profile?": ownProfile,
            "Achievement": data.description
        });
    }
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + id + '/achievements',
        success: function(achId) {
            alertify.success("Achievement added");
            $('.dummy-achievement').fadeOut().remove();
            $('#achDescription').val('');
            $('#achStartAdd').val('');
            var htmlEl = '<div class="achievement" id="achievementDetails' + achId + '">\
					<h4>' + data.startYear + '</h4>\
					<h5>' + data.description + '</h5>\
					<a onclick="editAchievementDetails(\'' + achId + '\');">\
						<span class="fa fa-pencil-square-o"></i></a></div>\
				<div class="row" style="display: none;" id="editAchievementDetails' + achId + '">\
					<div class="row">\
						<div class="col-sm-10 col-sm-offset-1 well">\
							<div class="form-group">\
								<h5 class="col-sm-4 control-label">Description</h5>\
								<div class="col-sm-8">\
									<input type="text" value="' + data.description + '" class="form-control descriptionEdit">\
								</div>\
								<br>\
							</div>\
							<div class="form-group">\
								<h5 class="col-sm-4 control-label">Year</h5>\
								<div class="col-sm-8">\
									<input type="text" value="' + data.startYear + '" class="form-control startEdit">\
								</div>\
							</div>\
							<div class="col-sm-12" style="margin-top:30px; font-size:30px; clear:both;">\
								<div class="col-sm-2 col-sm-offset-6">\
									<a onclick="saveAchievementDetails(\'' + user._id + '\', \'' + achId + '\');">\
										<span class="fa fa-check hover-icon" style="color:#2ecc71"></span></a>\
								</div>\
								<div class="col-sm-2">\
									<a onclick="cancelAchievementEdit(\'' + achId + '\');", style="margin-left: 5px;">\
										<span class="fa fa-times hover-icon" style="color:#e74c3c"></span></a>\
								</div>\
								<div class="col-sm-2">\
									<a onclick="deleteAchievementDetails(\'' + user._id + '\', \'' + achId + '\');" style="margin-left: 5px;">\
										<span class="fa fa-trash hover-icon" style="color:#e74c3c"><spani></a>\
								</div>\
							</div>\
						</div>\
					</div>\
				</div>';
            if ($('.achievement').length)
                $(".achievement").last().next().after(htmlEl);
            else
                $("#achievements h3").after(htmlEl);
        },
        error: function() {
            alertify.alert("Error: please check you have entered a single year.");
        }
    });
}

function cancelAchievementEdit(id) {
    $("#editAchievementDetails" + id).slideUp("fast", function() {
        $("#achievementDetails" + id).slideDown("fast", function() {});
    });
}

function editAchievementDetails(id) {
    $("#achievementDetails" + id).slideUp("fast", function() {
        $("#editAchievementDetails" + id).slideDown("fast", function() {
            $('#editAchievementDetails' + id + ' .descriptionEdit').focus();
        });
    });
}

function saveAchievementDetails(id, achId) {
    var data = {
        description: $('#editAchievementDetails' + achId + ' .descriptionEdit').val(),
        startYear: $('#editAchievementDetails' + achId + ' .startEdit').val()
    };
    console.log(data);
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + id + '/achievements/' + achId,
        success: function(result) {
            $("#editAchievementDetails" + achId).slideUp("fast", function() {
                $("#achievementDetails" + achId).slideDown("fast");
            });
            $('#achievementDetails' + achId + ' h4').html(data.startYear);
            $('#achievementDetails' + achId + ' h5').html(data.description);
            $('#editAchievementDetails' + achId + ' .descriptionEdit').attr('value', data.description);
            $('#editAchievementDetails' + achId + ' .startEdit').attr('value', data.startYear);
        }
    });
}

function deleteAchievementDetails(id, achId) {
    var data = {};
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + id + '/achievements/' + achId + '/delete',
        success: function(data) {
            if (user.visited && user.visited.profile)
                window.location.href = '/people/' + user._id;
            $("#editAchievementDetails" + achId).slideUp("fast", function() {});
            alertify.error("Achievement deleted. Please wait...");
        }
    });
}

function updateBiography(text) {
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        if (text && text.length && !(user.biography && user.biography.trim() !== '')) {
            mixpanel.track("User add biography", {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile
            });
        }
    }
    $.ajax({
        type: 'POST',
        data: JSON.stringify({
            content: text
        }),
        contentType: 'application/json',
        url: '/users/' + user._id + '/biography',
        success: function(data) {
            return text;
        }
    });
}

function updateSonglist(text) {
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        if (text && text.length && !(user.songlist && user.songlist.trim() !== '')) {
            mixpanel.track("User add songlist", {
                "Current URL": document.URL,
                "Profile": user.fullname,
                "Own profile?": ownProfile
            });
        }
    }
    $.ajax({
        type: 'POST',
        data: JSON.stringify({
            content: text
        }),
        contentType: 'application/json',
        url: '/users/' + user._id + '/songlist',
        success: function(data) {
            return text;
        }
    });
}

function updateConcertCount(up) {
    $tab = $('.tab-3');
    var txt = $tab.text();
    var count = parseInt(txt.slice(txt.indexOf('(') + 1, txt.length - 1));
    if (up)
        count++;
    else
        count = Math.max(0, count - 1);
    $tab.html(txt.slice(0, txt.indexOf('(')) + '(' + count + ')');
}

function updatePieceCount(up) {
    $tab = $('.tab-4');
    var txt = $tab.text();
    var count = parseInt(txt.slice(txt.indexOf('(') + 1, txt.length - 1));
    if (up)
        count++;
    else
        count = Math.max(0, count - 1);
    $tab.html(txt.slice(0, txt.indexOf('(')) + '(' + count + ')');
}

function submitConcertDetails(id) {
    console.log('submit');
    var dateF = new Date(Date.UTC(parseInt($('#concertYear').val().trim()), $('#concertMonth option:selected').index(), parseInt($('#concertDay').val().trim()), 12, 0, 0, 0));
    var now = new Date();
    if ((dateF.getFullYear() > now.getFullYear()) || ((dateF.getFullYear() == now.getFullYear()) && (dateF.getMonth() > now.getMonth())) || ((dateF.getFullYear() == now.getFullYear()) && (dateF.getMonth() == now.getMonth()) && (dateF.getDate() > now.getDate() + 1))) {
        $('#invaliddate').slideDown("medium", function() {});
        return;
    }
    var data = {
        title: $('#titleAdd').val().trim(),
        date: dateF,
        venue: $('#venueAdd').val().trim(),
        instruments: $('#instrumentAdd').val().trim()
    };
    console.log(data);
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        mixpanel.track("User add past concert", {
            "Current URL": document.URL,
            "Profile": user.fullname,
            "Own profile?": ownProfile,
            "Concert name": data.title
        });
    }
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + id + '/concerts',
        success: function(dataBack) {
            if (dataBack == 500)
                return;
            var id = dataBack;
            var dstr = ("0" + dateF.getDate()).slice(-2);
            var mstr = ("0" + (dateF.getMonth() + 1)).slice(-2);
            var ystr = dateF.getFullYear().toString().slice(-2);
            var concertRowHtml = "<div class='row concert-row filterElement' style='cursor:default' id='concertRow" + id + "'>\
								<div class='col-md-5 filterMe' style='font-weight:bold'>" + data.title + "</div>\
								<div class='col-md-2 filterMe'>" + moment.utc(dateF).format('D MMM YY') + "</div>\
								<div class='col-md-3 filterMe'>" + data.venue + "</div>\
								<div class='col-md-1 filterMe'>" + data.instruments + "</div>";
            concertRowHtml += "<div class='col-md-1 edit' style='display:none; height: 17px;'><div class='edit-concert-row' onclick='javascript: $(\"#concertSearch\").prop(\"disabled\", true); $(\".edit-concert-row\").hide(); $(\"#concertRow" + id + "\").hide(); $(\"#concertEditor" + id + "\").show();'><span class='fa fa-pencil'></i> Edit</div></div></div>";
            concertRowHtml += '<div class="concert-row-editor" id="concertEditor' + id + '">\
								<div class="col-md-5">\
									<input class="form-control" name="concert" placeholder="Concert" value="' + data.title + '" id="concertTitle' + id + '" style="box-shadow: none;">\
								</div><div class="col-md-2">\
									<input class="form-control" name="date" placeholder="DD/MM/YY" value="' + dstr + '/' + mstr + '/' + ystr + '" id="concertDate' + id + '" style="box-shadow: none;">\
								</div><div class="col-md-3">\
									<input class="form-control" name="venue" placeholder="Venue" value="' + data.venue + '" id="concertVenue' + id + '" style="box-shadow: none;">\
								</div><div class="col-md-2">\
									<input class="form-control inst-autocomplete" name="instrument" placeholder="Instrument" value="' + data.instruments + '"id="concertInst' + id + '" style="box-shadow: none;">\
								</div><div style="clear:both"></div><div class="row" style="margin-top:5px; margin-bottom:30px; font-size:30px; clear:both;">\
									<div class="col-sm-1 col-sm-offset-9">\
										<a onclick="updateConcertDetails(\'' + id + '\');">\
											<span class="fa fa-check hover-icon" style="color:#2ecc71"></i></a>\
									</div><div class="col-sm-1">\
										<a style="margin-left: 5px;" onclick="javascript: $(\'.invalid-date\').slideUp(\'fast\', function(){}); $(\'#concertSearch\').prop(\'disabled\', false); $(\'.edit-concert-row\').show(); $(\'#concertRow' + id + '\').show(); $(\'#concertEditor' + id + '\').hide();">\
											<span class="fa fa-times hover-icon" style="color:#e74c3c"></i></a>\
									</div><div class="col-sm-1">\
										<a onclick="deleteConcert(\'' + id + '\');" style="margin-left: 5px;">\
											<span class="fa fa-trash hover-icon" style="color:#e74c3c"></i></a>\
									</div>\
								</div></div>';
            $('#past-concert-table').append(concertRowHtml);
            $('.inst-autocomplete').autocomplete({
                source: instrumentList
            });
            $(".add-concert-details").slideDown("fast", function() {});
            $("#addConcertDetails").slideUp("fast", function() {});
            $('#titleAdd').val('');
            $('#venueAdd').val('');
            $('#instrumentAdd').val('');
            $('#concertDay').val(nowDay);
            $('#concertMonth').val(nowMonth);
            $('#concertYear').val(nowYear);
            $("#concertRow" + id).hover(function() {
                $(this).children(".edit").toggle();
            });
            updateConcertCount(true);
            alertify.success("Past concert added");
        }
    });
}

function updateConcertDetails(id) {
    console.log('submit');
    var date = $('#concertDate' + id).val().trim();
    var dateParts, fullyear;
    if (date.indexOf('/') > -1)
        dateParts = date.split('/');
    else if (date.indexOf('-') > -1)
        dateParts = date.split('-');
    if (typeof(dateParts) == "undefined" || !dateParts || dateParts.length != 3) {
        $('.invalid-date').slideDown('fast', function() {});
        return;
    }
    var year = parseInt(dateParts[2]);
    if (year < 50)
        fullyear = parseInt('20' + dateParts[2]);
    else
        fullyear = parseInt('19' + dateParts[2]);
    var dateF = new Date(Date.UTC(fullyear, parseInt(dateParts[1]) - 1, parseInt(dateParts[0]), 12, 0, 0, 0));
    if ((dateF.getFullYear().toString().slice(-2) != dateParts[2]) || (dateF.getMonth() + 1 != parseInt(dateParts[1])) || (dateF.getDate() != parseInt(dateParts[0]))) {
        $('.invalid-date').slideDown('fast', function() {});
        return;
    }
    $('.invalid-date').slideUp('fast', function() {});
    var data = {
        title: $('#concertTitle' + id).val().trim(),
        date: dateF,
        venue: $('#concertVenue' + id).val().trim(),
        instruments: $('#concertInst' + id).val().trim(),
    };
    console.log(data);
    var concertRowHtml = "<div class='col-md-5 filterMe' style='font-weight:bold'>" + data.title + "</div>\
						<div class='col-md-2 filterMe'>" + moment.utc(dateF).format('D MMM YY') + "</div>\
						<div class='col-md-3 filterMe'>" + data.venue + "</div>\
						<div class='col-md-1 filterMe'>" + data.instruments + "</div>\
						<div class='col-md-1 edit' style='display:none;'><div class='edit-concert-row' onclick=\"javascript: $('#concertSearch').prop('disabled', true); $('.edit-concert-row').hide();  $('#concertRow" + id + "').hide(); $('#concertEditor" + id + "').show(); \">\
						<span class='fa fa-pencil'> Edit</i></div></div>";
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + user._id + '/concerts/' + id + '/edit',
        success: function(data) {
            $("#concertEditor" + id).hide();
            $("#concertRow" + id).html(concertRowHtml);
            $("#concertRow" + id).show();
            $("#concertSearch").prop('disabled', false);
            $('.edit-concert-row').show();
        }
    });
}

function addConcertDetails() {
    $(".add-concert-details").slideUp("fast", function() {
        $("#addConcertDetails").slideDown("fast", function() {
            $('#titleAdd').focus();
        });
    });
}

function cancelConcertDetails() {
    $("#addConcertDetails").slideUp("fast", function() {
        $(".add-concert-details").slideDown("fast", function() {});
    });
}

function deleteConcert(id) {
    $('.invalid-date').slideUp('fast', function() {});
    var data = {};
    link = '/concerts/' + id + '/delete';
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: link,
        success: function(data) {
            $("#concertRow" + id).hide();
            $("#concertSearch").prop('disabled', false);
            $('.edit-concert-row').show();
            $("#concertRow" + id).remove();
            $("#concertEditor" + id).fadeOut('200', function() {
                $('#concertEditor' + id).remove();
            });
            updateConcertCount(false);
            alertify.error("Performance deleted");
        }
    });
}

function addPieceDetails() {
    $(".add-piece-details").slideUp("fast", function() {
        $("#addPieceDetails").slideDown("fast", function() {
            $('#pieceTitleAdd').focus();
        });
    });
}

function removeFromPiece(concertId, pieceId) {
    alertify.confirm("Are you sure?", function(e) {
        if (e) {
            var userId = user._id;
            $.ajax({
                type: 'POST',
                data: JSON.stringify({
                    playerId: userId
                }),
                contentType: 'application/json',
                url: '/users/' + user._id + '/concerts/' + concertId + '/pieces/removeme/' + pieceId,
                success: function(data) {
                    if (data == 200 || data == 'OK') {
                        $('#pieceRow' + pieceId).fadeOut('200', function() {
                            $('#pieceRow' + pieceId).remove();
                        });
                    }
                }
            });
        }
    });
}

function removeFromConcert(concertId) {
    alertify.confirm("Are you sure? All repertoire items from this concert will also be removed", function(e) {
        if (e) {
            var userId = user._id;
            $.ajax({
                type: 'POST',
                data: JSON.stringify({
                    playerId: userId
                }),
                contentType: 'application/json',
                url: '/users/' + user._id + '/concerts/' + concertId + '/removeme/',
                success: function(data) {
                    if (data == 200 || data == 'OK') {
                        $('#concertRow' + concertId).fadeOut('200', function() {
                            $('#concertRow' + concertId).remove();
                        });
                        alertify.success('Please wait for the page to reload...');
                        window.location.href = '/people/' + user._id + '/?tab=3';
                    }
                }
            });
        }
    });
}
$(window).on('resize', function() {
    var win = $(this);
    if (win.width() <= 768) {
        $(".mobile-cover").css("height", ($(".side-nav").height() + 12));
    }
});
$(".filterElement").hover(function() {
    $(this).children(".edit").toggle();
});
$('.sort-tip').hover(function() {
    console.log('hover!!!');
});
$(".piece-highlight").click(function(e) {
    console.log('registered');
    if ((typeof($(event.target).attr('class')) != "undefined") && ($(event.target).attr('class').indexOf("composer-link") > -1)) {} else {
        $(this).find(".piece-description").fadeToggle("medium");
    }
});

function submitCompDetails(id) {
    console.log('submit');
    var data = {
        piecetitle: $('#compTitleAdd').val().trim(),
        date: $('#compDateAdd').val().trim(),
        arrangement: $('#compArrangement').val().trim(),
        composer: user.fullname,
        description: $('#compDescAdd').val().trim(),
        media: (!$('#compMediaAdd').val().trim().length ? '' : $('#compMediaAdd').val().trim().indexOf('http://') > -1 || $('#compMediaAdd').val().trim().indexOf('https://') > -1 ? $('#compMediaAdd').val().trim() : 'http://' + $('#compMediaAdd').val().trim())
    };
    console.log(data);
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        mixpanel.track("User add composition", {
            "Current URL": document.URL,
            "Profile": user.fullname,
            "Own profile?": ownProfile,
            "Piece name": data.piecetitle + ' by ' + data.composer
        });
    }
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + id + '/composition',
        success: function(dataBack) {
            if (dataBack == 500)
                return;
            var id = dataBack;
            var desc = data.description;
            var pieceRowHtml = "<div id='compRow" + id + "' class='composition composition-editable filterElement'>\
									<div class='col-md-7'>\
										<div class='media-col' id='compYoutube" + id + "'>";
            if (data.media && data.media.length)
                pieceRowHtml += "<a href='" + data.media + "' style='display:inline-block' title='Listen' target='_blank'>\
									<img src='/img/icons/youtube-play.png' class='sidebar-icon compositionMediaLink'></a>";
            pieceRowHtml += '</div><div class="detail-col">';
            if (data.date && data.date.length)
                pieceRowHtml += '<div class="filterMe comp-title" id="compHeader' + id + '">' + data.piecetitle + ' (' + data.date + ')';
            else
                pieceRowHtml += '<div class="filterMe comp-title" id="compHeader' + id + '">' + data.piecetitle;
            pieceRowHtml += '<div class="edit" style="display: none; margin-left: 15px; padding:0px;">\
									<div class="edit-comp-row">\
										<span class="fa fa-pencil"></i>\
										  Click to edit</div></div>\
								</div><div class="filterMe comp-description" id="compDesc' + id + '" style="clear: both; font-size: 14px; margin-top:5px; margin-bottom: 0px; white-space: pre-wrap;">' + data.description + '\
								</div></div></div><div class="col-md-5">\
									<div class="filterMe" style="font-weight:bold">Instrumentation</div>\
									<div class="filterMe" id="compArr' + id + '" style="font-weight:normal; font-size: 14px; margin-top:4px; white-space:pre-wrap;">' + data.arrangement + '</div></div></div>';
            pieceRowHtml += '<div class="comp-row-editor" id="compEditor' + id + '">\
								<div class="col-md-2">\
									<input class="form-control" name="date" placeholder="Date" value="' + data.date + '" id="compDate' + id + '" style="box-shadow: none;">\
								</div><div class="col-md-4">\
									<input class="form-control" name="title" placeholder="Title" value="' + data.piecetitle + '" id="compTitle' + id + '" style="box-shadow: none;">\
								</div><div class="col-md-6">\
									<input class="form-control" name="arrangement" placeholder="Arrangement" value="' + data.arrangement + '"id="compArrangement' + id + '" style="box-shadow: none;">\
								</div><div class="col-md-12" style="padding-top:10px; clear: both;">\
									<textarea class="form-control" name="description" placeholder="Description" value="' + desc + '"id="compDescription' + id + '" style="box-shadow: none;">' + desc + '</textarea>\
								</div><div class="row" style="margin-top:5px; margin-bottom:15px; font-size:30px; clear:both;">\
									<div class="col-sm-4">\
										<input name="media" placeholder="YouTube link" value="' + data.media + '", id="compMedia' + id + '" style="box-shadow: none; margin-left:15px; margin-top:5px;" class="form-control">\
									</div><div class="col-sm-1 col-sm-offset-5">\
										<a onclick="updateCompDetails(\'' + id + '\');">\
											<span class="fa fa-check hover-icon" style="color:#2ecc71"></i></a>\
									</div><div class="col-sm-1">\
										<a style="margin-left: 5px;" onclick="javascript: $(\'#compositionsSearch\').prop(\'disabled\', false); $(\'.edit-comp-row\').show(); $(\'#compRow' + id + '\').show(); $(\'#compEditor' + id + '\').hide();">\
											<span class="fa fa-times hover-icon" style="color:#e74c3c"></i></a>\
									</div><div class="col-sm-1">\
										<a onclick="deleteComp(\'' + id + '\');" style="margin-left: 5px;">\
											<span class="fa fa-trash hover-icon" style="color:#e74c3c"></i></a>\
									</div>\
								</div></div>';
            pieceRowHtml += '<script>\
								$(\'#compRow' + id + '\').click(function(event){\
									if($(event.target).attr(\'class\').indexOf("compositionMediaLink") < 0){\
										$(\'#compositionsSearch\').prop(\'disabled\', true);\
										$(\'#compRow' + id + '\').fadeOut(\'fast\', function(){\
											$(\'#compEditor' + id + '\').fadeIn(\'fast\');\
										});\
									}\
								}); </script>';
            $('#compositions-table').append(pieceRowHtml);
            $('.inst-autocomplete').autocomplete({
                source: instrumentList
            });
            $(".add-comp-details").slideDown("fast", function() {});
            $("#addCompDetails").slideUp("fast", function() {});
            $('#compTitleAdd').val('');
            $('#compDescAdd').val('');
            $('#compDateAdd').val('');
            $('#compArrangement').val('');
            $('#compMediaAdd').val('');
            $("#compRow" + id).hover(function() {
                $(this).children(".edit").toggle();
            });
            $("#compRow" + id).click(function() {
                $(this).find(".comp-description").fadeToggle("slow");
            });
            updatePieceCount(true);
            alertify.success("Piece added");
        }
    });
}

function updateCompDetails(id) {
    console.log('submit');
    var comp = {
        date: $('#compDate' + id).val().trim(),
        piecetitle: $('#compTitle' + id).val().trim(),
        composer: user.fullname,
        arrangement: $('#compArrangement' + id).val().trim(),
        description: $('#compDescription' + id).val().trim(),
        media: (!$('#compMedia' + id).val().trim().length ? '' : $('#compMedia' + id).val().trim().indexOf('http://') > -1 || $('#compMedia' + id).val().trim().indexOf('https://') > -1 ? $('#compMedia' + id).val().trim() : 'http://' + $('#compMedia' + id).val().trim())
    };
    $.ajax({
        type: 'POST',
        data: JSON.stringify(comp),
        contentType: 'application/json',
        url: '/users/' + user._id + '/composition/' + id,
        success: function(data) {
            $("#compEditor" + id).hide();
            if (comp.date && comp.date.length)
                $("#compHeader" + id).html(comp.piecetitle + ' (' + comp.date + ')\
					<div class="edit" style="display: none; margin-left: 15px; padding:0px;">\
						<div class="edit-comp-row">\
							<span class="fa fa-pencil"></i>\
							  Click to edit</div></div>');
            else
                $("#compHeader" + id).html(comp.piecetitle + '\
					<div class="edit" style="display: none; margin-left: 15px; padding:0px;">\
						<div class="edit-comp-row">\
							<span class="fa fa-pencil"></i>\
							  Click to edit</div></div>');
            if (comp.media && comp.media.length)
                $('#compYoutube' + id).html("<a href='" + comp.media + "' style='display:inline-block' title='Listen' target='_blank'>\
									<img src='/img/icons/youtube-play.png' class='sidebar-icon compositionMediaLink'></a>");
            else
                $('#compYoutube' + id).html('');
            $("#compDesc" + id).html(comp.description);
            $("#compArr" + id).html(comp.arrangement);
            $("#compRow" + id).show();
            $("#compositionsSearch").prop('disabled', false);
            $('.edit-comp-row').show();
            alertify.success("Piece updated");
        }
    });
}

function addCompDetails() {
    $(".add-comp-details").slideUp("fast", function() {
        $("#addCompDetails").slideDown("fast", function() {
            $('#compTitleAdd').focus();
        });
    });
}

function cancelCompDetails() {
    $("#addCompDetails").slideUp("fast", function() {
        $(".add-comp-details").slideDown("fast", function() {});
    });
}

function deleteComp(id) {
    var data = {};
    var link = '/users/' + user._id + '/composition/' + id + '/delete';
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: link,
        success: function(data) {
            var data2 = {};
            link = '/users/' + user._id + '/pieces/' + id + '/delete2';
            $.ajax({
                type: 'POST',
                data: JSON.stringify(data2),
                contentType: 'application/json',
                url: link,
                success: function(data) {
                    $("#compRow" + id).remove();
                    $("#compEditor" + id).fadeOut('200', function() {
                        $('#compEditor' + id).remove();
                    });
                    $("#compositionsSearch").prop('disabled', false);
                    $('.edit-comp-row').show();
                    updatePieceCount(false);
                    alertify.error("Piece deleted");
                }
            });
        }
    });
}

function updateSoundcloud() {
    var video_id = $('#inputSoundcloud').val().trim().toLowerCase();
    if (!video_id || !video_id.length)
        return;
    if (video_id.indexOf('soundcloud.com') < 0)
        video_id = 'https://soundcloud.com/' + video_id;
    if (video_id.indexOf('https') < 0) {
        if (video_id.indexOf('http://') > -1) {
            video_id = video_id.slice(7, video_id.length);
        }
        video_id = 'https://' + video_id;
    }
    var data = {
        content: video_id
    };
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        mixpanel.track("User add soundcloud", {
            "Current URL": document.URL,
            "Profile": user.fullname,
            "Own profile?": ownProfile,
            "Soundcloud url": data.content
        });
    }
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + user._id + '/soundcloud',
        success: function(data) {
            updateEnabled('media', true);
            if (noSoundcloud) {
                noSoundcloud = false;
                SC.initialize({
                    client_id: "42594aad73d9b7596f9ae064e5d009a4"
                });
            }
            SC.oEmbed(video_id, {
                color: "ff0066"
            }, function(oEmbed) {
                if (oEmbed) {
                    $('#s').html(oEmbed.html);
                    widget = SC.Widget(document.querySelector('#s iframe'));
                }
            });
            $('#soundcloudDetails').css('margin-bottom', '10px');
            $('#soundcloudDetails').children().first().html('<img src="/img/icons/soundcloud-mini.png" width="34" height="34" style="margin-right:10px">\
								<input rows="1" placeholder="' + video_id + '" class="form-control input-rightbtn" id="inputSoundcloud" style="width:50%;flex-grow:2; -webkit-flex-grow:2;">\
								<btn style="margin-left: 0px; width:100px;" onclick="updateSoundcloud()" class="btn btn-info add-soundcloud-btn input-rightbtn">Update</btn>\
								<btn style="margin-left: 20px; width:120px;" onclick="removeSoundcloud()" class="btn btn-danger add-youtube-btn">\
									<span class="fa fa-trash" style="color:white; font-size:18px; margin-right:5px;"></span> Remove</btn>');
            $("#inputSoundcloud").keypress(function(e) {
                if (e.keyCode == 13) {
                    updateSoundcloud();
                }
            });
        }
    });
}

function removeSoundcloud() {
    alertify.confirm("Are you sure you want remove SoundCloud from your profile?", function(e) {
        if (e) {
            var data = {
                content: ""
            };
            $.ajax({
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                url: '/users/' + user._id + '/soundcloud',
                success: function(data) {
                    updateEnabled('media', false);
                    window.location.href = '/people/' + '' + user._id + '?tab=2';
                    $('#s').fadeOut();
                    alertify.success("Success! Please wait for the page to refresh...");
                }
            });
        }
    });
}

function getYoutubeTitle(id, callback) {
    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/youtube/v3/videos?id=' + id + '&key=AIzaSyBj8FfzItVRQXzFdM_KK472jYg0p71z2Ik&part=snippet',
        success: function(res) {
            callback(null, (res && res.items && res.items.length && res.items[0] ? res.items[0].snippet.title : ''));
        },
        error: function() {
            callback(true, null);
        }
    });
}

function getVimeoTitle(id, callback) {
    $.ajax({
        method: 'GET',
        url: 'http://vimeo.com/api/v2/video/' + id + '.json',
        success: function(res) {
            console.log(1, res);
            callback(null, (res && res.length ? res[0].title : ''));
        },
        error: function(e) {
            console.log(e);
            callback(true, null);
        }
    });
}

function updateYouTube(video_type) {
    var type = (video_type && video_type.length ? video_type : $(this).data('videoType'));
    var video_url;
    var getTitle;
    var video_id;
    if (type === 'youtube') {
        video_url = $('.js-upload-youtube').val().trim().replace(/youtu\.be\//gi, "youtube.com/watch?v=");
        video_id = video_url.split('v=')[1];
        if (!video_id || !video_id.length) {
            alertify.alert("Please enter the whole URL of the video, e.g. https://www.youtube.com/watch?v=dQw4w9WgXcQ");
            return;
        }
        getTitle = getYoutubeTitle;
    } else if (type === 'vimeo') {
        video_url = $('.js-upload-vimeo').val().trim();
        var video_match = video_url.match(/vimeo.com\/?.*\/([\d]+)/);
        video_id = (video_match && video_match.length > 1 ? video_match[1] : null);
        if (!video_id || !video_id.length) {
            alertify.alert("Please enter the whole URL of the video, e.g. https://vimeo.com/120469122");
            return;
        }
        getTitle = getVimeoTitle;
    } else {
        return;
    }
    var ampersandPosition = video_id.indexOf('&');
    if (ampersandPosition != -1) {
        video_id = video_id.substring(0, ampersandPosition);
    }
    getTitle(video_id, function(err, title) {
        if (err || !title) {
            alertify.prompt("Please enter a title for your video:", function(success, title) {
                if (success) {
                    if (!title) {
                        return alertify.alert("Please try again: no title entered");
                    }
                    var data = {
                        videoId: video_id,
                        type: type,
                        title: title
                    };
                    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
                        mixpanel.track("User add YouTube", {
                            "Current URL": document.URL,
                            "Profile": user.fullname,
                            "Own profile?": ownProfile,
                            "YouTube url": data.videoId
                        });
                    }
                    $.ajax({
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        url: '/users/' + user._id + '/youtube',
                        success: function(data) {
                            updateEnabled('media', true);
                            window.location.href = '/people/' + user._id + '?tab=2';
                            alertify.success("Success! Please wait for the page to refresh...");
                        }
                    });
                }
            });
        } else {
            var data = {
                videoId: video_id,
                type: type,
                title: title
            };
            if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
                mixpanel.track("User add YouTube", {
                    "Current URL": document.URL,
                    "Profile": user.fullname,
                    "Own profile?": ownProfile,
                    "YouTube url": data.videoId
                });
            }
            $.ajax({
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                url: '/users/' + user._id + '/youtube',
                success: function(data) {
                    updateEnabled('media', true);
                    window.location.href = '/people/' + user._id + '?tab=2';
                    alertify.success("Success! Please wait for the page to refresh...");
                }
            });
        }
    });
}

function removeYouTube(id) {
    alertify.confirm("Are you sure you want remove this video?", function(e) {
        if (e) {
            var data = {
                id: id,
            };
            $.ajax({
                type: 'DELETE',
                data: JSON.stringify(data),
                contentType: 'application/json',
                url: '/users/' + user._id + '/youtube',
                success: function(data) {
                    updateEnabled('media', false);
                    window.location.href = '/people/' + user._id + '?tab=2';
                    alertify.success("Success! Please wait for the page to refresh...");
                }
            });
        }
    });
}

function updateYouTubeChannel() {
    var video_id = $('#inputYouTubeChannel').val().trim();
    if (!video_id || !video_id.length)
        return;
    if (video_id.lastIndexOf('/') == video_id.length - 1)
        video_id = video_id.slice(0, video_id.length - 1);
    if (video_id.indexOf('/') > -1)
        video_id = video_id.slice(video_id.lastIndexOf('/') + 1, video_id.length);
    var data = {
        content: video_id
    };
    if (typeof(mixpanelTracking) != 'undefined' && mixpanelTracking && mixpanel) {
        mixpanel.track("User add YouTube channel", {
            "Current URL": document.URL,
            "Profile": user.fullname,
            "Own profile?": ownProfile,
            "YouTube url": data.content
        });
    }
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + user._id + '/youtubechannel',
        success: function(data) {
            updateEnabled('media', true);
            window.location.href = '/people/' + user._id + '?tab=2';
            alertify.success("Success! Please wait for the page to refresh...");
        }
    });
}

function removeYouTubeChannel() {
    alertify.confirm("Are you sure you want remove this channel?", function(e) {
        if (e) {
            var data = {
                content: ""
            };
            $.ajax({
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                url: '/users/' + user._id + '/youtubechannel',
                success: function(data) {
                    updateEnabled('media', false);
                    window.location.href = '/people/' + user._id + '?tab=2';
                    alertify.success("Success! Please wait for the page to refresh...");
                }
            });
        }
    });
}

function removeImage(id) {
    alertify.confirm("Delete this image?", function(e) {
        if (e) {
            var url = '/users/' + user._id + '/gallery/delete/' + id;
            $.ajax({
                type: 'POST',
                data: JSON.stringify({}),
                contentType: 'application/json',
                url: url,
                success: function(data) {
                    $('#galleryThumb' + id).fadeOut(function() {
                        $(this).remove();
                        if (!$('#links').children().length) {
                            $('.image-gallery-holder').fadeOut(function() {
                                $(this).hide();
                            });
                        }
                    });
                }
            });
        }
    });
}

function updateTwitter() {
    t = $('#inputTwitter').val().replace(/\s/g, '');
    var data = {};
    if ((typeof(t) == "undefined") || (t === "")) {
        $('.twitter-value').html('Click here to add twitter');
        data.content = '';
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: '/users/' + user._id + '/twitterHandle',
            success: function(data) {
                return '';
            }
        });
        return;
    }
    if (t.indexOf('.com/') > -1)
        t = t.slice(t.indexOf('.com/') + 5);
    var tHandle = "";
    var index = t.indexOf("@");
    if (index === 0) {
        tHandle = t;
    } else if (index < 0) {
        tHandle = "@" + t;
    } else {
        tHandle = t.slice(index);
    }
    data.content = tHandle;
    console.log(data);
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: '/users/' + user._id + '/twitterHandle',
        success: function(data) {
            return tHandle;
        }
    });
    $('.twitter-value').html(tHandle);
}
$(".follow-btn").mouseenter(function() {
    if (following === true)
        $(this).html('<span class="fa fa-minus" style="color:white; margin-right:10px; position:relative; top:0px;"></span>Unfollow');
}).mouseleave(function() {
    if (following === true)
        $(this).html('<span class="fa fa-user" style="color:white; margin-right:10px; position:relative; top:-1px;"></span>Following');
});

function contactPopup(name, recipientID) {
    var width = ($(window).width() < 768 ? '90vw' : '70%');
    var height = ($(window).width() < 768 ? $(window).height() - 50 : 'auto');
    if ($(window).width() < 768)
        $('body').addClass('mobile');
    else
        $('body').removeClass('mobile');
    $('.contact-overlay').dialog({
        height: height,
        width: width,
        draggable: false,
        resizable: false,
        modal: true,
        dialogClass: 'contact-dialog',
        show: {
            effect: 'fade',
            duration: 300
        },
        hide: {
            effect: 'fade',
            duration: 300
        },
        open: function(event, ui) {
            $('html').addClass('stop-scrolling');
            $('.ui-widget-overlay').bind('click', function() {
                $(this).siblings('.ui-dialog').find('.ui-dialog-content').dialog('close');
            });
        },
        beforeClose: function(event, ui) {
            $('html').removeClass('stop-scrolling');
        }
    });
    $(".nameText").text("We'll send your message as an email to " + name);
    $("#messageTextarea").text("Hi " + name + ",\n\n\n\nBest wishes,\n\n");
    $("#sendMsg").attr("onclick", "message('" + recipientID + "')");
}

function message(targetId) {
    if ($("#messageSubject").val().replace(/\s/g, '').length && $("#messageTextarea").val().replace(/\s/g, '').length) {
        var data = {
            subject: $("#messageSubject").val(),
            message: $("#messageTextarea").val()
        };
        var subjectFilter = new Sanitize(data.subject, []);
        if (subjectFilter.check()) {
            var error = subjectFilter.generateError();
            var subjectError = error.slice(0, -1) + ' from the subject field.'
            alertify.alert(subjectError);
            return;
        }
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: '/users/message/' + targetId,
            success: function(data) {
                if (data == 200 || data == 'OK') {
                    console.log("Message sent!");
                    alertify.success("Message sent");
                    $('.contact-overlay').dialog('close');
                }
            },
            error: function(xhr, err) {
                console.log("error!");
                console.log(err);
                alertify.alert("Sorry, something went wrong. Please try again or contact help@encoremusicians.com if this problem persists.");
            }
        });
        var params = getURLParameters();
        if (params.jobRef) {
            $.ajax({
                type: 'PUT',
                data: JSON.stringify({}),
                contentType: 'application/json',
                url: '/jobs/' + params.jobRef + '/replied',
                data: JSON.stringify({
                    applicantId: targetId
                }),
                error: function(err) {
                    console.log(err)
                },
                success: function(data) {
                    if (data == 200 || data == 'OK') {
                        console.log("applicant updated");
                    }
                }
            });
        }
    } else {
        alertify.alert("Please enter a subject and message");
    }
}

function submitReference(refereeID, content, callback) {
    $.ajax({
        type: 'POST',
        data: JSON.stringify({
            referee: refereeID,
            content: content
        }),
        contentType: 'application/json',
        url: '/users/' + user._id + '/reference',
        success: callback
    });
}

function handleJobAlerts(id, disable) {
    $.ajax({
        type: 'POST',
        url: "/users/" + id + "/jobAlerts",
        contentType: 'application/json',
        data: JSON.stringify({
            disable: disable,
            weeks: $('.job-alert-weeks').val() || 0,
            userId: id
        }),
        success: function(data) {
            if (data == 'OK' || data == 200) {
                alertify.success("Saved");
                if (JSON.parse(this.data).disable) {
                    $(".disable-job-alerts").hide();
                    $('.job-alert-weeks').hide();
                    $('.job-alert-weeks-label').hide();
                    $(".enable-job-alerts").show();
                    $(".job-alerts-disabled-text").show();
                    var suspensionTime = parseInt(JSON.parse(this.data).weeks) * 604800000;
                    var currentTime = new Date().getTime();
                    $(".job-alerts-disabled-text").text("Disabled until " + new Date(currentTime + suspensionTime));
                } else {
                    $(".enable-job-alerts").hide();
                    $(".job-alerts-disabled-text").hide();
                    $(".disable-job-alerts").show();
                    $('.job-alert-weeks').show();
                    $('.job-alert-weeks-label').show();
                }
            } else {
                alert("Error: " + data);
            }
        }
    });
}

function handleCallRequest(id) {
    $.ajax({
        type: 'POST',
        url: "/users/" + id + "/callRequest",
        contentType: 'application/json',
        data: JSON.stringify({
            enable: !isCallRequestEnabled,
            userId: id
        }),
        success: function(data) {
            if (data == 'OK' || data == 200) {
                alertify.success("Saved");
                isCallRequestEnabled = !isCallRequestEnabled;
            } else {
                alert("Error: " + data);
            }
            $('#allowCallRequestButton').html(isCallRequestEnabled ? 'Disable call request' : 'Enable call request');
        }
    });
}

function deleteUser(id) {
    alertify.confirm("Are you sure?", function(e) {
        if (e) {
            $('#dbtn' + id).hide();
            $.ajax({
                type: 'POST',
                url: "/users/" + id + "/delete",
                contentType: 'application/json',
                data: JSON.stringify({}),
                success: function(data) {
                    if (data == 'OK') {
                        alertify.success("DELETED");
                        window.location = '/dashboard';
                    } else {
                        alert("Error: " + data);
                    }
                }
            });
        }
    });
}

function deleteTrack() {
    var gcid = $(this).data('gcid');
    $.ajax({
        method: 'DELETE',
        url: '/users/' + user._id + '/audio',
        data: {
            gcid: gcid
        },
        success: function() {
            $('.track[data-gcid="' + gcid + '"]').remove();
            updateEnabled('media', false);
        },
        error: function() {
            console.log('Error delete track');
        }
    })
}

function savePli() {
    var n = $('.pli-input').val();
    if (n && n.length && parseInt(n) > 0) {
        var cover = '£' + parseInt(n) + ' million';
        $.ajax({
            type: 'PUT',
            url: "/users/" + user._id + "/update",
            contentType: 'application/json',
            data: JSON.stringify({
                pliCover: cover
            }),
            success: function(data) {
                console.log("Update successful");
            }
        });
        $.ajax({
            type: 'POST',
            url: "/users/" + user._id + "/tag",
            data: {
                tag: 'pli'
            },
            success: function(data) {
                console.log("success");
                alertify.success("Success. Please wait...");
                window.location.reload();
            }
        });
    }
}

function removePli() {
    $.ajax({
        type: 'POST',
        url: "/users/" + user._id + "/untag",
        data: {
            tag: 'pli'
        },
        success: function(data) {
            console.log("success");
            alertify.success("Success. Please wait...");
            window.location.reload();
        }
    });
}

function addPli() {
    $('.pli-form').toggle();
}

function updateNotes(e) {
    var val = $('.js-musician-notes').val().trim();
    $.ajax({
        type: 'POST',
        data: {
            notes: val
        },
        url: '/users/' + user._id + '/admin/notes',
        success: function(data) {
            if (data == 200 || data == 'OK') {
                console.log("Details updated");
                alertify.success("Success. Please wait...");
            }
        }
    });
}